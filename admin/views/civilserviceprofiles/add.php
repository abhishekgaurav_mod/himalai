<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		          		<a href="<?=_URL?>civilserviceprofiles/" class="btn btn-primary">All Civil Service Profile</a>
						<a href="<?=_URL?>civilserviceprofiles/add" class="btn btn-primary">Add Civil Service</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<h2><?=ucfirst($action).' '.ucfirst($this->formhead)?></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="forms addslider">


						<form name="frmaddslider" id="frmaddslider" action="<?=_URL?>civilserviceprofiles/add" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask">
							<div class="fieldbox pad10top">
								<input type="hidden" name="action" value="<?=$action?>" />
								<input type="hidden" name="imagename" id="imagename" value="<?=(!empty($object->category_icon)?$object->category_icon:'')?>" />
								<?php if ($action=='edit' && !empty($object)): ?>
								<input type="hidden" name="civilserviceprofileId" value="<?=$id?>" />
								<!-- <input type="hidden" name="thumbId" value="<?=(empty($object->thumb))?null:$object->thumb?>" /> -->
								<?php endif;?>
							    <div class="row">
							      	<div class="col-md-4 col-xs-12">
										<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label for="name">Name</label>
											<input name="name" type="text" id="name" placeholder="Name" value="<?=(!empty($object->name)?htmlentities($object->name):'')?>" maxlength="500" autocomplete="off" class="form-control has-feedback-left" />
		                        			<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
										</div>
									</div>
							      	<div class="col-md-7 col-xs-12">
										<div class="field">
											<label class="label">Description</label>
											<textarea name="description" id="description" rows="7" cols="50" placeholder="Civilserviceprofile Text..."><?=(!empty($object->description)?stripcslashes($object->description):'')?></textarea>
										</div>

										<div class="clearfix"></div>
				                      	<div class="ln_solid"></div>
				                      	<div class="form-group">
											<div class="field col-md-9 col-sm-9 col-xs-12">
												<span class="loading">Please wait...</span>
												<button tabindex="11" type="submit" name="save" value="Save" class="inline btn btn-success" >Save</button>
												<button tabindex="12" type="reset" name="reset" value="Clear" class="inline btn btn-success" >Clear</button>
											</div>
				                      	</div>

									</div>
								</div>
							</div>
							</form>

						</div>		            
		          	</div>
		        </div>
	      	</div>

	    </div>
  	</div>
</div>
<div class="clearfix"></div>

<script type="text/javascript">
/*CKEDITOR.replace('description',{allowedContent:true,resize_enabled:'false',resize_maxHeight:'280',resize_minHeight: '280',toolbar:[{name:'document',items:['Source','-','NewPage','Preview','-','Templates']},{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },{name:'basicstyles',items:['Bold','Italic']}]});*/
var ckConfig = {
		allowedContent: true, resize_enabled:'false',width: '100%', height: '350px', resize_maxHeight:'320', resize_minHeight: '600',
		toolbar:[
			{ name:'document', items:['source'] },
			{ name:'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
			{ name:'basicstyles', items:['Bold','Italic','Underline'] },
			'/',
	       	{ name:'insert', items: ['SpecialChar', 'Maximize', 'Source'] },
			{ name:'styles', items : ['Styles', 'Format','FontSize'] }
		]
	};
CKEDITOR.config.forcePasteAsPlainText 		= true;
CKEDITOR.replace('description',ckConfig);
</script>
