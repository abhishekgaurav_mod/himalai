<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		          		<a href="<?=_URL?>questionyears/index" class="btn btn-primary">Question Years</a>
						<a href="<?=_URL?>questionyears/add" class="btn btn-primary">Add New Question Year</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

		<?php if (!empty($obj_questionyear)): ?>
	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<ul class="nav navbar-right panel_toolbox">
							<a href="<?=_URL?>questionyears/add?action=edit&id=<?=$id?>" class="btn btn-primary">Update</a>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
	      					<div class="col-md-6 col-xs-12">
								<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
				                    	<table class="table" width="100%" border="0">
											<tr>
												<td><b>Question Year Id</b></td>
												<td>: <?=$obj_questionyear->questionyear_id?></td>
											</tr>
											<tr>
												<td><b>Question Year Title</b></td>
												<td>: <?=$obj_questionyear->questionyear_name?></td>
											</tr>
											<tr>
												<td><b>Exam Type</b></td>
												<td>: <?=($obj_questionyear->exam_type=='P')?'Prelims':'Mains'?></td>
											</tr>
											<tr>
												<td><b>Question Year</b></td>
												<td>: <?=$obj_questionyear->question_year?></td>
											</tr>
											<!-- <tr>
												<td><b>Questionyear Icon</b></td>
												<td>:<? if (!empty($obj_questionyear->imagepath)):?> <a href="<?=_CATEGORY_ORG_URL.$obj_questionyear->imagepath?>" rel="facebox">View Picture</a><? endif;?></td>
											</tr> -->
											<tr>
												<td><b>Enabled</b></td>
												<td>: <?=($obj_questionyear->enabled=='Y')?'Yes':'No'?></td>
											</tr>
											<tr>
												<td><b>Created On</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($obj_questionyear->created_on))?></td>
											</tr>
											<tr>
												<td><b>Updated On</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($obj_questionyear->updated_on))?></td>
											</tr>
										</table>
				                    </div>
		      					</div>
		      				</div>
						</div>
	                </div>
	                
		        </div>
	      	</div>
	    </div>
		<?php endif; ?>
  	</div>
</div>
<div class="clearfix"></div>