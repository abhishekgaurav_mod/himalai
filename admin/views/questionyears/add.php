<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		          		<a href="<?=_URL?>questionyears/index" class="btn btn-primary">Question Years</a>
						<a href="<?=_URL?>questionyears/add" class="btn btn-primary">Add New Question Year</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

	    <div class="row">
	      	<div class="col-md-6 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<h2><?=ucfirst($action).' '.ucfirst($this->formhead)?></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="forms addslider">
							<form name="frmaddslider" id="frmaddslider" action="<?=_URL?>questionyears/add" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask">
								<input type="hidden" name="action" value="<?=$action?>" />
								<?php if ($action=='edit' && !empty($obj_questionyear)): ?>
								<input type="hidden" name="questionyearId" value="<?=$id?>" />
								<?php endif;?>

								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label for="title">Question Year Title: </label>
									<input name="title" type="text" id="title" placeholder="Questionyear Title" value="<?=(!empty($obj_questionyear->questionyear_name)?$obj_questionyear->questionyear_name:'')?>" maxlength="255" autocomplete="off" class="form-control has-feedback-left" />
	                        		<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>
								
								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label>Question Year Order: </label>
									<select id="exam_type" name="exam_type" class="form-control has-feedback-left">
										<option value="">-Select Exam Type-</option>
										<option value="P" <?=(($obj_questionyear->exam_type == 'P')?'selected':'')?> >Prelims</option>
										<option value="M" <?=(($obj_questionyear->exam_type == 'M')?'selected':'')?> >Mains</option>
									</select>
                    				<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>
								
								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label for="question_year">Question Year: </label>
									<input name="question_year" type="text" id="question_year" placeholder="Question Year" value="<?=(!empty($obj_questionyear->question_year)?$obj_questionyear->question_year:'')?>" maxlength="4" class="form-control has-feedback-left" />
	                        		<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>

								<div class="clearfix"></div>
		                      	<div class="ln_solid"></div>
		                      	<div class="form-group">
									<div class="field col-md-9 col-sm-9 col-xs-12">
										<span class="loading">Please wait...</span>
										<button type="submit" name="save" value="Save" class=" inline btn btn-success" >Save</button>
										<button type="reset" name="reset" value="Clear" class="inline btn btn-success" >Clear</button>
									</div>
		                      	</div>
							</form>

						</div>		            
		          	</div>
		        </div>
	      	</div>

	    </div>
  	</div>
</div>
<div class="clearfix"></div>