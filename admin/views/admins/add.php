<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

	    <div class="row">
	      	<div class="col-md-6 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<h2><?=ucfirst($action).' '.ucfirst($this->formhead)?></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="forms addslider">

							<form name="frmaddslider" id="frmaddslider" action="<?=_URL?>admins/add" method="post" enctype="multipart/form-data">
								<input type="hidden" name="action" value="<?=$action?>" />
								<?php if ($action=='edit' && !empty($obj_admin)): ?>
								<input type="hidden" name="adminId" value="<?=$id?>" />
								<?php endif;?>
								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label>Admin Email</label>
									<input name="email" type="text" id="email" placeholder="Email" maxlength="50" autocomplete="off" value="<?=(!empty($obj_admin->email)?$obj_admin->email:'')?>" class="form-control has-feedback-left" />
                        			<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label>Password</label>
									<input name="password" type="password" id="password" placeholder="Password" autocomplete="off" maxlength="20" class="form-control has-feedback-left" />
                        			<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label>Retype Password</label>
									<input name="retypepassword" type="password" id="retypepassword" placeholder="Retype Password" autocomplete="off" maxlength="20" class="form-control has-feedback-left" />
                        			<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label>Full Name</label>
									<input name="name" type="text" id="name" placeholder="Full Name" maxlength="255" autocomplete="off" value="<?=(!empty($obj_admin->name)?$obj_admin->name:'')?>" class="form-control has-feedback-left" />
                        			<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>

								<div class="clearfix"></div>
		                      	<div class="ln_solid"></div>
		                      	<div class="form-group">
									<div class="field col-md-9 col-sm-9 col-xs-12">
										<span class="loading">Please wait...</span>
										<button type="submit" name="save" value="Save" class=" inline btn btn-success" >Save</button>
									</div>
		                      	</div>
							</form>

						</div>		            
		          	</div>
		        </div>
	      	</div>

	    </div>
  	</div>
</div>
<div class="clearfix"></div>