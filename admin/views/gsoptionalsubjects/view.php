<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		          		<a href="<?=_URL?>gsoptionalsubjects/index" class="btn btn-primary">GS Optional Subject</a>
						<a href="<?=_URL?>gsoptionalsubjects/add" class="btn btn-primary">Add GS Subject</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

		<?php if (!empty($object)): ?>
	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<ul class="nav navbar-right panel_toolbox">
							<a href="<?=_URL?>gsoptionalsubjects/add?action=edit&id=<?=$id?>" class="btn btn-primary">Update</a>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
	      					<div class="col-md-6 col-xs-12">
								<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
										<h3>Subject Name :- <?=ucfirst($object->name)?></h3>
										<h5>Created On :- <br><?=date('jS F, Y g:s A',strtotime($object->created_on))?></h4>
										<h5>Updated On :- <br><?=date('jS F, Y g:s A',strtotime($object->updated_on))?></h4><br><br>
										<h3>Subject Thumbnail :-</h3>
				                      	<div><?php if(!empty($object->thumb)):?><img src="<?=_GS_OPTIONALSUBJECT_THUMB_ORG_URL.$object->thumb?>" class="img-responsive"><?php endif;?></div>
				                    </div>
		      					</div>
		      				</div>
	      					<div class="col-md-6 col-xs-12">
	      						<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
										<h3>Description :- </h3>
				                      	<div><?=ucfirst($object->description)?></div>
				                    </div>
		      					</div>
		      				</div>
						</div>
	                </div>
	                
		        </div>
	      	</div>
	    </div>
		<?php endif; ?>
  	</div>
</div>
<div class="clearfix"></div>
