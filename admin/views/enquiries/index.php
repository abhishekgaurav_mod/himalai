<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>
	    </div>
	    <div class="clearfix"></div>

	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_content">

						<div class="filterTable">
						    <div class="row">
						      	<div class="col-md-6 col-xs-12">
								    <div class="row">
								      	<div class="col-md-6 col-xs-12">
								      		<div class="title">Filter By Date</div>
											<input type="text" placeholder="Date" name="date" id="date" class="form-control" value="<?=(!empty($getdate))?$getdate:''?>" />
											<button type="button" name="clear" value="clear" onclick="filterArticles('bydate');">Go</button>							      		
								      	</div>
								      	<div class="col-md-6 col-xs-12">
								      		<div class="title">Filter Between Dates</div>
											<input placeholder="Start Date" type="text" name="startdate" id="startdate" class="form-control" value="<?=(!empty($startdate))?$startdate:''?>" />
											<input placeholder="End Date" type="text" name="enddate" id="enddate" class="form-control" value="<?=(!empty($enddate))?$enddate:''?>" />
											<button type="button" name="clear" value="clear" onclick="filterArticles('bydaterange');">Go</button>
								      	</div>
								    </div>
						      	</div>
						      	<div class="col-md-6 col-xs-12">
						      		<div> <!-- <?=$_GET['actions']?> --> </div>
						      	</div>
						    </div>

						    <div>
						    	<a class="clearBtn" href="<?=_URL?>enquiries">Clear</a>
						    	<a class="exportBtn" href="<?=_URL?>enquiries/generateEnquiryExcel?action=<?=$_GET['action']?>&status=<?=$_GET['status']?>&date=<?=$_GET['getdate']?>&startdate=<?=$_GET['start']?>&enddate=<?=$_GET['end']?>" target="_blank">Export</a>
						    </div>
						</div>
						
						<div class="datalist">
					    	<table class="display" id="datatable">
					        <thead>
					        	<tr>
					        		<th width="50">Enquiry Id</th>
					        		<th width="100">Name</th>
					        		<th width="100">Phone No</th>
					                <th width="150">Email </th>
					                <th width="100">Interest </th>
					                <th width="100">Created On</th>
					                <th width="50">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="7" class="dataTables_empty">Loading data from server</td>
								</tr>
							</tbody>
					        </table>
						</div>
		            
		          	</div>
		        </div>
	      	</div>

	    </div>
  	</div>
</div>
<div class="clearfix"></div>
<script>
var filterArticles	=	function (action){
	var status		=	'<?=$status?>';
	if(action == 'bydate')
	{	
		var getdate		=	$('#date').val();
		if(getdate == '')
		{	alert('Please select date'); return false;}
		window.location.href	=	'<?=_URL?>enquiries?action='+action+'&status='+status+'&getdate='+getdate;
	}
	if(action == 'bydaterange')
	{	
		var start	=	$('#startdate').val();
		var end		=	$('#enddate').val();
		if(start == ''){ alert("Please select Start Date"); return false;  }
		if(end	== '') { alert("Please select End Date"); return false;  }
		if(Date.parse(start) >= Date.parse(end))
		{	alert("End date should be greater than Start date"); return false;	}
		window.location.href	=	'<?=_URL?>enquiries?action='+action+'&status='+status+'&start='+start+'&end='+end;
	}
	
	if(action == 'byall')
	{	window.location.href	=	'<?=_URL?>enquiries?status='+status;	}
};

$( "#date" ).datepicker({ 
	changeMonth: true,
	changeYear: true,
	defaultDaurlte: new Date(),
	dateFormat: 'yy-mm-dd',
	maxDate: new Date()
});
$( "#startdate" ).datepicker({ 
	changeMonth: true,
	changeYear: true,
	defaultDaurlte: new Date(),
	dateFormat: 'yy-mm-dd',
	maxDate: new Date()
});
$( "#enddate" ).datepicker({ 
	changeMonth: true,
	changeYear: true,
	defaultDaurlte: new Date(),
	dateFormat: 'yy-mm-dd',
	maxDate: new Date()
});

$(document).ready(function() {

	if (oTable!=null){	oTable.fnDestroy();	}
    var oTable = $('#datatable').dataTable( {
    	"bDestroy" : true,
    	"bStateSave": true,
    	"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "<?=$ajaxSource?>",
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({ "name": "status", "value": "<?=$status?>" },
					{ "name": "action", "value": "<?=$action?>" },
					{ "name": "date", "value": "<?=$date?>" },
					{ "name": "startdate", "value": "<?=$startdate?>" },
					{ "name": "enddate", "value": "<?=$enddate?>" });
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			}).done(function() {
				$('#datatable').removeAttr('style');
				$('.deleters').on('click', function(){
					var id	=	(this.id).replace('del_','');
					deleters(id, 'properties', 'properties');
				});
				$('.editrs').on('click', function(){
					var id	=	(this.id).replace('edit_','');
					loadform('properties', 'action=edit&id='+id);
				});
			});
		},
		"fnDrawCallback": function () {},
        "sPaginationType": "full_numbers",
        "iDisplayLength": <?=_DATA_LIMIT?>,
        "bProcessing": true,
        "sDom": 'CRTfrtip',
       	"aoColumns": [null,null,null,null,null,null,
						{	"sName": "id",
							"bSearchable": false,
							"bSortable": false,
							"fnRender": function (oObj) {
								var html	=	'<a href="<?=_URL?>enquiries/view?enquiryId='+oObj.aData[0]+'">View</a>';							
								return html;
							}
						}
    	],
     	"aaSorting": [[ 0, "desc" ]],
    });
    return oTable;
});
</script>

