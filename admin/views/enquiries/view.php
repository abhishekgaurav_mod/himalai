<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		            	<a href="<?=_URL?>enquiries/" class="btn btn-primary">All Enquiries</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

		<?php if (!empty($obj_enquiry)): ?>
	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_content">
						<div class="row">
	      					<div class="col-md-6 col-xs-12">
								<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
										<h4><b>Name :-</b> <?=ucfirst($obj_enquiry->name)?></h4><br>
										<h4><b>Phone :-</b> <?=ucfirst($obj_enquiry->phone)?></h4><br>
										<h4><b>Email :-</b> <?=ucfirst($obj_enquiry->email)?></h4><br>
										<h4><b>Qualification :-</b> <?=ucfirst($obj_enquiry->qualification)?></h4><br>
										<h4><b>Interest :-</b> <?=ucfirst($obj_enquiry->interest)?></h4><br>
										<h4><b>Enquired On :-</b> <?=ucfirst($obj_enquiry->created_on)?></h4>
				                    </div>
		      					</div>
		      				</div>
	      					<div class="col-md-6 col-xs-12">
	      						<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
										<h3><b>Message :-</b> </h3>
				                      	<div><?=stripcslashes($obj_enquiry->message)?></div>
				                    </div>
		      					</div>
		      				</div>
						</div>
	                </div>
	                
		        </div>
	      	</div>
	    </div>
		<?php endif; ?>
  	</div>
</div>
<div class="clearfix"></div>
