<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		            	<a href="<?=_URL?>sliders" class="btn btn-primary">Sliders</a>
		            	<a href="<?=_URL?>sliders/add" class="btn btn-primary">Add New Slider</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<h2><?=ucfirst($action).' '.ucfirst($this->formhead)?></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="forms addslider">

							<form name="frmaddslider" id="frmaddslider" action="<?=_URL?>sliders/add" accept-charset="utf-8"  method="post" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask">
								<div class="fieldbox pad10top">
									<input type="hidden" name="action" value="<?=$action?>" />
									<?php if ($action=='edit' && !empty($obj_slider)): ?>
										<input type="hidden" name="sliderId" value="<?=$id?>" />
										<input type="hidden" name="imagename" value="<?=(empty($obj_slider->imagepath))?null:$obj_slider->imagepath?>" />
									<?php endif;?>

									<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
										<label for="imagepath">Upload Image</label>
										<input type="file" name="imagepath" id="imagepath" />
										<?php if (!empty($obj_slider->imagepath)):?> <a href="<?=_SLIDER_ORG_URL.$obj_slider->imagepath;?>" rel="facebox">View Picture</a><?php endif;?>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
										<label>Slider Type</label>
										<select id="slider_type" name="slider_type" class="form-control has-feedback-left">
											<option value="">-Select Slider Type-</option>
											<option value="M" <?=(($obj_slider->slider_type == 'M')?'selected':'')?> >Main Slider</option>
											<option value="KAS" <?=(($obj_slider->slider_type == 'KAS')?'selected':'')?> >KAS Slider</option>
										</select>
                        				<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
									</div>
									<div class="field col-md-12 col-sm-12 col-xs-12">
										<label>Description</label>
										<textarea name="description" id="description" rows="7" cols="50" placeholder="Slider text..."><?=(!empty($obj_slider->description)?$obj_slider->description:'')?></textarea>
									</div>

									<div class="clearfix"></div>
			                      	<div class="ln_solid"></div>
			                      	<div class="form-group">
										<div class="field col-md-9 col-sm-9 col-xs-12">
											<span class="loading">Please wait...</span>
											<button type="submit" name="save" value="Save" class=" inline btn btn-success" >Save</button>
											<button type="reset" name="reset" value="Clear" class="inline btn btn-success" >Clear</button>
										</div>
			                      	</div>
								</div>
							</form>

						</div>		            
		          	</div>
		        </div>
	      	</div>

	    </div>
  	</div>
</div>
<div class="clearfix"></div>

<script type="text/javascript">
/*CKEDITOR.replace('description',{allowedContent:true,resize_enabled:'false',resize_maxHeight:'280',resize_minHeight: '280',toolbar:[{name:'document',items:['Source','-','NewPage','Preview','-','Templates']},{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },{name:'basicstyles',items:['Bold','Italic']}]});*/

// CK EDITOR
var ckConfig = {
		allowedContent: true, resize_enabled:'false',width: '100%', height: '350px', resize_maxHeight:'320', resize_minHeight: '600',
		toolbar:[
			{ name:'clipboard', items:['Cut', 'Copy', 'Paste', '-', 'Undo', 'Redo'] },
			{ name:'document', items:['source'] },
			{ name:'paragraph', /*groups: ['list', 'indent', 'blocks', 'align', 'bidi'],*/ items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
			{ name:'basicstyles', items:['Bold','Italic','Underline','Strike', 'Scayt'] },
			'/',
	       	{ name:'insert', items: ['HorizontalRule', 'SpecialChar', 'PageBreak' , 'Image', /*'Link', 'Unlink',*/ 'Maximize', 'Table', 'Source'] },
			{ name:'styles', items : ['Styles', 'Format','FontSize'] }
		]
	};
CKEDITOR.config.forcePasteAsPlainText 		= true;
/*CKEDITOR.config.filebrowserImageBrowseUrl 	= '<?=_URL?>fileuploader/index?type=articlecontentimages';*/
CKEDITOR.replace('description',ckConfig);


</script>
