<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		            	<a href="<?=_URL?>sliders" class="btn btn-primary">Sliders</a>
		            	<a href="<?=_URL?>sliders/add" class="btn btn-primary">Add New Slider</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

		<?php if (!empty($obj_slider)): ?>
	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<ul class="nav navbar-right panel_toolbox">
							<a href="<?=_URL?>sliders/add?action=edit&id=<?=$id?>" class="btn btn-primary">Update</a>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
	      					<div class="col-md-6 col-xs-12">
								<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
										<h4>Slider Type :- <?=$obj_slider->slider_type?></h4>
										<h4>Created On :- <?=date('jS F, Y g:s A',strtotime($obj_slider->created_on))?></h4>
										<h4>Updated On :- <?=date('jS F, Y g:s A',strtotime($obj_slider->updated_on))?></h4>
				                    </div>
		      					</div>
		      				</div>
	      					<div class="col-md-6 col-xs-12">
	      						<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
										<h3>Description :- </h3>
				                      	<div><?=ucfirst($obj_slider->description)?></div><br><br>
				                      	<div><img src="<?=_SLIDER_ORG_URL.$obj_slider->imagepath?>" class="img-responsive"></div>
				                    </div>
		      					</div>
		      				</div>
						</div>
	                </div>
	                
		        </div>
	      	</div>
	    </div>
		<?php endif; ?>
  	</div>
</div>
<div class="clearfix"></div>