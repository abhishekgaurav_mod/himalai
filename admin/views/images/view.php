<div class="mainbody">
	<div class="datalist-head">
    	<h1><?=ucfirst($this->pagetitle)?></h1>
        <ul>
        	<li><a href="<?=_URL?>galleries/index">Gallery</a></li>
            <li><a href="<?=_URL?>galleries/add">Add New Image</a></li>
		</ul>
	</div>
	<div class="datalist">
		<?php if (!empty($obj_image)): ?>
		<div class="forms brdform floatleft pad10bottom">
			<div class="formhead"><?=ucfirst($this->pagetitle)?>
				<span class="editlink"><a href="<?=_URL?>galleries/add?action=edit&id=<?=$id?>">Update</a></span>
			</div>
			<div class="fieldbox pad10top">
				<div class="story">
					<div class="storyblock">
						<div class="heading"><?=ucfirst($obj_image->title)?></div>
					</div>
					<div class="storyblock"><div class="picture"><img src="<?=_GALLERY_ORG_URL.$obj_image->imagepath?>"></div></div>
					<div class="storyblock">
						<div class="author-short">
							<div class="text">
								<div class="author pad10top">Created On : <?=date('jS F, Y g:s A',strtotime($obj_image->created_on))?></div>
								<div class="author">Updated On : <?=date('jS F, Y g:s A',strtotime($obj_image->created_on))?></div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
</div>