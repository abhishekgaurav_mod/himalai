<div class="mainbody">
	<div class="datalist-head">
    	<h1><?=ucfirst($this->pagetitle)?></h1>
        <ul>
        	<li><a href="<?=_URL?>galleries/index">Gallery</a></li>
            <li><a href="<?=_URL?>galleries/add">Add New Image</a></li>
		</ul>
	</div>
	<div class="datalist">
		<div class="forms brdform floatleft addslider">
			<form name="frmaddslider" id="frmaddslider" action="<?=_URL?>galleries/add" accept-charset="utf-8"  method="post" enctype="multipart/form-data">
			<div class="formhead"><?=ucfirst($action).' '.ucfirst($this->formhead)?> </div>
			<div class="fieldbox pad10top">
				<input type="hidden" name="action" value="<?=$action?>" />
				<?php if ($action=='edit' && !empty($obj_image)): ?>
				<input type="hidden" name="imageId" value="<?=$id?>" />
				<input type="hidden" name="imagename" value="<?=(empty($obj_image->imagepath))?null:$obj_image->imagepath?>" />
				<?php endif;?>
				<div class="grid-5050">
					<div class="grid-5050-a pad10right">
						<div class="field">
							<label class="label">Title</label>
							<input name="title" type="text" id="title" placeholder=" Title" value="<?=(!empty($obj_image->title)?$obj_image->title:'')?>" maxlength="255" autocomplete="off" />
						</div>
						<div class="field">
							<label for="imagepath" class="labelinline">Upload Image (Optional)</label>
							<input type="file" name="imagepath" id="imagepath" />
							<? if (!empty($obj_image->imagepath)):?> <a href="<?=_GALLERY_ORG_URL.$obj_image->imagepath;?>" rel="facebox">View Picture</a><? endif;?>
						</div>
					</div>
					<div class="grid-5050-b pad10left">
					</div>
				</div>
				<div class="field textaligncenter floatleft pad10top">
					<span class="loading">Please wait...</span>
					<button tabindex="11" type="submit" name="save" value="Save" class="inline" >Save</button>
					<button tabindex="12" type="reset" name="reset" value="Clear" class="inline" >Clear</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>