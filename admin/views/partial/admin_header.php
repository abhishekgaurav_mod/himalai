<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?=_TITLE?></title>

    <!-- Bootstrap -->
    <link href="<?=_THEME?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=_THEME?>font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?=_THEME?>nprogress/nprogress.css" rel="stylesheet">
    <!-- Dropzone.js -->
    <link href="<?=_THEME?>dropzone/dist/min/dropzone.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?=_THEME?>iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="<?=_THEME?>bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?=_THEME?>jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?=_THEME?>bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?=_THEME?>build/css/custom.min.css" rel="stylesheet">

	<link rel="stylesheet" href="<?=_CSS?>jquery-ui.css" type="text/css" media="screen" charset="utf-8" />
	<link rel="stylesheet" href="<?=_CSS?>fonts.css" type="text/css" media="screen" charset="utf-8" />
	<link rel="stylesheet" href="<?=_CSS?>admin.css" type="text/css" media="screen" charset="utf-8" />
	<link rel="stylesheet" href="<?=_CSS?>facebox.css" type="text/css" media="screen" charset="utf-8" />
	<link rel="stylesheet" href="<?=_PLUGINS?>datatables/media/css/jquery.dataTables.css" type="text/css" media="screen" charset="utf-8" />
	<link rel="stylesheet" href="<?=_PLUGINS?>colorpick/css/colpick.css" type="text/css"/>
	<link rel="stylesheet" href="<?=_PLUGINS?>token/token-input.css" type="text/css" />
	<link rel="stylesheet" href="<?=_PLUGINS?>token/token-input-facebook.css" type="text/css" />
	<link rel="shortcut icon" href="<?=_IMAGES?>favicon.png">
	<script type="text/javascript" src="<?=_JS?>jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="<?=_PLUGINS?>ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?=_JS?>jquery-ui.js"></script>
	<script type="text/javascript">
	var _URL	=	'<?=_URL?>';
	</script>
	
</head>
<body class="nav-md">
	<?php if (!empty($_SESSION['adminId'])):?>

    <div class="container body">
      	<div class="main_container">
      	
	        <div class="col-md-3 left_col">
	          <div class="left_col scroll-view">
	            <div class="navbar nav_title" style="border: 0;">
	              <a href="<?=_URL?>" class="site_title"><i class="fa fa-home"></i> <span>Himalai</span></a>
	            </div>

	            <div class="clearfix"></div>

	            <!-- menu profile quick info -->
	            <div class="profile clearfix">
	              <div class="profile_pic">
	                <img src="<?=_IMAGES?>profile-img.png" alt="<?=$_SESSION['name']?>" class="img-circle profile_img">
	              </div>
	              <div class="profile_info">
	                <span>Welcome,</span>
	                <h2><?=$_SESSION['name']?></h2>
	              </div>
	            </div>

	            <br />

	            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	              	<div class="menu_section">
		                <h3>General</h3>
		                <ul class="nav side-menu">
		                  	<li><a><i class="fa fa-home"></i> DashBoard <span class="fa fa-chevron-down"></span></a>
			                    <ul class="nav child_menu">
			                      <li><a href="<?=_URL?>" <?=($this->current=='dashboard')?'class="current"':''?>>DashBoard</a></li>
			                    </ul>
		                  	</li>
		                  	<li><a><i class="fa fa-edit"></i> CMS Pages <span class="fa fa-chevron-down"></span></a>
			                    <ul class="nav child_menu">
									<li><a href="<?=_URL?>cmscontents/index?type=home" <?=($this->current=='home')?'class="current"':''?>>Home</a></li>
									<li><a href="<?=_URL?>cmscontents/index?type=aboutus" <?=($this->current=='aboutus')?'class="current"':''?>>About Us</a></li>
									<li><a href="<?=_URL?>cmscontents/index?type=whyhimalai" <?=($this->current=='whyhimalai')?'class="current"':''?>>Why Himalai</a></li>
									<li><a href="<?=_URL?>cmscontents/index?type=examtrends" <?=($this->current=='examtrends')?'class="current"':''?>>Exam Trends</a></li>
									<li><a href="<?=_URL?>cmscontents/index?type=methodologyandachievements" <?=($this->current=='methodologyandachievements')?'class="current"':''?>>Teaching Methodology and Achievements</a></li>
			                    </ul>
		                  	</li>
		                  	<li><a><i class="fa fa-edit"></i> Indian Civil Service Exam <span class="fa fa-chevron-down"></span></a>
			                    <ul class="nav child_menu">
			                      	<li><a href="<?=_URL?>civilserviceprofiles" <?=($this->current=='civilserviceprofiles')?'class="current"':''?>>Civil Service Profile</a></li>
									<li><a href="<?=_URL?>cmscontents/index?type=stagesofiasexam" <?=($this->current=='stagesofiasexam')?'class="current"':''?>>Stages and Syllabus</a></li>
									<li><a href="<?=_URL?>cmscontents/index?type=generalstudiesmains" <?=($this->current=='generalstudiesmains')?'class="current"':''?>>Syllabus for Mains</a></li>
			                      	<li><a href="<?=_URL?>gsoptionalsubjects" <?=($this->current=='gsoptionalsubjects')?'class="current"':''?>>List Of Optional Subject</a></li>
									<li><a href="<?=_URL?>cmscontents/index?type=iasgeneralinstructions" <?=($this->current=='iasgeneralinstructions')?'class="current"':''?>>General Instructions</a></li>
									<li><a href="<?=_URL?>cmscontents/index?type=iasexameligibility" <?=($this->current=='iasexameligibility')?'class="current"':''?>>Eligibility Criteria</a></li>
									<li><a href="<?=_URL?>cmscontents/index?type=iasserviceallocation" <?=($this->current=='iasserviceallocation')?'class="current"':''?>>Service Allocation</a></li>
									<li><a href="<?=_URL?>cmscontents/index?type=iasreservationissues" <?=($this->current=='iasreservationissues')?'class="current"':''?>>Reservation Issues</a></li>
									<li><a href="<?=_URL?>cmscontents/index?type=iasphcategory" <?=($this->current=='iasphcategory')?'class="current"':''?>>PH Category</a></li>
									<li><a href="<?=_URL?>cmscontents/index?type=iasmedicalexamofcandidates" <?=($this->current=='iasmedicalexamofcandidates')?'class="current"':''?>>Medical Exam</a></li>
			                    </ul>
		                  	</li>
		                  	<li><a><i class="fa fa-edit"></i> KAS Exam <span class="fa fa-chevron-down"></span></a>
			                    <ul class="nav child_menu">
									<li><a href="<?=_URL?>cmscontents/index?type=kasexam" <?=($this->current=='kasexam')?'class="current"':''?>>KAS Exam</a></li>
									<li><a href="<?=_URL?>cmscontents/index?type=kaseligibility" <?=($this->current=='kaseligibility')?'class="current"':''?>>KAS Eligibility</a></li>
									<li><a href="<?=_URL?>cmscontents/index?type=stagesofkasexam" <?=($this->current=='stagesofkasexam')?'class="current"':''?>>Stages of KAS Exam</a></li>
			                    </ul>
		                  	</li>
		                  	<li><a><i class="fa fa-edit"></i> Slider <span class="fa fa-chevron-down"></span></a>
			                    <ul class="nav child_menu">
			                      <li><a href="<?=_URL?>sliders" <?=($this->current=='slider')?'class="current"':''?>>Slider</a></li>
			                    </ul>
		                  	</li>
		                  	<li><a><i class="fa fa-edit"></i> Testimonials <span class="fa fa-chevron-down"></span></a>
			                    <ul class="nav child_menu">
			                      <li><a href="<?=_URL?>testimonials" <?=($this->current=='testimonials')?'class="current"':''?>>Testimonials</a></li>
			                    </ul>
		                  	</li>
		                  	<li><a><i class="fa fa-edit"></i> Questions <span class="fa fa-chevron-down"></span></a>
			                    <ul class="nav child_menu">
			                      <li><a href="<?=_URL?>questionyears" <?=($this->current=='questionyears')?'class="current"':''?>>Questions</a></li>
			                    </ul>
		                  	</li>
		                  	<li><a><i class="fa fa-edit"></i> Gallery <span class="fa fa-chevron-down"></span></a>
			                    <ul class="nav child_menu">
			                      <li><a href="<?=_URL?>galleries" <?=($this->current=='gallery')?'class="current"':''?>>Gallery</a></li>
			                    </ul>
		                  	</li>
		                  	<li><a><i class="fa fa-edit"></i> Enquiries <span class="fa fa-chevron-down"></span></a>
			                    <ul class="nav child_menu">
			                      <li><a href="<?=_URL?>enquiries" <?=($this->current=='enquiries')?'class="current"':''?>>Enquiries</a></li>
			                    </ul>
		                  	</li>

		                </ul>
	              	</div>

		        </div>


	            <div class="sidebar-footer hidden-small">
	              <!-- <a data-toggle="tooltip" data-placement="top" title="Settings">
	                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
	              </a>
	              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
	                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
	              </a>
	              <a data-toggle="tooltip" data-placement="top" title="Lock">
	                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
	              </a> -->
	              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?=_URL?>admins/signout">
	                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
	              </a>
	            </div>
	          </div>
	        </div>

	        <div class="top_nav">
	          <div class="nav_menu">
	            <nav>
	              <div class="nav toggle">
	                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
	              </div>

	              <ul class="nav navbar-nav navbar-right">
	                <li class="">
	                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
	                    <img src="<?=_IMAGES?>profile-img.png" alt="<?=$_SESSION['name']?>"><?=$_SESSION['name']?>
	                    <span class=" fa fa-angle-down"></span>
	                  </a>
	                  <ul class="dropdown-menu dropdown-usermenu pull-right">
	                    <li><a href="<?=_URL?>admins/view?id=<?=$_SESSION['adminId']?>"> Profile</a></li>
	                    <li>
	                      <a href="<?=_URL?>admins/changepassword">
	                        <span>Change Password</span>
	                      </a>
	                    </li>
	                    <li><a href="<?=_URL?>admins/signout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
	                  </ul>
	                </li>
	              </ul>
	            </nav>
	          </div>
	        </div>


	<?php endif; ?>