    <?php if (!empty($_SESSION['adminId'])):?>
        <footer>
            <div class="pull-left footerLogo"></div>
            <div class="pull-right">
                Himalai Admin Panel<br>Powered by <a href="http://www.motherofdesign.in" target="_blank">Mother of Design</a>
            </div>
            <div class="clearfix"></div>
        </footer>

      </div>
    </div>
    <!-- jQuery -->
    <script src="<?=_THEME?>jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?=_THEME?>bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?=_THEME?>fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?=_THEME?>nprogress/nprogress.js"></script>
    <!-- Dropzone.js -->
    <script src="<?=_THEME?>dropzone/dist/min/dropzone.min.js"></script>
    <!-- Chart.js -->
    <script src="<?=_THEME?>Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?=_THEME?>gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?=_THEME?>bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?=_THEME?>iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?=_THEME?>skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?=_THEME?>Flot/jquery.flot.js"></script>
    <script src="<?=_THEME?>Flot/jquery.flot.pie.js"></script>
    <script src="<?=_THEME?>Flot/jquery.flot.time.js"></script>
    <script src="<?=_THEME?>Flot/jquery.flot.stack.js"></script>
    <script src="<?=_THEME?>Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?=_THEME?>flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?=_THEME?>flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?=_THEME?>flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?=_THEME?>DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?=_THEME?>jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?=_THEME?>jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?=_THEME?>jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?=_THEME?>moment/min/moment.min.js"></script>
    <script src="<?=_THEME?>bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?=_THEME?>build/js/custom.min.js"></script>
    
    <?php endif; ?>
    <script type="text/javascript" src="<?=_PLUGINS?>datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?=_PLUGINS?>colorpick/js/colpick.js"></script>
    <script type="text/javascript" src="<?=_JS?>jquery.form.min.js"></script>
    <script type="text/javascript" src="<?=_JS?>facebox.js"></script>
    <script type="text/javascript" src="<?=_JS?>notify.min.js"></script>
    <script type="text/javascript" src="<?=_PLUGINS?>token/jquery.tokeninput.js"></script>
    <script type="text/javascript" src="<?=_JS?>admin.js"></script>

</body>
</html>
