<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		            	<a href="<?=_URL?>testimonials/index<?=(($status=='Y')?'?status=n':'')?>" class="btn btn-primary"><?=(($status=='Y')?'Disabled ':'Enabled ').'Testimonials'?></a>
		            	<a href="<?=_URL?>testimonials/add" class="btn btn-primary">Add Testimonials</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_content">
						
						<div class="datalist">
					    	<table class="display" id="example">
					        <thead>
					        	<tr>
					        		<th width="150">Testimonial Id</th>
					                <th>Name</th>
					                <th width="180">Testimonial Type</th>
					                <th width="180">Student Type</th>
					                <th width="80">Order</th>
					                <th width="80">Date</th>
					                <th width="150">Created On</th>
					                <th width="150">Updated On</th>
					                <th>Enabled</th>
					                <th width="150">Actions</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="7" class="dataTables_empty">Loading data from server</td>
								</tr>
							</tbody>
					        </table>
						</div>
		            
		          	</div>
		        </div>
	      	</div>

	    </div>
  	</div>
</div>
<div class="clearfix"></div>


<script type="text/javascript">
$(document).ready(function() {
	var page	=	0;
    oTable = $('#example').dataTable( {
    	"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "<?=$ajaxSource?>",
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({ "name": "status", "value": "<?=$status?>" });
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			}).done(function() {});
		},
		"fnDrawCallback": function () { page	=	this.fnPagingInfo().iPage; },
        "sPaginationType": "full_numbers",
        "iDisplayLength": <?=_DATA_LIMIT?>,
        "bProcessing": true,
        "sDom": 'CRTfrtip',
    	"aoColumns": [null,null,
						{	
							"fnRender": function (oObj) {
								var html;
								if (oObj.aData[2] == 'CC') { html = 'Cleared Candidate'; }
								else if (oObj.aData[2] == 'S') { html = 'Student'; }
								else { html = '-' }
								return html;
							}
						},
						{	
							"fnRender": function (oObj) {
								var html;
								if (oObj.aData[3] == 'PE') { html = 'Preparatory Exam'; }
								else if (oObj.aData[3] == 'CRC') { html = 'Crash Course'; }
								else if (oObj.aData[3] == 'SC') { html = 'Special Class'; }
								else if (oObj.aData[3] == 'TDC') { html = 'Test / Discussion Class'; }
								else if (oObj.aData[3] == '2017TC') { html = '2017 Tests & Classes'; }
								else { html = '-' }
								return html;
							}
						},null,null,null,null,{ "bVisible": false},
						{	"sName": "id",
							"bSearchable": false,
							"bSortable": false,
							"fnRender": function (oObj) {
								var html	=	'<a href="<?=_URL?>testimonials/view?id='+oObj.aData[0]+'">View</a><span class="separator">|</span>';
								html	+=	'<a href="<?=_URL?>testimonials/add?action=edit&id='+oObj.aData[0]+'">Edit</a><span class="separator">|</span>';
								html	+=	'<a onclick="javascript:deleters(\'Testimonial\', \''+oObj.aData[0]+'\',\''+((oObj.aData[7]=='Y')?'N':'Y')+'\',\''+((oObj.aData[7]=='Y')?'Disable':'Enable')+'\')">'+((oObj.aData[7]=='Y')?'Disable':'Enable')+'</a>';
								return html;
							}
						}
    	],
        "aaSorting": [[ 0, "desc" ]],
    });
});
</script>