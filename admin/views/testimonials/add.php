<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		            	<a href="<?=_URL?>testimonials/" class="btn btn-primary">Testimonials</a>
		            	<a href="<?=_URL?>testimonials/add" class="btn btn-primary">Add Testimonial</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<h2><?=ucfirst($action).' '.ucfirst($this->formhead)?></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="forms addslider">

							<form name="frmaddslider" id="frmaddslider" action="<?=_URL?>testimonials/add" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask">
								<input type="hidden" name="action" value="<?=$action?>" />
								<input type="hidden" name="imagename" id="imagename" value="<?=(!empty($object->category_icon)?$object->category_icon:'')?>" />
								<?php if ($action=='edit' && !empty($object)): ?>
								<input type="hidden" name="testimonialId" value="<?=$id?>" />
								<input type="hidden" name="authorImageId" value="<?=(empty($object->authorimagepath))?null:$object->authorimagepath?>" />
								<input type="hidden" name="testimonialImageId" value="<?=(empty($object->testimonialimagepath))?null:$object->testimonialimagepath?>" />
								<?php endif;?>

							    <div class="row">
							      	<div class="col-md-4 col-xs-12">

										<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label for="name">Name</label>
											<input name="name" type="text" id="name" placeholder="Name" value="<?=(!empty($object->name)?htmlentities($object->name):'')?>" maxlength="500" autocomplete="off" class="form-control has-feedback-left" />
				                        	<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
										</div>

										<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label for="tags">Tags (Optional)</label>
											<input name="tags" type="text" id="tags" placeholder="Tags (Optional)" value="<?=(!empty($object->tags)?htmlentities($object->tags):'')?>" maxlength="255" autocomplete="off" class="form-control has-feedback-left" />
				                        	<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label>Testimonial Type : </label>
											<select id="testimonial_type" name="testimonial_type" class="form-control has-feedback-left">
												<option value="">-Select Testimonial Type-</option>
												<option value="CC" <?=(($object->testimonial_type == 'CC')?'selected':'')?> >Cleared Candidate</option>
												<option value="S" <?=(($object->testimonial_type == 'S')?'selected':'')?> >Student</option>
											</select>
	                    					<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label>Student Type : </label>
											<select id="student_type" name="student_type" class="form-control has-feedback-left">
												<option value="">-Select Student Type-</option>
												<option value="PE" <?=(($object->student_type == 'PE')?'selected':'')?> >Preparatory Exam</option>
												<option value="CRC" <?=(($object->student_type == 'CRC')?'selected':'')?> >Crash Course</option>
												<option value="SC" <?=(($object->student_type == 'SC')?'selected':'')?> >Special Class</option>
												<option value="TDC" <?=(($object->student_type == 'TDC')?'selected':'')?> >Test / Discussion Class</option>
												<option value="2017TC" <?=(($object->student_type == '2017TC')?'selected':'')?> >2017 Tests & Classes</option>
											</select>
	                    					<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label for="testimonial_order">Testimonial Order</label>
											<input name="testimonial_order" type="text" id="testimonial_order" placeholder="Testimonial Order" value="<?=(!empty($object->testimonial_order)?htmlentities($object->testimonial_order):'')?>" maxlength="500" autocomplete="off" class="form-control has-feedback-left" />
				                        	<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label class="labelinline">Author Image </label>
											<input type="file" name="authorimagepath" /><?=(!empty($object->authorimagepath)?'<a href="'._TESTIMONIAL_AUTHOR_ORG_URL.$object->authorimagepath.'" rel="facebox">View Picture</a>':'')?>
										</div>
										<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label class="labelinline">Testimonial Image </label>
											<input type="file" name="testimonialimagepath" /><?=(!empty($object->testimonialimagepath)?'<a href="'._TESTIMONIAL_ORG_URL.$object->testimonialimagepath.'" rel="facebox">View Picture</a>':'')?>
										</div>

										<div class="field col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label class="labelinline">Date</label>
											<input name="publishing_date" type="text" id="publishing_date" placeholder="Publishing Date" value="<?=(!empty($object->publishing_date)?$object->publishing_date:'')?>" autocomplete="off" />
										</div>

									</div>
							      	<div class="col-md-8 col-xs-12">
										<div class="field padzero">
											<label class="label">Description</label>
											<textarea name="description" id="description" rows="7" cols="50" placeholder="Testimonial Text..."><?=(!empty($object->description)?stripcslashes($object->description):'')?></textarea>
										</div>

										<div class="clearfix"></div>
				                      	<div class="ln_solid"></div>
				                      	<div class="form-group">
											<div class="field col-md-9 col-sm-9 col-xs-12">
												<span class="loading">Please wait...</span>
												<button type="submit" name="save" value="Save" class=" inline btn btn-success" >Save</button>
												<button type="reset" name="reset" value="Clear" class="inline btn btn-success" >Clear</button>
											</div>
				                      	</div>
									</div>
								</div>
							</form>

						</div>		            
		          	</div>
		        </div>
	      	</div>
	    </div>

  	</div>
</div>
<div class="clearfix"></div>

<script type="text/javascript">
$(function() {
	$("#publishing_date").datepicker({ dateFormat: "dd-mm-yy" }).val();
});
/*CKEDITOR.replace('description',{allowedContent:true,resize_enabled:'false',resize_maxHeight:'280',resize_minHeight: '280',toolbar:[{name:'document',items:['Source','-','NewPage','Preview','-','Templates']},{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },{name:'basicstyles',items:['Bold','Italic']}]});*/
var ckConfig = {
		allowedContent: true, resize_enabled:'false',width: '100%', height: '350px', resize_maxHeight:'320', resize_minHeight: '600',
		toolbar:[
			{ name:'document', items:['source'] },
			{ name:'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
			{ name:'basicstyles', items:['Bold','Italic','Underline'] },
			'/',
	       	{ name:'insert', items: ['SpecialChar', 'Maximize', 'Source'] },
			{ name:'styles', items : ['Styles', 'Format','FontSize'] }
		]
	};
CKEDITOR.config.forcePasteAsPlainText 		= true;
CKEDITOR.replace('description',ckConfig);
</script>
