<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		            	<a href="<?=_URL?>testimonials/" class="btn btn-primary">Testimonials</a>
		            	<a href="<?=_URL?>testimonials/add" class="btn btn-primary">Add Testimonial</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

		<?php if (!empty($object)): ?>
	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<ul class="nav navbar-right panel_toolbox">
							<a href="<?=_URL?>testimonials/add?action=edit&id=<?=$id?>" class="btn btn-primary">Update</a>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
	      					<div class="col-md-6 col-xs-12">
								<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
										<h4><b>Author Name :-</b> <?=ucfirst($object->name)?></h4>
										<h4><b>Tags :-</b> <?=ucfirst($object->tags)?></h4>
										<h4><b>Testimonial Type :-</b> 
											<?php
												if ($object->testimonial_type == 'CC') { echo 'Cleared Candidate'; }
												else if ($object->testimonial_type == 'S') { echo 'Student'; }
												else { echo '-'; }
											?>
										</h4>
										<h4><b>Student Type :-</b> 
											<?php
												if ($object->student_type == 'PE') { echo 'Preparatory Exam'; }
												else if ($object->student_type == 'CRC') { echo 'Crash Course'; }
												else if ($object->student_type == 'SC') { echo 'Special Class'; }
												else if ($object->student_type == 'TDC') { echo 'Test / Discussion Class'; }
												else if ($object->student_type == '2017TC') { echo '2017 Tests & Classes'; }
												else { echo '-'; }
											?>
										</h4><br>
										<h4><b>Publishing Date :-</b> <?=date('jS F, Y',strtotime($object->publishing_date))?></h4>
										<h4><b>Created On :-</b> <?=date('jS F, Y g:s A',strtotime($object->created_on))?></h4><br><br>
										<?php if (!empty($object->authorimagepath)) { ?>
				                      		<div><img src="<?=_TESTIMONIAL_AUTHOR_ORG_URL.$object->authorimagepath?>" class="img-responsive"></div><br>
				                      	<?php } ?>
										<?php if (!empty($object->testimonialimagepath)) { ?>
				                      		<div><img src="<?=_TESTIMONIAL_ORG_URL.$object->testimonialimagepath?>" class="img-responsive"></div>
				                      	<?php } ?>
				                    </div>
		      					</div>
		      				</div>
	      					<div class="col-md-6 col-xs-12">
	      						<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
										<h3><b>Description :-</b> </h3>
				                      	<div><?=stripcslashes($object->description)?></div>
				                    </div>
		      					</div>
		      				</div>
						</div>
	                </div>
	                
		        </div>
	      	</div>
	    </div>
		<?php endif; ?>
  	</div>
</div>
<div class="clearfix"></div>
