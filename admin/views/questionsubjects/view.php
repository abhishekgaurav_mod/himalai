<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle.' ('.$questionyearName.')')?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-6 col-sm-6 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		          		<a href="<?=_URL?>questionyears/index" class="btn btn-primary">Question Years</a>
						<a href="<?=_URL?>questionsubjects/index?id=<?=$questionyearId?>" class="btn btn-primary"><?=$obj_questionsubject->question_year?> Question</a>
						<a href="<?=_URL?>questionsubjects/add?questionyearId=<?=$questionyearId?>" class="btn btn-primary">Add <?=$obj_questionsubject->question_year?> Question</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

		<?php if (!empty($obj_questionsubject)): ?>
	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<ul class="nav navbar-right panel_toolbox">
							<a href="<?=_URL?>questionsubjects/add?action=edit&questionsubjectId=<?=$id?>&questionyearId=<?=$obj_questionsubject->questionyear_id?>" class="btn btn-primary">Update</a>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
	      					<div class="col-md-6 col-xs-12">
								<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
				                    <table class="table" width="100%" border="0">
											<tr>
												<td><b>Question Subject Id</b></td>
												<td>: <?=$obj_questionsubject->questionsubject_id?></td>
											</tr>
											<tr>
												<td><b>Question Subject</b></td>
												<td>: <?=$obj_questionsubject->questionsubject_name?></td>
											</tr>
											<tr>
												<td><b>Question Type</b></td>
												<td>: 
													<?php
														if ($obj_questionsubject->question_type == 'GS') { echo 'General Subject'; }
														else if ($obj_questionsubject->question_type == 'POS') { echo 'Prelims Optional Subject'; }
														else if ($obj_questionsubject->question_type == 'MIL') { echo 'Mains Indian Language'; }
														else if ($obj_questionsubject->question_type == 'MOS') { echo 'Mains Optional Subject'; }
														else if ($obj_questionsubject->question_type == 'MLS') { echo 'Mains Literature Subject'; }
														else { echo '-'; }
													?>
												</td>
											</tr>
											<tr>
												<td><b>Subject Thumb</b></td>
												<td>: <?=(!empty($obj_questionsubject->thumb)?'<a href="'._QUESTION_THUMB_ORG_URL.$obj_questionsubject->thumb.'" rel="facebox">View Picture</a>':'Not Available')?></td>
											</tr>
											<tr>
												<td><b>Question Paper</b></td>
												<td>: <?=(!empty($obj_questionsubject->question_paper)?'<a href="'._QUESTION_ORG_URL.$obj_questionsubject->question_paper.'" target="_blank">View Question Paper</a>':'Not Available')?></td>
											</tr>
											<tr>
												<td><b>Question Year Title</b></td>
												<td>: <?=$obj_questionsubject->questionyear_name?></td>
											</tr>

											<tr>
												<td><b>Enabled</b></td>
												<td>: <?=($obj_questionsubject->enabled=='Y')?'Yes':'No'?></td>
											</tr>
											<tr>
												<td><b>Created On</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($obj_questionsubject->created_on))?></td>
											</tr>
											<tr>
												<td><b>Updated On</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($obj_questionsubject->updated_on))?></td>
											</tr>
										</table>
				                    </div>
		      					</div>
		      				</div>
						</div>
	                </div>
	                
		        </div>
	      	</div>
	    </div>
		<?php endif; ?>
  	</div>
</div>
<div class="clearfix"></div>