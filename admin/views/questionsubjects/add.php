<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-6 col-sm-6 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		          		<a href="<?=_URL?>questionyears/index" class="btn btn-primary">Question Years</a>
						<a href="<?=_URL?>questionsubjects/index?id=<?=$questionyearId?>" class="btn btn-primary"><?=$obj_questionyear->question_year?> Questions</a>
						<a href="<?=_URL?>questionsubjects/add?questionyearId=<?=$questionyearId?>" class="btn btn-primary">Add New Question</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

	    <div class="row">
	      	<div class="col-md-6 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<h2><?=ucfirst($action).' '.ucfirst($this->formhead)?></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="forms addslider">

							<form name="frmaddslider" id="frmaddslider" action="<?=_URL?>questionsubjects/add" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask">
								<div class="fieldbox pad10top">
									<input type="hidden" name="action" value="<?=$action?>" />
									<?php if ($action=='edit' && !empty($obj_questionsubject)): ?>
									<input type="hidden" name="questionsubjectId" value="<?=$questionsubjectId?>" />
									<input type="hidden" name="thumbId" value="<?=(empty($obj_questionsubject->thumb))?null:$obj_questionsubject->thumb?>" />
									<input type="hidden" name="question_paperId" value="<?=(empty($obj_questionsubject->question_paper))?null:$obj_questionsubject->question_paper?>" />
									<?php endif;?>
									<input type="hidden" name="questionyearId" value="<?=$questionyearId?>" />
									<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
										<label for="title">Question Subject : </label>
										<input name="title" type="text" id="title" placeholder="Question Subject Name" value="<?=(!empty($obj_questionsubject->questionsubject_name)?$obj_questionsubject->questionsubject_name:'')?>" maxlength="255" autocomplete="off" class="form-control has-feedback-left" />
	                        			<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
										<label>Question Type : </label>
										<select id="question_type" name="question_type" class="form-control has-feedback-left">
											<option value="">-Select Question Type-</option>
											<option value="GS" <?=(($obj_questionsubject->question_type == 'GS')?'selected':'')?> >General Subject</option>
											<option value="POS" <?=(($obj_questionsubject->question_type == 'POS')?'selected':'')?> >Prelims Optional Subject</option>
											<option value="MIL" <?=(($obj_questionsubject->question_type == 'MIL')?'selected':'')?> >Mains Indian Language</option>
											<option value="MOS" <?=(($obj_questionsubject->question_type == 'MOS')?'selected':'')?> >Mains Optional Subject</option>
											<option value="MLS" <?=(($obj_questionsubject->question_type == 'MLS')?'selected':'')?> >Mains Literature Subject</option>
										</select>
                    					<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
										<label>Subject Thumb :  </label>
										<input type="file" name="thumb" /><?=(!empty($obj_questionsubject->thumb)?'<a href="'._QUESTION_THUMB_ORG_URL.$obj_questionsubject->thumb.'" rel="facebox">View Picture</a>':'')?>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
										<label>Question Paper :  </label>
										<input type="file" name="question_paper" /><?=(!empty($obj_questionsubject->question_paper)?'<a href="'._QUESTION_ORG_URL.$obj_questionsubject->question_paper.'" target="_blank">View Question Paper</a>':'')?>
									</div>

									<div class="clearfix"></div>
			                      	<div class="ln_solid"></div>
			                      	<div class="form-group">
										<div class="field col-md-9 col-sm-9 col-xs-12">
											<span class="loading">Please wait...</span>
											<button type="submit" name="save" value="Save" class=" inline btn btn-success" >Save</button>
											<button type="reset" name="reset" value="Clear" class="inline btn btn-success" >Clear</button>
										</div>
			                      	</div>
								</div>
							</form>

						</div>		            
		          	</div>
		        </div>
	      	</div>

	    </div>
  	</div>
</div>
<div class="clearfix"></div>