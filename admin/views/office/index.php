<div class="mainbody">
	<div class="datalist-head">
    	<h1><?=ucfirst($this->pagetitle)?></h1>
        <ul>
        	<li><a href="<?=_URL?>casestudies/index<?=(($status=='Y')?'?status=n':'')?>"><?=(($status=='Y')?'Disabled ':'Enabled ').'Casestudies'?></a></li>
            <li><a href="<?=_URL?>casestudies/add">Add New Casestudy</a></li>
		</ul>
	</div>
	<div class="datalist">
    	<table class="display" id="example">
        <thead>
        	<tr><th width="100">Casestudy Id</th>
                <th>Title</th>
                <th width="160">Type</th>
                <th width="110">Created On</th>
                <th width="110">Updated On</th>
                <th>Enabled</th>
                <th width="120">Actions</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="5" class="dataTables_empty">Loading data from server</td>
			</tr>
		</tbody>
        </table>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	var page	=	0;
    oTable = $('#example').dataTable( {
    	"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "<?=$ajaxSource?>",
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({ "name": "status", "value": "<?=$status?>" });
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			}).done(function() {});
		},
		"fnDrawCallback": function () { page	=	this.fnPagingInfo().iPage; },
        "sPaginationType": "full_numbers",
        "iDisplayLength": <?=_DATA_LIMIT?>,
        "bProcessing": true,
        "sDom": 'CRTfrtip',
    	"aoColumns": [null,null,null,null,null,{ "bVisible": false},
						{	"sName": "id",
							"bSearchable": false,
							"bSortable": false,
							"fnRender": function (oObj) {
								var html	=	'<a href="<?=_URL?>casestudies/view?id='+oObj.aData[0]+'">View</a><span class="separator">|</span>';
								html	+=	'<a href="<?=_URL?>casestudies/add?action=edit&id='+oObj.aData[0]+'">Edit</a><span class="separator">|</span>';
								html	+=	'<a onclick="javascript:deleters(\'Resource\', \''+oObj.aData[0]+'\',\''+((oObj.aData[5]=='Y')?'N':'Y')+'\',\''+((oObj.aData[5]=='Y')?'Disable':'Enable')+'\')">'+((oObj.aData[5]=='Y')?'Disable':'Enable')+'</a>';
								return html;
							}
						}
    	],
        "aaSorting": [[ 0, "desc" ]],
    });
});
</script>