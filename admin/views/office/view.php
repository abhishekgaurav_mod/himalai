<div class="mainbody">
	<div class="datalist-head">
    	<h1><?=ucfirst($this->pagetitle)?></h1>
        <ul>
        	<li><a href="<?=_URL?>casestudies/">Casestudies</a></li>
            <li><a href="<?=_URL?>casestudies/add">Add New Casestudy</a></li>
		</ul>
	</div>
	<div class="datalist">
		<?php if (!empty($obj_casestudy)): ?>
		<div class="forms brdform floatleft pad10bottom">
			<div class="formhead"><?=ucfirst($obj_casestudy->title)?>
				<span class="editlink"><a href="<?=_URL?>casestudies/add?action=edit&id=<?=$id?>">Update</a></span>
			</div>
			<div class="fieldbox pad10top">
				<table width="100%" border="0">
				<tr>
					<td align="left" valign="top" width="50%" class="padzero">
						<table width="100%" border="0">
						<tr>
							<td rowspan="6" width="110" class="picture" valign="middle" align="center">
							<?php if(!empty($obj_casestudy->imagepath)): ?>
								<a href="<?=_CASESTUDY_ORG_URL.$obj_casestudy->imagepath?>" rel="facebox"><img src="<?=_CASESTUDY_ORG_URL.$obj_casestudy->imagepath?>" /></a>
							<?php else:?>
							No Image
							<?php endif;?>
							</td>
							<td class="label" width="150"> Title</td>
							<td>: <?=ucfirst($obj_casestudy->title)?></td>
						</tr>
						<tr>
							<td class="label">Sub-Title</td>
							<td>: <?=ucfirst($obj_casestudy->sub_title)?></td>
						</tr>
						<tr>
							<td class="label">Resource Type</td>
							<td>: <?=ucfirst($obj_casestudy->type)?></td>
						</tr>
						<tr>
							<td class="label">External Link </td>
							<td>:<a href="<?=$obj_casestudy->external_link?>" target="_blank"> <?=(!empty($obj_casestudy->external_link))?'Click Link':''?></a></td>
						</tr>
						</table>
					</td>
					<td align="left" valign="top">
						<table width="100%" border="0">
						<tr>
							<td class="label">Enabled</td>
							<td colspan="2">: <?=($obj_casestudy->enabled=='Y')?'Yes':'No'?></td>
						</tr>
						<tr>
							<td class="label" width="150">Created On</td>
							<td>: <?=date('jS F, Y g:s A',strtotime($obj_casestudy->created_on))?></td>
						</tr>
						<tr>
							<td class="label">Updated On</td>
							<td>: <?=date('jS F, Y g:s A',strtotime($obj_casestudy->updated_on))?></td>
						</tr>
						</table>
					</td>
				</tr>
				<tr><td class="label padzero" colspan="2" height="5"></td></tr>
				<tr><td class="label padzero" colspan="2">Description: </td></tr>
				<tr><td colspan="2" class="padzero"><div class="bordersolidtop pad10top"><?=str_replace(PHP_EOL, '<br />', $obj_casestudy->description)?></div></td></tr>
				</table>
			</div>
		</div>
		<?php endif; ?>
	</div>
</div>