<div class="mainbody">
	<div class="datalist-head">
    	<h1><?=ucfirst($this->pagetitle)?></h1>
        <ul>
        	<li><a href="<?=_URL?>casestudies/">Casestudies</a></li>
            <li><a href="<?=_URL?>casestudies/add">Add New Casestudy</a></li>
		</ul>
	</div>
	<div class="datalist">
		<div class="forms brdform floatleft addcasetudy">
			<form name="frmaddcasetudy" id="frmaddcasetudy" action="<?=_URL?>casestudies/add" accept-charset="utf-8"  method="post" enctype="multipart/form-data">
			<div class="formhead"><?=ucfirst($action).' '.ucfirst($this->formhead)?> </div>
			<div class="fieldbox pad10top">
				<input type="hidden" name="action" value="<?=$action?>" />
				<?php if ($action=='edit' && !empty($obj_casestudy)): ?>
				<input type="hidden" name="casestudyId" value="<?=$id?>" />
				<input type="hidden" name="imagename" value="<?=(empty($obj_casestudy->imagepath))?null:$obj_casestudy->imagepath?>" />
				<?php endif;?>
				<div class="grid-5050">
					<div class="grid-5050-a pad10right bordersolidright">
						<div class="field">
							<label class="label">Select Resource Type</label>
							<select name="type" id="type" required="">
								<option value="">Select Type</option>
								<option value="Case Study" <?=(!empty($obj_casestudy->type)?(($obj_casestudy->type=='Case Study')?' selected="selected"':''):'')?>>Case Study</option>
								<option value="White Papper" <?=(!empty($obj_casestudy->type)?(($obj_casestudy->type=='White Papper')?' selected="selected"':''):'')?>>White Paper</option>
							</select>
						</div>
						<div class="field">
							<label class="label">Title</label>
							<input name="title" type="text" id="title" placeholder="Case study Title" value="<?=(!empty($obj_casestudy->title)?$obj_casestudy->title:'')?>" maxlength="255" autocomplete="off" />
						</div>
						<div class="field">
							<label class="label">Sub Title (Optional)</label>
							<input name="sub_title" type="text" id="sub_title" placeholder="Case study Sub Title" value="<?=(!empty($obj_casestudy->sub_title)?$obj_casestudy->sub_title:'')?>" maxlength="255" autocomplete="off" />
						</div>
						<div class="field">
							<label class="label">External Link (Optional)</label>
							<input name="external_link" type="text" id="external_link" placeholder="Add External Link (Optional)" value="<?=(!empty($obj_casestudy->title)?$obj_casestudy->title:'')?>" maxlength="255" autocomplete="off" />
						</div>
						<div class="field">
							<label for="imagepath" class="labelinline">Upload Image (Optional)</label>
							<input type="file" name="imagepath" id="imagepath" />
							<? if (!empty($obj_casestudy->imagepath)):?> <a href="<?=_CASESTUDY_ORG_URL.$obj_casestudy->imagepath;?>" rel="facebox">View Picture</a><? endif;?>
						</div>
					</div>
					<div class="grid-5050-b pad10left">
						<div class="field padzero">
							<label class="label">Description</label>
							<textarea name="description" id="description" rows="7" cols="50" placeholder="Case study Text..."><?=(!empty($obj_casestudy->description)?$obj_casestudy->description:'')?></textarea>
						</div>
					</div>
				</div>
				<div class="field textaligncenter floatleft pad10top">
					<span class="loading">Please wait...</span>
					<button tabindex="11" type="submit" name="save" value="Save" class="inline" >Save</button>
					<button tabindex="12" type="reset" name="reset" value="Clear" class="inline" >Clear</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
CKEDITOR.replace('description',{allowedContent:true,resize_enabled:'false',resize_maxHeight:'280',resize_minHeight: '280',toolbar:[{name:'document',items:['Source','-','NewPage','Preview','-','Templates']},{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },{name:'basicstyles',items:['Bold','Italic']}]});
</script>