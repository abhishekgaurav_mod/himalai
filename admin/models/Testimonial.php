<?php
class Testimonial extends DBSource
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function addTestimonials($object)
	{
		try
		{
			$sql = 'INSERT INTO testimonials
					(
						name
						, description
						, tags
            , testimonial_type
            , student_type
            , testimonial_order
						, publishing_date
						, authorimagepath
						, testimonialimagepath
            , slug
						, created_on
						, updated_on
					)
					VALUES
					(	"'.$this->mysqlEscapeString($object->name).'"
						, "'.$this->mysqlEscapeString($object->description).'"
						, "'.$this->mysqlEscapeString($object->tags).'"
            , "'.$this->mysqlEscapeString($object->testimonial_type).'"
            , "'.$this->mysqlEscapeString($object->student_type).'"
            , "'.$this->mysqlEscapeString($object->testimonial_order).'"
						, "'.$this->mysqlEscapeString($object->publishing_date).'"
						, "'.$this->mysqlEscapeString($object->authorimagepath).'"
						, "'.$this->mysqlEscapeString($object->testimonialimagepath).'"
                  , "'.$this->mysqlEscapeString($object->slug).'"
						, CURRENT_TIMESTAMP
						, CURRENT_TIMESTAMP);';
   			$this->db_query($sql);
			return $this->mysqlInsertId();
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function updateTestimonials($object)
	{
		try
		{
   			$sql	="	UPDATE testimonials SET
   						name                 =	'".$this->mysqlEscapeString($object->name)."'
   						, description        =	'".$this->mysqlEscapeString($object->description)."'
   						, tags               =	'".$this->mysqlEscapeString($object->tags)."'
              , testimonial_type   =  '".$this->mysqlEscapeString($object->testimonial_type)."'
              , student_type       =  '".$this->mysqlEscapeString($object->student_type)."'
              , testimonial_order  =  '".$this->mysqlEscapeString($object->testimonial_order)."'
   						, publishing_date    =	'".$this->mysqlEscapeString($object->publishing_date)."'
   						, authorimagepath	   =	'".$this->mysqlEscapeString($object->authorimagepath)."'
   						, testimonialimagepath	=	'".$this->mysqlEscapeString($object->testimonialimagepath)."'
              , slug       		=  '".$this->mysqlEscapeString($object->slug)."'
						, updated_on		=	CURRENT_TIMESTAMP
   						WHERE testimonial_id	=	'".$this->mysqlEscapeString($object->testimonial_id)."'";
			return ($this->db_query($sql))?true:false;
		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function deleteTestimonials ($testimonialId, $enabled)
	{
		try
		{
			$sql	=    "	UPDATE 	testimonials
							SET 	enabled 		=  '".$this->mysqlEscapeString($enabled)."'
							WHERE 	testimonial_id	=	'".$this->mysqlEscapeString($testimonialId)."'";
			return ($this->db_query($sql))?true:false;
		}
		catch(Exception $e)
		{	throw $e;	}
	}

    public function getTestimonialsById($id)
    {
    	try
    	{
    		$sql = "SELECT	*	FROM testimonials WHERE testimonial_id	= '".$this->mysqlEscapeString($id)."'";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }
    
   public function checkTestimonialSlug($slug) {
      try {
         $sql="SELECT testimonial_id
                  , name
              FROM testimonials WHERE slug = '".$this->mysqlEscapeString($slug)."'";
         $res=$this->db_query($sql);
         if($this->db_num_rows($res)==0) {
            $this->db_free_results($res);
            return false;
         } else {
            $this->db_free_results($res);
            return true;
         }
      }catch (Exception $e){ $this->db_error($e); }
   }

}
?>
