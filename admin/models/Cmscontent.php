<?php
class Cmscontent extends DBSource
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}


	public function updateCmscontent($obj_structure)
	{
		try
		{
   			$sql	="	UPDATE  cms_contents SET
   						title	    			=	'".$this->mysqlEscapeString($obj_structure->title)."'
              , subtitle      = '".$this->mysqlEscapeString($obj_structure->subtitle)."'
   						, description		=	'".$this->mysqlEscapeString($obj_structure->description)."'
              , metakeyword       = '".$this->mysqlEscapeString($obj_structure->metakeyword)."'
              , metadescription   = '".$this->mysqlEscapeString($obj_structure->metadescription)."'
						  , updated_on			=	CURRENT_TIMESTAMP
   						WHERE type			=	'".$this->mysqlEscapeString($obj_structure->type)."'";
			return ($this->db_query($sql))?true:false;
		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

    public function getCmscontentByType($type)
    {
    	try
    	{
    		$sql = "SELECT * FROM cms_contents WHERE type = '".$this->mysqlEscapeString($type)."'";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }
}
?>
