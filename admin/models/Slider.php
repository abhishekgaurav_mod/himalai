<?php 
class Slider extends DBSource 
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function addSlider($obj_slider)
	{
		try
		{
			$sql = 'INSERT INTO sliders
					(	
						description
						, slider_type
                        , imagepath
						, created_on
						, updated_on
					)
					VALUES
					(	
						"'.$this->mysqlEscapeString($obj_slider->description).'"
						, "'.$this->mysqlEscapeString($obj_slider->slider_type).'"
                        , "'.$this->mysqlEscapeString($obj_slider->imagepath).'"
						, CURRENT_TIMESTAMP
						, CURRENT_TIMESTAMP);';
   			$this->db_query($sql);
			return $this->mysqlInsertId();
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

    public function updateSlider ($obj_slider)
    {
        try
        {
            $sql    ="  UPDATE sliders SET
                        description     =   '".$this->mysqlEscapeString($obj_slider->description)."',
                        slider_type     =   '".$this->mysqlEscapeString($obj_slider->slider_type)."',
                        imagepath       =   '".$this->mysqlEscapeString($obj_slider->imagepath)."',
                        updated_on  	=   CURRENT_TIMESTAMP
                        WHERE slider_id  =   '".$this->mysqlEscapeString($obj_slider->slider_id)."'";
            return ($this->db_query($sql))?true:false;
        }
        catch(Exception $e)
        {   throw $e;   }
    }
	
	public function deleteSlider($sliderId, $status)
	{
		try
		{
			$sql	=    "	DELETE FROM sliders WHERE slider_id	=	'".$this->mysqlEscapeString($sliderId)."'";
			return ($this->db_query($sql))?true:false;
		}
		catch(Exception $e)
		{	throw $e;	}
	}
	
    public function getSliderById($Id)
    {
    	try
    	{
    		$sql = "SELECT	slider_id, slider_type, description, imagepath, created_on, updated_on, enabled 
    		 		FROM sliders 
    				WHERE slider_id = '".$this->mysqlEscapeString($Id)."' ";
    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }
	
}
?>
