<?php
class Gsoptionalsubject extends DBSource
{
	public function __construct()
	{	parent::__construct();	}

   public function __destruct()
	{	parent::__destruct();	}

	public function addGsoptionalsubjects($object)
	{
		try
		{
			$sql = 'INSERT INTO gs_optional_subjects
					(
						name
						, description
                  , thumb
                  , slug
						, created_on
						, updated_on
					)
					VALUES
					(	"'.$this->mysqlEscapeString($object->name).'"
						, "'.$this->mysqlEscapeString($object->description).'"
                  , "'.$this->mysqlEscapeString($object->thumb).'"
                  , "'.$this->mysqlEscapeString($object->slug).'"
						, CURRENT_TIMESTAMP
						, CURRENT_TIMESTAMP);';
   			$this->db_query($sql);
			return $this->mysqlInsertId();
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function updateGsoptionalsubjects($object)
	{
		try
		{
   			$sql	="	UPDATE gs_optional_subjects SET
   						name		    	=	'".$this->mysqlEscapeString($object->name)."'
   						, description	=	'".$this->mysqlEscapeString($object->description)."'
                     , thumb        = '".$this->mysqlEscapeString($object->thumb)."'
                     , slug       	=  '".$this->mysqlEscapeString($object->slug)."'
                     , updated_on	=	CURRENT_TIMESTAMP
                     WHERE gsoptionalsubject_id	=	'".$this->mysqlEscapeString($object->gsoptionalsubject_id)."'";
			return ($this->db_query($sql))?true:false;
		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function deleteGsoptionalsubjects ($gsoptionalsubjectId, $enabled)
	{
		try
		{
			$sql	=    "	UPDATE 	gs_optional_subjects
   							SET 	enabled 		=  '".$this->mysqlEscapeString($enabled)."'
   							WHERE 	gsoptionalsubject_id	=	'".$this->mysqlEscapeString($gsoptionalsubjectId)."'";
			return ($this->db_query($sql))?true:false;
		}
		catch(Exception $e)
		{	throw $e;	}
	}

    public function getGsoptionalsubjectsById($id)
    {
    	try
    	{
    		$sql = "SELECT * FROM gs_optional_subjects WHERE gsoptionalsubject_id	= '".$this->mysqlEscapeString($id)."'";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
   }

   public function checkGsoptionalsubjectSlug($slug) {
      try {
         $sql="SELECT gsoptionalsubject_id , name 
               FROM gs_optional_subjects WHERE slug = '".$this->mysqlEscapeString($slug)."'";
         $res=$this->db_query($sql);
         if($this->db_num_rows($res)==0) {
            $this->db_free_results($res);
            return false;
         } else {
            $this->db_free_results($res);
            return true;
         }
      }catch (Exception $e){ $this->db_error($e); }
   }

}
?>
