<?php
class Questionsubject extends DBSource
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function addQuestionsubject($obj_questionsubject)
	{
		try
		{
			$sql = 'INSERT INTO questionsubjects
					(
						questionsubject_name
                  		, question_type
						, thumb
						, question_paper
						, questionyear_id
						, created_on
						, updated_on
					)
					VALUES
					(	"'.$this->mysqlEscapeString($obj_questionsubject->questionsubject_name).'"
                  		, "'.$this->mysqlEscapeString($obj_questionsubject->question_type).'"
						, "'.$this->mysqlEscapeString($obj_questionsubject->thumb).'"
						, "'.$this->mysqlEscapeString($obj_questionsubject->question_paper).'"
						, "'.$this->mysqlEscapeString($obj_questionsubject->questionyear_id).'"
						, CURRENT_TIMESTAMP
						, CURRENT_TIMESTAMP);';
   			$this->db_query($sql);
			return $this->mysqlInsertId();
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function updateQuestionsubject($obj_questionsubject)
	{
		try
		{
   			$sql	="	UPDATE questionsubjects SET
   						questionsubject_name		=	'".$this->mysqlEscapeString($obj_questionsubject->questionsubject_name)."'
                  		,  question_type           	=  '".$this->mysqlEscapeString($obj_questionsubject->question_type)."'
						, thumb						=	'".$this->mysqlEscapeString($obj_questionsubject->thumb)."'
						, question_paper			=	'".$this->mysqlEscapeString($obj_questionsubject->question_paper)."'
						, updated_on				=	CURRENT_TIMESTAMP
   						WHERE questionsubject_id	=	'".$this->mysqlEscapeString($obj_questionsubject->questionsubject_id)."'";
			return ($this->db_query($sql))?true:false;
		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function deleteQuestionsubject ($questionsubjectId, $enabled)
	{
		try
		{
			$sql	=    "	UPDATE 	questionsubjects
							SET 	enabled 	=  '".$this->mysqlEscapeString($enabled)."'
							WHERE 	questionsubject_id	=	'".$this->mysqlEscapeString($questionsubjectId)."'";
			return ($this->db_query($sql))?true:false;
		}
		catch(Exception $e)
		{	throw $e;	}
	}

    public function getQuestionfileById($id)
    {
    	try
    	{
    		$sql = "SELECT	thumb , question_paper
    				FROM questionsubjects
    				WHERE questionsubject_id	= '".$this->mysqlEscapeString($id)."'";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }

    public function getQuestionsubjectById($id)
    {
    	try
    	{
    		$sql = "SELECT	r.questionyear_name
    						, r.questionyear_id
    						, r.question_year
    		                , c.questionsubject_name
                        	, c.question_type
    		                , c.thumb
    		                , c.question_paper
    		                , c.questionsubject_id
    		                , c.created_on
    		                , c.updated_on
    		                , c.enabled
    				FROM questionyears AS r
    				LEFT JOIN questionsubjects AS c ON c.questionyear_id = r.questionyear_id
    				WHERE c.questionsubject_id	= '".$this->mysqlEscapeString($id)."'";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }
    public function getQuestionsubjectsByQuestionyearId($questionyearId)
	{
		try
		{
			$sql = 'SELECT 	c.questionsubject_id
							, c.questionyear_id
							, c.questionsubject_name
                     		, c.question_type
							, c.thumb
							, c.question_paper
					FROM questionsubjects AS c
					WHERE c.questionyear_id	=	"'.$this->mysqlEscapeString($questionyearId).'"';

			$res=$this->db_query($sql);
			if($this->db_num_rows($res)==0)
			{
				$this->db_free_results($res);
				return 0;
			}
			else
			{
    			for($i=0;$row=$this->db_fetch_object($res);$i++)
    			{	$arr[$i] = $row;	}
    			$this->db_free_results($res);
    			return $arr;
			}
		}
		catch(Exception $e)
		{	throw $e;	}
	}
	public function getQuestionsubjectsAndQuestionyears()
	{
		try
		{
			$sql = 'SELECT 	c.questionsubject_id
							, c.questionyear_id
							, c.questionsubject_name
							, c.thumb
							, c.question_paper
							, r.questionyear_name
					FROM questionsubjects AS c
					LEFT JOIN questionyears AS r ON r.questionyear_id = c.questionyear_id
					WHERE c.enabled	=	"Y"';

			$res=$this->db_query($sql);
			if($this->db_num_rows($res)==0)
			{
				$this->db_free_results($res);
				return 0;
			}
			else
			{
    			for($i=0;$row=$this->db_fetch_object($res);$i++)
    			{	$arr[$i] = $row;	}
    			$this->db_free_results($res);
    			return $arr;
			}
		}
		catch(Exception $e)
		{	throw $e;	}
	}
}
?>
