<?php 
class Admin extends DBSource 
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function addAdmin($obj_admin)
	{
		try
		{
			$sql = 'INSERT INTO admins
					(	
						email
						, password
						, salt_string
						, name
						, created_on
						, updated_on
					)
					VALUES
					(	
						"'.$this->mysqlEscapeString($obj_admin->email).'"
						, MD5("'.$this->mysqlEscapeString($obj_admin->salt_string.$obj_admin->password).'")
						, "'.$this->mysqlEscapeString($obj_admin->salt_string).'"
						, "'.$this->mysqlEscapeString($obj_admin->name).'"
						, CURRENT_TIMESTAMP
						, CURRENT_TIMESTAMP);';
   			$this->db_query($sql);
			return $this->mysqlInsertId();
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function updateAdmin ($obj_admin)
	{
		try
		{
   			$sql	="	UPDATE admins SET
   						name			=	'".$this->mysqlEscapeString($obj_admin->name)."' 
   						, email			=	'".$this->mysqlEscapeString($obj_admin->email)."'
						, updated_on	=	CURRENT_TIMESTAMP "; 
   			if (!empty($obj_admin->password))
			{	$sql .= "	, salt_string	=	'".$this->mysqlEscapeString($obj_admin->salt_string)."' 
							, password	=	MD5('".$this->mysqlEscapeString($obj_admin->salt_string.$obj_admin->password)."') ";	}		   					
   					
			$sql	.= " WHERE admin_id	=	'".$this->mysqlEscapeString($obj_admin->admin_id)."'";
			return ($this->db_query($sql))?true:false;
		}
   		catch(Exception $e)
   		{	throw $e;	}
	}
	
    public function updateAdminSignParam ($obj_admin)
    {
        try
        {
            $sql    ="  UPDATE admins SET
                        sign_in_count       =   '".$this->mysqlEscapeString($obj_admin->sign_in_count)."',
                        current_sign_in_at  =   CURRENT_TIMESTAMP,
                        last_sign_in_at     =   '".$this->mysqlEscapeString($obj_admin->last_sign_in_at)."', 
                        current_sign_in_ip  =   '".$this->mysqlEscapeString($obj_admin->current_sign_in_ip)."', 
                        last_sign_in_ip     =   '".$this->mysqlEscapeString($obj_admin->last_sign_in_ip)."'
                        WHERE admin_id =   '".$this->mysqlEscapeString($obj_admin->admin_id)."'";
            return ($this->db_query($sql))?true:false;
        }
        catch(Exception $e)
        {   throw $e;   }
    }
	
	public function deleteAdmin ($adminId, $status)
	{
		try
		{
			$sql	=    "	UPDATE admins SET enabled =  '".$this->mysqlEscapeString($status)."' 
							WHERE admin_id	=	'".$this->mysqlEscapeString($adminId)."'";
			return ($this->db_query($sql))?true:false;
		}
		catch(Exception $e)
		{	throw $e;	}
	}
	
	
	public function authenticateAdmin ($adminemail, $adminpassword)
	{
		try
		{
   			$sql='	SELECT * FROM admins  
   					WHERE email 	= \''.$this->mysqlEscapeString($adminemail).'\' 
   					AND password	= MD5(\''.$this->mysqlEscapeString($adminpassword).'\')';
   				
   			$res=$this->db_query($sql);
   			if($this->db_num_rows($res)==0)
   			{
   				$this->db_free_results($res);
				return 0;
   			}
			else
			{
				$row	=	$this->db_fetch_object($res);
				$this->db_free_results($res);
				return $row;
			}
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}
	
    public function getAdminById($adminId, $type=NULL)
    {
    	try
    	{
    		$sql = "SELECT	admin_id, email, name, sign_in_count, current_sign_in_at, last_sign_in_at
    						, current_sign_in_ip, last_sign_in_ip, created_on, updated_on, enabled 
    		 		FROM admins 
    				WHERE admin_id = '".$this->mysqlEscapeString($adminId)."' ";

    		if ($type!=NULL)
    		{	$sql	.=	" AND admin_type	=	'".$this->mysqlEscapeString($type)."'";	}
    	
    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }
	
    public function changePassword($salt, $password, $adminId)
	{
		try
		{
			$sql = 'UPDATE admins 
					SET password = MD5(\''.$this->mysqlEscapeString($salt).$this->mysqlEscapeString($password).'\')
						, salt_string =	\''.$this->mysqlEscapeString($salt).'\'
						, updated_on		=	CURRENT_TIMESTAMP
					WHERE admin_id = '.$adminId.';';
   			return ($this->db_query($sql))?true:false;
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}
	
    public function getAdminByEmail($email)
    {
        try
        {
            $sql='	SELECT 	admin_id
            				, email
            				, salt_string
            				, name
            		FROM admins WHERE email = "'.$this->mysqlEscapeString($email).'"';
                
            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0)
            {
                $this->db_free_results($res);
                return 0;
            }
            else
            {
                $row    =   $this->db_fetch_object($res);
                $this->db_free_results($res);
                return $row;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }
	
	public function checkAdminEmail($email)
    {
        try
        {
            $sql="SELECT admin_id, email FROM admins WHERE email = '".$this->mysqlEscapeString($email)."'";
            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0)
            {
                $this->db_free_results($res);
                return false;
            }
            else
            {
                $this->db_free_results($res);
                return true;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }
}
?>
