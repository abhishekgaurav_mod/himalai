<?php
class Civilserviceprofile extends DBSource
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function addCivilserviceprofiles($object)
	{
		try
		{
			$sql = 'INSERT INTO civil_service_profiles
					(
						name
						, description
                  		, slug
						, created_on
						, updated_on
					)
					VALUES
					(	"'.$this->mysqlEscapeString($object->name).'"
						, "'.$this->mysqlEscapeString($object->description).'"
                  , "'.$this->mysqlEscapeString($object->slug).'"
						, CURRENT_TIMESTAMP
						, CURRENT_TIMESTAMP);';
   			$this->db_query($sql);
			return $this->mysqlInsertId();
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function updateCivilserviceprofiles($object)
	{
		try
		{
   			$sql	="	UPDATE civil_service_profiles SET
   						name		    	=	'".$this->mysqlEscapeString($object->name)."'
   						, description		=	'".$this->mysqlEscapeString($object->description)."'
                     	, slug       		=  '".$this->mysqlEscapeString($object->slug)."'
						, updated_on		=	CURRENT_TIMESTAMP
   						WHERE civilserviceprofile_id	=	'".$this->mysqlEscapeString($object->civilserviceprofile_id)."'";
			return ($this->db_query($sql))?true:false;
		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function deleteCivilserviceprofiles ($civilserviceprofileId, $enabled)
	{
		try
		{
			$sql	=    "	UPDATE 	civil_service_profiles
							SET 	enabled 		=  '".$this->mysqlEscapeString($enabled)."'
							WHERE 	civilserviceprofile_id	=	'".$this->mysqlEscapeString($civilserviceprofileId)."'";
			return ($this->db_query($sql))?true:false;
		}
		catch(Exception $e)
		{	throw $e;	}
	}

    public function getCivilserviceprofilesById($id)
    {
    	try
    	{
    		$sql = "SELECT * FROM civil_service_profiles WHERE civilserviceprofile_id	= '".$this->mysqlEscapeString($id)."'";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }

   public function checkCivilserviceprofileSlug($slug) {
      try {
         $sql="SELECT civilserviceprofile_id
                  , name
              FROM civil_service_profiles WHERE slug = '".$this->mysqlEscapeString($slug)."'";
         $res=$this->db_query($sql);
         if($this->db_num_rows($res)==0) {
            $this->db_free_results($res);
            return false;
         } else {
            $this->db_free_results($res);
            return true;
         }
      }catch (Exception $e){ $this->db_error($e); }
   }

}
?>
