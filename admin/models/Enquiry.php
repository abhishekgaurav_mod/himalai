<?php 
class Enquiry extends DBSource 
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function getEnquiryById($enquiryId)
    {
    	try
    	{
    		$sql = "SELECT 	* 
    				FROM enquiries
    				WHERE enquiry_id	= '".$this->mysqlEscapeString($enquiryId)."'";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }

    public function getEnquiries($object)
    {
        try
        {
            $sql = "SELECT * FROM enquiries
            WHERE enabled = 'Y' ";
        
            $condition = '';
            if (!empty($object->date)){
                $condition  .=  ' AND DATE(created_on)  = "'.date("Y-m-d", strtotime($object->date)).'"';
                    
            }
            if(!empty($object->startdate) && (!empty($object->enddate))){
                $condition  .=  ' AND (created_on BETWEEN "'.date("Y-m-d", strtotime($object->startdate)).'" AND "'.date('Y-m-d', strtotime($object->enddate)).'")';
            }
            $sql .= $condition;
            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0)
            {
                $this->db_free_results($res);
                return 0;
            }
            else
            {
                $arr    =   array();
                for($i=0;$row=$this->db_fetch_row($res);$i++)
                {   $arr[$i]    =   $row;   }
                $this->db_free_results($res);
                return $arr;
            }
        }catch (Exception $e){ $this->db_error($e); }
    }
}
?>
