<?php
class Questionyear extends DBSource
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function addQuestionyear($obj_questionyear)
	{
		try
		{
			$sql = 'INSERT INTO questionyears
					(
						questionyear_name
						, exam_type
						, question_year
						, created_on
						, updated_on
					)
					VALUES
					(	"'.$this->mysqlEscapeString($obj_questionyear->questionyear_name).'"
						, "'.$this->mysqlEscapeString($obj_questionyear->exam_type).'"
						, "'.$this->mysqlEscapeString($obj_questionyear->question_year).'"
						, CURRENT_TIMESTAMP
						, CURRENT_TIMESTAMP);';
   			$this->db_query($sql);
			return $this->mysqlInsertId();
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function updateQuestionyear($obj_questionyear)
	{
		try
		{
   			$sql	="	UPDATE questionyears SET
   						questionyear_name	    =	'".$this->mysqlEscapeString($obj_questionyear->questionyear_name)."'
   						, exam_type    			=	'".$this->mysqlEscapeString($obj_questionyear->exam_type)."'
   						, question_year    =	'".$this->mysqlEscapeString($obj_questionyear->question_year)."'
						, updated_on			=	CURRENT_TIMESTAMP
   						WHERE questionyear_id	=	'".$this->mysqlEscapeString($obj_questionyear->questionyear_id)."'";
			return ($this->db_query($sql))?true:false;
		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function deleteQuestionyear ($questionyearId, $enabled)
	{
		try
		{
			$sql	=    "	UPDATE 	questionyears
							SET 	enabled 	=  '".$this->mysqlEscapeString($enabled)."'
							WHERE 	questionyear_id	=	'".$this->mysqlEscapeString($questionyearId)."'";
			return ($this->db_query($sql))?true:false;
		}
		catch(Exception $e)
		{	throw $e;	}
	}

    public function getQuestionyearById($id)
    {
    	try
    	{
    		$sql = "SELECT	* FROM questionyears
    				WHERE 	questionyear_id	= '".$this->mysqlEscapeString($id)."'";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }
	public function getQuestionyearsAndSubQuestionyears()
	{
		try
		{
			$sql = 'SELECT 	* FROM questionyears WHERE enabled = "Y" ';

			$res=$this->db_query($sql);
			if($this->db_num_rows($res)==0)
			{
				$this->db_free_results($res);
				return 0;
			}
			else
			{
				$subquestionyear	=	new Subquestionyear();
				for($i=0;$row=$this->db_fetch_object($res);$i++)
				{
					$object		=	(object)'';
					$object->questionyear_id	=	$row->questionyear_id;
					$object->questionyear_name	=	$row->questionyear_name;
					$object->exam_type			=	$row->exam_type;
					$object->created_on			=	$row->created_on;
					$object->updated_on			=	$row->updated_on;
					$object->enabled			=	$row->enabled;
					$obj						=	$subquestionyear->getSubQuestionyearsByQuestionyearId($object->questionyear_id);
					if(!empty($obj))
					{
						$object->subquestionyears	=	$obj;
						$arr[$i] = $object;
					}
				}
				$this->db_free_results($res);
				return $arr;
			}
		}
		catch(Exception $e)
		{	throw $e;	}
	}
}
?>
