$(document).ready(function($) {
	$('a[rel*=facebox]').facebox();
	if ($('.signin').length>0)
	{	formsubmit('frmsignin','.forms','top center',false);	}
	if ($('.changepassword').length>0)
	{	formsubmit('frmchangepassword','.forms','top center',false);	}
	if ($('.addadmin').length>0)
	{	formsubmit('frmadduser','.grid-5050-a','top center',false);	}
	if ($('.addcasetudy').length>0)
	{	formsubmit('frmaddcasetudy','.grid-5050-a','top center',false);	}
	if ($('.addquestionyear').length>0)
	{	formsubmit('frmaddquestionyear','.forms','top center',false);	}
	if ($('.addcountry').length>0)
	{	formsubmit('frmaddcountry','.forms','top center',false);	}
	if ($('.addfaq').length>0)
	{	formsubmit('frmaddfaq','.forms','top center',false);	}
	if ($('.addcmscontent').length>0)
	{	formsubmit('frmaddcmscontent','.forms','top center',false);	}
	if ($('.addevents').length>0)
	{	formsubmit('frmaddevents','.grid-5050-a','top center',false);	}
	if ($('.addslider').length>0)
	{	formsubmit('frmaddslider','.forms','top center',false);	}
	if ($('.addservice').length>0)
	{	formsubmit('frmaddservice','.grid-5050-a','top center',false);	}
	if ($('.addfacilities').length>0)
	{	formsubmit('frmaddfacilities','.grid-5050-a','top center',false);	}
	if ($('.addtag').length>0)
	{	formsubmit('frmaddtag','.forms','top center',false);	}

});

var formsubmit 	= 	function(id,relative,pos,scroll){
	$('#'+id+' button[type="submit"]').removeAttr('disabled','disabled');
	$('#'+id).ajaxForm({
		forceSync: true,
		dataType: 'json',
		beforeSerialize:function($Form, options){
			return CKupdate();
	    },
		beforeSubmit: function(){
			$('#'+id+' .loading').show();
			$('#'+id+' button[type="submit"]').attr('disabled','disabled');
			return CKupdate();
		},
		success: function(responseText, statusText, xhr, $form){
			var responseText 	= 	jQuery.parseJSON(xhr.responseText);
			if(responseText.redirect)
			{	window.location.href	=	responseText.url;	}
			if(responseText.status)
			{	$('#'+id)[0].reset();	}
			if(responseText.prompt)
			{
				var error	=	(responseText.status)?'success':'error';
				$(relative).notify(responseText.message, {className:error, position:pos});
			}
		},
		complete: function(){
			$('a[rel*=facebox]').facebox();
			$('#'+id+' button[type="submit"]').removeAttr('disabled','disabled');
			$('#'+id+' .loading').hide();
			if (scroll)
			{	$('.mainbody').scrollTop(15);	}
		}
	});
};

var CKupdate	=	function(){
	if(CKEDITOR!=undefined || CKEDITOR!=null){
		for (instance in CKEDITOR.instances)
	    {	CKEDITOR.instances[instance].updateElement();	}
	}
	return true;
};

$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
{
    return {
        "iStart":         oSettings._iDisplayStart,
        "iEnd":           oSettings.fnDisplayEnd(),
        "iLength":        oSettings._iDisplayLength,
        "iTotal":         oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage":          oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages":    oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
};

var deleters	=	function(action, id, status, text)
{
	var contoller	=	action;
	if(action.toLowerCase()=='enquiry')
	{	contoller	=	'enquirie';	}
	if(action.toLowerCase()=='newsevent')
	{	contoller	=	'newsandevent';	}
	if(action.toLowerCase()=='resource')
	{	contoller	=	'casestudie';	}
	if(action.toLowerCase()=='country')
	{	contoller	=	'countrie';	}
	if(action.toLowerCase()=='image')
	{	contoller	=	'gallerie';	}
	if(action.toLowerCase()=='gallery image')
	{	contoller	=	'gallerie';	}
	if(action.toLowerCase()=='facility image')
	{	contoller	=	'galleryfacilitie';	}
	if(action.toLowerCase()=='newsevent image')
	{	contoller	=	'gallerynewsevent';	}
	if(action.toLowerCase()=='facility')
	{	contoller	=	'facilitie';	}

	var ans	=	confirm("Are Sure you want to "+text+" this "+action+"!");
	if (ans==true)
	{
		$.ajax({
			url: _URL+contoller.toLowerCase()+'s'+'/'+'delete',
			data: "action="+action+"&id="+id+'&status='+status,
			type: 'POST',
			dataType: 'json',
			success: function(data) {
				if (data.prompt)
				{	alert(data.message);	}
				if (data.redirect)
				{	window.location.href = data.url;	}
			},
			error: function() {	alert('Error while updating Records.');	},
			complete: function() {	window.location.reload();	}
		});
	}
};
var sendmail	=	function(type, id, name)
{
	var ans	=	confirm("Are Sure you want to send the login details to "+name+"!\nNote: This will reset there password first.");
	if (ans==true)
	{
		$.ajax({
			url: _URL+'users'+'/'+'sendmail',
			data: "id="+id+'&name='+name,
			type: 'POST',
			dataType: 'json',
			success: function(data) {
				if (data.redirect)
				{	window.location.href = data.url;	}
				if (data.prompt)
				{	alert(data.message);	}
			},
			error: function() {	alert('Error while sending login details.');	},
			complete: function() {}
		});
	}
};

var gettweets	=	function()
{
	$.ajax({
			url: _URL+'tweets'+'/'+'twitterfeeds',
			data: "",
			type: 'POST',
			beforeSend : function(xhr, opts){ openloader(); },
			dataType: 'json',
			success: function(data) {},
			complete: function() {
				closeloader();
				window.location.href	=	_URL+'tweets/index';
			}
		});
};

var getfacebooks	=	function()
{
	$.ajax({
			url: _URL+'facebooks'+'/'+'facebookfeeds',
			data: "",
			type: 'POST',
			beforeSend : function(xhr, opts){ openloader(); },
			dataType: 'json',
			success: function(data) {},
			complete: function() {
				closeloader();
				window.location.href	=	_URL+'facebooks/index';
			}
		});
};

var publish	=	function(action, id, status, text)
{
	var contoller	=	action;

	var ans	=	confirm("Are Sure you want to "+text+" this "+action+"!");
	if (ans==true)
	{
		$.ajax({
			url: _URL+contoller.toLowerCase()+'s'+'/'+'publish',
			data: "action="+action+"&id="+id+'&status='+status,
			type: 'POST',
			dataType: 'json',
			success: function(data) {
				if (data.prompt)
				{	alert(data.message);	}
				if (data.redirect)
				{	window.location.href = data.url;	}
			},
			error: function() {	alert('Error while updating Records.');	},
			complete: function() {	window.location.reload();	}
		});
	}
};

var openloader	=	function(){
	var html	=	'<div class="overlay"></div>';
	html		+=	'<div class="dialog"><div class="popup"><div class="loading">Please while we import latest feeds.</div></div></div>';
	$('body').append(html);
};
var closeloader	=	function(){
	$( ".overlay" ).remove();
	$( ".dialog" ).remove();
};
