<?php
	require 'config.php';
	require 'settings.php';
	
	session_start();
	date_default_timezone_set(_TIME_ZONE);
	if (_ERROR_REPORTING)
	{	error_reporting(E_ALL & ~E_STRICT);	}
	else
	{	error_reporting(0);	}
	
	function __autoload($_class)
	{
		$str_class          =   _CONTROLLERS_ROOT.$_class . '.php';
		$str_model_class    =   _MODEL_ROOT.$_class . '.php';
		$str_lib_class    	=   _LIBS_ROOT.$_class . '.php';
		
		if (file_exists($str_class))
		{	$str_class  =   $str_class;	}
		if (file_exists($str_model_class))
		{	$str_class  =   $str_model_class;	}
		if (file_exists($str_lib_class))
		{	$str_class  =   $str_lib_class;	}
		if(is_file($str_class))
		{	include_once($str_class);	}
	}
?>