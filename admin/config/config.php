<?php
   /* Application Settings
    * Development - dev
    * Staging - staging
    * Production - prod
    * */
   ob_start("ob_gzhandler");  //Gzip Compress
   define('_ENV', 'dev');
   
   date_default_timezone_set("Asia/Calcutta");
   define('_TIME_ZONE', 'Asia/Calcutta');
   define('_ELASTIC_SEARCH',false);

   define('_PROTOCOL',(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://");

   // ROOT  Settings
   define('WORK_DIR_PATH',getcwd());
   define('DOC_PATH',$_SERVER['DOCUMENT_ROOT']);
   define('CURR_PATH',dirname(__FILE__));
   if(!defined('CWD_PATH')){
      $work_dir_path = explode('/', CURR_PATH);
      array_pop($work_dir_path);
      $work_dir_path = implode('/', $work_dir_path).'/';
      define('CWD_PATH', $work_dir_path);
   }
   define('_ROOT', CWD_PATH);

   /* load ENV Setting */
   require _ENV.'-config.php';
?>
