<?php
   /* Development ENV Settings */
	/* Debugging */
	define('_DEBUG', true);

	/* Master RDS */
	$database	=	array(	'_DB_HOST' => 'localhost',
							'_DB_NAME' => 'himalai',
							'_DB_USER' => 'root',
							'_DB_PASS' => 'admin');

	/* URL & Path */
	$userdefind	=	array(	'_ROOT' => CWD_PATH,
                     		'_URL' => _PROTOCOL.$_SERVER['HTTP_HOST']."/",
							'_MEDIA_ROOT' => "/var/www/html/himalai/website/webroot/media/",
							'_MEDIA_URL' => "http://web.himalai.com/webroot/media/");
?>
