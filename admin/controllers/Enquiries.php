<?php
class Enquiries extends Controller {
	
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}
			
	public function index()
	{
		$this->islogged();
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}
    	
    	$status     =   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
		$action			=	(!empty($action))?$action:'';
		$date			=	(!empty($getdate))?$getdate:'';
		$startdate		=	(!empty($start))?$start:'';
		$enddate		=	(!empty($end))?$end:'';

    	$this->current	=	'contact';
    	$this->pagetitle=	'enquiries';
    	 
    	$ajaxSource	=	_URL.'enquiries/getenquiries';
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'enquiries/index.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}
	
	public function getenquiries()
	{
		$this->islogged();
		foreach($_POST as $key=>$val)
		{	$$key   = $val;	}
		
		if (!empty($status))
		{
			$sTable			=	'enquiries';
			$sIndexColumn	=	'enquiry_id';
			$aColumns 		= 	array('enquiry_id','name','phone','email','interest','created_on','enquiry_id');
			$condition		=	((isset($status))?" enabled = '".$status."'":'');
			if (!empty($action)){
				switch($action)
				{
					case 'bydate'		:	$condition	.=	' AND DATE(created_on) 	= "'.date("Y-m-d", strtotime($date)).'"';
					break;
					case 'bydaterange'	:	$condition	.=	' AND (created_on BETWEEN "'.date("Y-m-d", strtotime($startdate)).'" AND "'.date('Y-m-d', strtotime($enddate)).'")';
					break;
					
				}
			}
			$datatable		=	new DataTable($sTable, $aColumns, $sIndexColumn, $condition);
			$datatable->output();
		}
	}
	
    public function generateEnquiryExcel(){	   	
   		try{
	    	$this->islogged(true);
	   		foreach($_REQUEST as $key=>$val)
			{	$$key	= $val;	}
			$object				=   new stdClass();
			$object->date		=   $date;
			$object->startdate	= 	$startdate;
			$object->enddate	= 	$enddate;

			$enquiry       =  new Enquiry();
		   	$arr_enquiries  =  $enquiry->getEnquiries($object);

			$arr_cellHeading = [
				'A'	=> 'Id',
				'B' => 'Name',
			   	'C' => 'Phone',
			   	'D' => 'Email',
			   	'E' => 'Qualification',
			   	'F' => 'Interest',
			   	'G' => 'Message',
			   	'H' => 'Created On',
			   	'I' => 'Updated On',
			   	'J' => 'Enabled'
			];
		$object->html = '';
        $object->html .= "<table>";
        $object->html .= "<tr>";
        foreach ($arr_cellHeading as $key => $cellHeading) {
        	$object->html .='<th>'.$cellHeading.'</th>';
        }

        $object->html .= "</tr>";
        foreach($arr_enquiries AS $obj_enquiry){
            $object->html .= "<tr>";
            for($i=0;$i<sizeof($obj_enquiry);$i++){
                $object->html .= "<td>".$obj_enquiry[$i]."</td>";
            }
            $object->html .= "</tr>";
        }
        $object->html .= "</table>";

		$file = 'Leads';
		$file .= (!empty($date)?'_'.$date:'');
		$file .= (!empty($startdate)?'_'.$startdate:'');
		$file .= (!empty($enddate)?'_to_'.$enddate:'');
		$file .=".xls";

		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file");
		echo $object->html;

		exit();

   		}catch (Exception $e){ $this->error_log($e); }
 		
	   			
   }
	
	public function view()
	{
		$this->islogged();
		$this->current	=	'contact';
    	$this->pagetitle=	'view enquiry';
		foreach($_GET as $key=>$val)
		{	$$key   = $val;	}
	
		if (!empty($enquiryId))
		{
			$enquiry		=	new Enquiry();
			$obj_enquiry	=	$enquiry->getEnquiryById($enquiryId);
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'enquiries/view.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}
} 
?>