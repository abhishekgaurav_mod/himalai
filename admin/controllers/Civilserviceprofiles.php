<?php
class Civilserviceprofiles extends Controller {

	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function index()
	{
		$this->islogged();
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}

    	$status     =   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
    	$this->current	=	'civilserviceprofiles';
    	$this->pagetitle=	'Civil Service Profile';

    	$ajaxSource	=	_URL.'civilserviceprofiles/getcivilserviceprofiles';
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'civilserviceprofiles/index.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function getcivilserviceprofiles()
	{
		$this->islogged();
		foreach($_POST as $key=>$val)
		{	$$key   = $val;	}

		if (!empty($status))
		{
			$sTable			=	'civil_service_profiles';
			$sIndexColumn	=	'civilserviceprofile_id';
			$aColumns 		= 	array(	'civilserviceprofile_id','name','created_on','updated_on','enabled','civilserviceprofile_id');
			$condition		=	((isset($status))?" enabled = '".$status."'":'');
			$datatable		=	new DataTable($sTable, $aColumns, $sIndexColumn, $condition, $outType='array');
			$data			=	$datatable->output();

			echo json_encode($data);
		}
	}

	public function view()
	{
		$this->islogged();
		$this->current		=	'civilserviceprofiles';
    	$this->pagetitle	=	'Civil Service Profile';

		foreach($_GET as $key=>$val)
		{	$$key   = $val;	}

		if (!empty($id))
		{
			$civilserviceprofile		=	new Civilserviceprofile();
			$object   			=   $civilserviceprofile->getCivilserviceprofilesById($id);
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'civilserviceprofiles/view.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function add()
	{
		$this->islogged();
		$civilserviceprofile		=	new Civilserviceprofile();

		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{
				if(gettype($val)=='string')
				{	$$key	= trim(strip_tags($val));	}
				else
				{	$$key	= $val;		}
			}

			$name				=	trim(strip_tags($name));
			$description		= 	($_POST['description']);

			$err        =   NULL;
			switch($action)
			{
				case 'add'	:  	if ($err==NULL && empty($name))
								{	$err    =   'Please enter name.';	}
								if ($err==NULL && $civilserviceprofile->checkCivilserviceprofileSlug(Common::slugify($name)) )
								{ 	$err    =   'Name already in use.'; 	}
								/*if ($err==NULL && (empty($_FILES['thumb'])))
								{	$err	=	'Please upload Thumb Image.';		}*/
								if ($err==NULL && empty($description))
								{	$err    =   'Please enter description.';	}

								if ($err==NULL)
								{
									$object						=   (Object)'';
									$object->name  				=   $name;
									$object->description 		=   $description;
                  					$object->slug           	=   Common::slugify($name);

									if ($err==NULL)
									{
										$civilserviceprofileId     =   $civilserviceprofile->addCivilserviceprofiles($object);
										if (!empty($civilserviceprofileId))
										{	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'civilserviceprofiles/view?id='.$civilserviceprofileId)));		}
									}
									$err    =   ($err==NULL)?'Unable to add civilserviceprofile.':$err;
								}
				break;
				case 'edit'	:	if ($err==NULL && empty($name))
								{	$err    =   'Please enter name.';	}
								if ($err==NULL && empty($description))
								{	$err    =   'Please enter description.';	}


								if ($err==NULL)
                                {
									$object   					=   new stdClass();
									$object->civilserviceprofile_id		=   $civilserviceprofileId;
									$object->name  				=   $name;
									$object->description 		=   $description;
                  					$object->slug           	=   Common::slugify($name);

                                	if ($err==NULL)
                                	{
	                                    if ($civilserviceprofile->updateCivilserviceprofiles($object))
	                                    {	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'civilserviceprofiles/view?id='.$civilserviceprofileId)));		}
                                	}
                                	$err    =   ($err==NULL)?'Unable to update civilserviceprofile.':$err;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			foreach($_GET as $key=>$val)
			{	$$key   = $val;		}
			$action     =   (isset($action))?(($action=='edit')?$action:'add'):'add';

			$this->current		=	'civilserviceprofiles';
    		$this->pagetitle	=	'Civil Service Profile';
			$this->formhead		=	'Civil Service Profile';
			if (!empty($id))
			{	$object   =   $civilserviceprofile->getCivilserviceprofilesById($id);		}
			else
			{   $action     =   'add';	}
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'civilserviceprofiles/add.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}

		if (!empty($id))
		{
			$civilserviceprofile		=	new Civilserviceprofile();
			if ($civilserviceprofile->deleteCivilserviceprofiles($id,$status))
			{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Civilserviceprofile'.(($status=='N')?"disabled":"enabled").' successfully.')));	}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.')));
	}
}
?>
