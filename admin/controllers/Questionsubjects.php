<?php
class Questionsubjects extends Controller {

	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function index()
	{
		$this->islogged();
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}

		$questionyear	 	=	New Questionyear();

    	if(empty($id))
		{	header('Location:'._URL.'questionyears/');	}

    	$status     	=   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
		$questionyearId		=	$id;
		$obj_questionyear   =   $questionyear->getQuestionyearById($id);

    	$this->current	=	'questionyears';
    	$this->pagetitle=	'Question Subjects ('.$obj_questionyear->questionyear_name.')';

    	$ajaxSource	=	_URL.'questionsubjects/getquestionsubjects';
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'questionsubjects/index.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function getquestionsubjects()
	{
		$this->islogged();
		foreach($_POST as $key=>$val)
		{	$$key   = $val;	}

		if (!empty($status))
		{
			$sTable			=	'questionsubjects';
			$sIndexColumn	=	'questionsubject_id';
			$aColumns 		= 	array(	'questionsubject_id','questionsubject_name','question_type','created_on','updated_on','enabled','questionyear_id');
			$condition		=	((isset($status))?" enabled = '".$status."'":'')." AND questionyear_id = '".$questionyearId."'";
			$datatable		=	new DataTable($sTable, $aColumns, $sIndexColumn, $condition);
			$datatable->output();
		}
	}

	public function view()
	{
		$this->islogged();
		$this->current	=	'questionyears';
		$this->pagetitle=	'Question Subjects';

		foreach($_GET as $key=>$val)
		{	$$key   = $val;	}


		if (!empty($id))
		{
			$questionsubject			=	new Questionsubject();
			$obj_questionsubject		=	$questionsubject->getQuestionsubjectById($id);
		}
		$questionyearId					= (!empty($obj_questionsubject))?$obj_questionsubject->questionyear_id:null;
		$questionyearName				= (!empty($obj_questionsubject))?$obj_questionsubject->questionyear_name:null;
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'questionsubjects/view.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function add()
	{
		$this->islogged();

		$questionsubject			=	new Questionsubject();
		$questionyear	 			=	New Questionyear();

		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{
				if(gettype($val)=='string')
				{	$$key	= trim(strip_tags($val));	}
				else
				{	$$key	= $val;		}
			}

			$err        =   NULL;
			switch($action) {
				case 'add'	:  	if ($err==NULL && empty($title))
								{	$err    =   'Please enter questionsubject.';	}


								if ($err==NULL)
								{
									$obj_questionsubject   =   (Object)'';
									$obj_questionsubject->questionyear_id  		=   $questionyearId;
									$obj_questionsubject->questionsubject_name  =   $title;
									$obj_questionsubject->question_type			=   $question_type;
									$obj_questionsubject->thumb  				=   null;
									$obj_questionsubject->question_paper  		=   null;

									if (!empty($_FILES['thumb'])){
										if (empty($_FILES['thumb']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['thumb']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['thumb']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = strtoupper(Common::slugify($title)).'-'.$question_type.'-'.uniqid();
												$thumbName	=	$images->uploadimage($_FILES['thumb'], $fileName, _QUESTION_THUMB_ORGS);
											}
											if ($thumbName!=NULL)
											{	$obj_questionsubject->thumb	=	$thumbName;	}
											else
											{	$err	=	'Error: uploading thumb.';		}
										}
									}

									if (!empty($_FILES['question_paper'])){
										if (empty($_FILES['question_paper']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['question_paper']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['question_paper']['type'],unserialize(_ALLWOED_FILE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = strtoupper(Common::slugify($title)).'-'.$question_type.'-'.uniqid();
												$questionPaperName	=	$images->uploadimage($_FILES['question_paper'], $fileName, _QUESTION_ORGS);
											}
											if ($questionPaperName!=NULL)
											{	$obj_questionsubject->question_paper	=	$questionPaperName;	}
											else
											{	$err	=	'Error: uploading question paper.';		}
										}
									}

									if ($err==NULL)
									{
										$questionsubjectId     =   $questionsubject->addQuestionsubject($obj_questionsubject);
										if (!empty($questionyearId))
										{	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'questionsubjects/view?id='.$questionsubjectId)));	}
									}
									$err    =   ($err==NULL)?'Unable to add Question Subject.':$err;
								}
				break;
				case 'edit'	:	$obj_questionsubject  =   (Object)'';
								if ($err==NULL && empty($title))
								{	$err    =   'Please enter questionsubject.';	}

								if (!empty($questionsubjectId))
								{	$obj_questionfile   =   $questionsubject->getQuestionfileById($questionsubjectId);	}

								if ($err==NULL)
                                {
									$obj_questionsubject   =   (Object)'';
                                	$obj_questionsubject->questionsubject_id 	=   $questionsubjectId;
									$obj_questionsubject->questionsubject_name  =   $title;
									$obj_questionsubject->question_type			=   $question_type;
									$obj_questionsubject->thumb  				=   $thumbId;
									$obj_questionsubject->question_paper  		=   $question_paperId;

									if (!empty($_FILES['thumb'])){
										if (empty($_FILES['thumb']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['thumb']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['thumb']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = strtoupper(Common::slugify($title)).'-'.$question_type.'-'.uniqid();
												$thumbName	=	$images->uploadimage($_FILES['thumb'], $fileName, _QUESTION_THUMB_ORGS);
											}
											if ($thumbName!=NULL) {
												$obj_questionsubject->thumb	=	$thumbName;
												if(file_exists(_QUESTION_THUMB_ORGS.$obj_questionfile->thumb)){
													unlink(_QUESTION_THUMB_ORGS.$obj_questionfile->thumb);
												}
											}
											else
											{	$err	=	'Error: uploading thumb.';		}
										}
									}

									if (!empty($_FILES['question_paper'])){
										if (empty($_FILES['question_paper']['error']))
										{
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['question_paper']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['question_paper']['type'],unserialize(_ALLWOED_FILE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = strtoupper(Common::slugify($title)).'-'.$question_type.'-'.uniqid();
												$questionPaperName	=	$images->uploadimage($_FILES['question_paper'], $fileName, _QUESTION_ORGS);
											}
											if ($questionPaperName!=NULL) {
												$obj_questionsubject->question_paper	=	$questionPaperName;
												if(file_exists(_QUESTION_ORGS.$obj_questionfile->question_paper)){
													unlink(_QUESTION_ORGS.$obj_questionfile->question_paper);
												}
											}
											else
											{	$err	=	'Error: uploading question paper.';		}
										}
									}

                                	if ($err==NULL)
                                	{
	                                    if ($questionsubject->updateQuestionsubject($obj_questionsubject))
	                                    {   exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'questionsubjects/view?id='.$questionsubjectId)));	}
                                	}
                                	$err    =   ($err==NULL)?'Unable to update Question Subject.':$err;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			foreach($_GET as $key=>$val)
			{	$$key   = $val;		}
			$action     =   (isset($action))?(($action=='edit')?$action:'add'):'add';

			if(empty($questionyearId))
			{	header('Location:'._URL.'questionyears/');	}

			$obj_questionyear   =   $questionyear->getQuestionyearById($questionyearId);

			$this->current		=	'questionyears';
			$this->pagetitle	=	'Exam Year ('.$obj_questionyear->questionyear_name.')';
			$this->formhead		=	'Question Subjects';

			if (!empty($questionsubjectId))
			{	$obj_questionsubject   =   $questionsubject->getQuestionsubjectById($questionsubjectId);	}
			else
			{   $action     =   'add';	}
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'questionsubjects/add.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}

		if (!empty($id))
		{
			$questionsubject		=	new Questionsubject();
			if ($questionsubject->deleteQuestionsubject($id,$status))
			{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Question Subject '.(($status=='N')?"disabled":"enabled").' successfully.')));	}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.')));
	}
}
?>
