<?php
class Offices extends Controller {
	
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}
			
	public function index()
	{
		$this->islogged();
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}
    	
    	$status     		=   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
    	$this->current		=	'contact';
    	$this->pagetitle	=	'offices';
    	 
    	$ajaxSource			=	_URL.'offices/getoffices';
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'offices/index.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}
	
	public function getoffices()
	{
		$this->islogged();
		foreach($_POST as $key=>$val)
		{	$$key   = $val;	}
		
		if (!empty($status))
		{
			$sTable			=	'offices';
			$sIndexColumn	=	'office_id';
			$aColumns 		= 	array(	'office_id','title','type', 'created_on','updated_on','enabled','office_id');
			$condition		=	((isset($status))?" enabled = '".$status."'":'');
			$datatable		=	new DataTable($sTable, $aColumns, $sIndexColumn, $condition);
			$datatable->output();
		}
	}

	public function view()
	{
		$this->islogged();
		$this->current	=	'casestudies';
    	$this->pagetitle=	'resources';
		foreach($_GET as $key=>$val)
		{	$$key   = $val;	}
	
		if (!empty($id))
		{
			$casestudy		 =	new Casestudy();
			$obj_casestudy   =   $casestudy->getCasestudyById($id);;
		}
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'casestudies/view.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}
	
	public function add()
	{
		$this->islogged();
		$casestudy		=	new Casestudy();
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{
				if(gettype($val)=='string')
				{	$$key	= trim(strip_tags($val));	}	
				else
				{	$$key	= $val;		}
			}
			
			$err        =   NULL;
			switch($action)	
			{
				case 'add'	:  	if ($err==NULL && empty($type))
								{	$err    =   'Please select type.';	}
								if ($err==NULL && empty($title))
								{	$err    =   'Please add title.';	}
							/*	if ($err==NULL && empty($description))
								{
									if(empty($external_link))	
									$err    =   'Please add external link.';								}
								if ($err==NULL && empty($external_link))
								{
									if(empty($description))	
									$err    =   'Please add description.';	
								} */							
								
								if ($err==NULL)
								{
									$obj_casestudy   =   new stdClass();
									$obj_casestudy->type			=   $type;
									$obj_casestudy->source  	 	=   (empty($external_link))?'N':'E';
									$obj_casestudy->title			=   $title;
									$obj_casestudy->sub_title		=   (!empty($sub_title))?$sub_title:'';
									$obj_casestudy->imagepath		=	null;
									$obj_casestudy->description		=   $description;
									$obj_casestudy->external_link	=   (!empty($external_link))?$external_link:'';
									
									if (!empty($_FILES['imagepath'])){
										if (empty($_FILES['imagepath']['error']))
										{
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['imagepath']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['imagepath']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											
											$fileName	=	$images->uploadimage($_FILES['imagepath'], time().uniqid(), _CASESTUDY_ORGS);
											if ($fileName!=NULL)
											{	$obj_casestudy->imagepath	=	$fileName;		}
											else
											{	$err	=	'Error: uploading Image.';			}
										}
									}
									if ($err==NULL)
									{	
										$casestudyId     =   $casestudy->addCasestudy($obj_casestudy);
										if (!empty($casestudyId))
										{	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'casestudies/view?id='.$casestudyId)));	}
									}
									$err    =   ($err==NULL)?'Unable to add Case study or White Papper.':$err;
								}
				break;
				case 'edit'	:	$obj_admin  =   (Object)'';
								if ($err==NULL && empty($type))
								{	$err    =   'Please select type.';	}
								if ($err==NULL && empty($title))
								{	$err    =   'Please add title.';	}
							/*	if ($err==NULL && empty($description))
								{:''
									if(empty($external_link))	
									$err    =   'Please add external link.';								}
								if ($err==NULL && empty($external_link))
								{
									if(empty($description))	
									$err    =   'Please add description.';	
								} */
								
								if ($err==NULL)
                                {
                                	$obj_casestudy   =   new stdClass();
									$obj_casestudy->casestudy_id	=   $casestudyId;
									$obj_casestudy->type			=   $type;
									$obj_casestudy->source  	 	=   (empty($external_link))?'N':'E';
									$obj_casestudy->title			=   $title;
									$obj_casestudy->sub_title		=   (!empty($sub_title))?$sub_title:'';
									$obj_casestudy->imagepath		=	$imagename;
									$obj_casestudy->description		=   $description;
									$obj_casestudy->external_link	=   (!empty($external_link))?$external_link:'';
                                	
                                	if (!empty($_FILES['imagepath'])){
										if (empty($_FILES['imagepath']['error']))
										{
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['imagepath']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['imagepath']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											
											$fileName	=	$images->uploadimage($_FILES['imagepath'], time().uniqid(), _CASESTUDY_ORGS);
											if ($fileName!=NULL)
											{	$obj_casestudy->imagepath	=	$fileName;		}
											else
											{	$err	=	'Error: uploading Image.';			}
										}
									}
                                	if ($err==NULL)
                                	{ 
	                                    if ($casestudy->updateCasestudy($obj_casestudy))
	                                    {  exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'casestudies/view?id='.$casestudyId)));	}
                                	}
                                	$err    =   ($err==NULL)?'Unable to update Case study or White Papper.':$err;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			foreach($_GET as $key=>$val)
			{	$$key   = $val;		}
			$action     =   (isset($action))?(($action=='edit')?$action:'add'):'add';
			$this->current		=	'casestudies';
    		$this->pagetitle	=	'resources';
    		$this->formhead		=	'casestudy';
			if (!empty($id))
			{	$obj_casestudy   =   $casestudy->getCasestudyById($id);	}
			else
			{   $action     =   'add';	}
		}
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'casestudies/add.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}
	
	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}

		if (!empty($id))
		{	
			$casestudy		=	new Casestudy();
			if ($casestudy->deleteCasestudy($id,$status))
			{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Resource '.(($status=='N')?"disabled":"enabled").' successfully.')));	}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.'))); 
	}

} 
?>