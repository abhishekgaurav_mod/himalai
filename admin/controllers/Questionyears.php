<?php
class Questionyears extends Controller {
	
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}
			
	public function index()
	{
		$this->islogged();
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}
    	
    	$status     =   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
    	$this->current	=	'questionyears';
    	$this->pagetitle=	'Question Years';
    	 
    	$ajaxSource	=	_URL.'questionyears/getquestionyears';
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'questionyears/index.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}
	
	public function getquestionyears()
	{
		$this->islogged();
		foreach($_POST as $key=>$val)
		{	$$key   = $val;	}
		
		if (!empty($status))
		{
			$sTable			=	'questionyears';
			$sIndexColumn	=	'questionyear_id';
			$aColumns 		= 	array(	'questionyear_id','questionyear_name','exam_type','question_year','created_on','updated_on','enabled','questionyear_id');
			$condition		=	((isset($status))?" enabled = '".$status."'":'');
			$datatable		=	new DataTable($sTable, $aColumns, $sIndexColumn, $condition);
			$datatable->output();
		}
	}
	
	public function view()
	{
		$this->islogged();
		$this->current	=	'questionyears';
		$this->pagetitle=	'Question Year';
		foreach($_GET as $key=>$val)
		{	$$key   = $val;	}
	
		if (!empty($id))
		{
			$questionyear			=	new Questionyear();		
			$obj_questionyear		=	$questionyear->getQuestionyearById($id);
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'questionyears/view.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}
	
	public function add()
	{
		$this->islogged();
		$questionyear		=	new Questionyear();
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{
				if(gettype($val)=='string')
				{	$$key	= trim(strip_tags($val));	}	
				else
				{	$$key	= $val;		}
			}
			
			$err        =   NULL;
			switch($action)	
			{
				case 'add'	:  	if ($err==NULL && empty($title))
								{	$err    =   'Please enter question year.';	}
								if ($err==NULL && empty($exam_type))
								{	$err    =   'Please select exam type.';	}
								if ($err==NULL && empty($question_year))
								{	$err    =   'Please enter question year';	}
								
								if ($err==NULL)
								{
									$obj_questionyear   =   (Object)'';
									$obj_questionyear->questionyear_name  	=   $title;
									$obj_questionyear->exam_type  			=   $exam_type;
									$obj_questionyear->question_year		=   $question_year;
									
									if ($err==NULL)
									{	
										$questionyearId     =   $questionyear->addQuestionyear($obj_questionyear);
										if (!empty($questionyearId))
										{	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'questionyears/view?id='.$questionyearId)));	}
									}
									$err    =   ($err==NULL)?'Unable to add questionyear.':$err;
								}
				break;
				case 'edit'	:	$obj_questionyear  =   (Object)'';
								if ($err==NULL && empty($title))
								{	$err    =   'Please enter questionyear title.';	}
								if ($err==NULL && empty($exam_type))
								{	$err    =   'Please select exam type.';	}
								if ($err==NULL && empty($question_year))
								{	$err    =   'Please enter question year';	}
								
								if ($err==NULL)
                                {
									$obj_questionyear   =   (Object)'';
                                	$obj_questionyear->questionyear_id  	=   $questionyearId;
									$obj_questionyear->questionyear_name  	=   $title;
									$obj_questionyear->exam_type  			=   $exam_type;
									$obj_questionyear->question_year	=   $question_year;
									
                                	if ($err==NULL)
                                	{ 
	                                    if ($questionyear->updateQuestionyear($obj_questionyear))
	                                    {   exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'questionyears/view?id='.$questionyearId)));	}
                                	}
                                	$err    =   ($err==NULL)?'Unable to update questionyear.':$err;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			foreach($_GET as $key=>$val)
			{	$$key   = $val;		}
			$action     =   (isset($action))?(($action=='edit')?$action:'add'):'add';
			$this->current		=	'questionyears';
			$this->pagetitle	=	'Question Year';
			$this->formhead		=	'Question Year';
			if (!empty($id))
			{	$obj_questionyear   =   $questionyear->getQuestionyearById($id);	}
			else
			{   $action     =   'add';	}
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'questionyears/add.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}
	
	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}

		if (!empty($id))
		{	
			$questionyear	=	new Questionyear();
			if ($questionyear->deleteQuestionyear($id,$status))
			{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Questionyear '.(($status=='N')?"disabled":"enabled").' successfully.')));	}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.'))); 
	}
} 
?>