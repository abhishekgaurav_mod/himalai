<?php
class Testimonials extends Controller {

	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function index()
	{
		$this->islogged();
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}

    	$status     =   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
    	$this->current	=	'testimonials';
    	$this->pagetitle=	'Testimonials';

    	$ajaxSource	=	_URL.'testimonials/gettestimonials';
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'testimonials/index.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function gettestimonials()
	{
		$this->islogged();
		foreach($_POST as $key=>$val)
		{	$$key   = $val;	}

		if (!empty($status))
		{
			$sTable			=	'testimonials';
			$sIndexColumn	=	'testimonial_id';
			$aColumns 		= 	array(	'testimonial_id','name','testimonial_type','student_type','testimonial_order','publishing_date','created_on','updated_on','enabled','testimonial_id');
			$condition		=	((isset($status))?" enabled = '".$status."'":'');
			$datatable		=	new DataTable($sTable, $aColumns, $sIndexColumn, $condition, $outType='array');
			$data			=	$datatable->output();
			/*if(!empty($data)){
				for($i= 0; $i<count($data['aaData']); $i++)
				{	$data['aaData'][$i][3]	=	($data['aaData'][$i][3] == 'E')?'Events':(($data['aaData'][$i][3] == 'N')?'News':'Workshop');		}
			}*/
			echo json_encode($data);
		}
	}

	public function view()
	{
		$this->islogged();
		$this->current		=	'testimonials';
    	$this->pagetitle	=	'Testimonials';

		foreach($_GET as $key=>$val)
		{	$$key   = $val;	}

		if (!empty($id))
		{
			$testimonial		=	new Testimonial();
			$object   			=   $testimonial->getTestimonialsById($id);
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'testimonials/view.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function add()
	{
		$this->islogged();
		$testimonial		=	new Testimonial();

		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{
				if(gettype($val)=='string')
				{	$$key	= trim(strip_tags($val));	}
				else
				{	$$key	= $val;		}
			}

			$name				=	trim(strip_tags($name));
			$publishing_date	=	trim(strip_tags($publishing_date));
			$tags				=	(!empty($tags))?trim(strip_tags($tags)):'';
			$description		= 	($_POST['description']);

			$err        =   NULL;
			switch($action)
			{
				case 'add'	:  	if ($err==NULL && empty($name))
								{	$err    =   'Please enter name.';	}
								if ($err==NULL && $testimonial->checkTestimonialSlug(Common::slugify($name)) )
								{ 	$err    =   'Name already in use.'; 	}
								/*if ($err==NULL && (empty($_FILES['authorimagepath'])))
								{	$err	=	'Please upload Author Image.';		}*/
								if ($err==NULL && empty($publishing_date))
								{	$err    =   'Please enter publishing date.';	}
								if ($err==NULL && empty($testimonial_type))
								{	$err    =   'Please select testimonial type.';	}
								if ($err==NULL && $testimonial_type == 'S' && empty($student_type))
								{	$err    =   'Please select student type.';	}
								if ($err==NULL && empty($description))
								{	$err    =   'Please enter description.';	}

								if ($err==NULL)
								{
									$object						=   (Object)'';
									$object->name  				=   $name;
									$object->tags  				=   $tags;
									$object->publishing_date 	=   date('Y-m-d H:i:s',strtotime($publishing_date));
									$object->description 		=   $description;
									$object->testimonial_type 	=   $testimonial_type;
									$object->student_type 		=   $student_type;
									$object->testimonial_order 	=   $testimonial_order;
									$object->authorimagepath  	=   null;
									$object->testimonialimagepath  		=   null;
                  					$object->slug           	=   Common::slugify($name);

									if (!empty($_FILES['authorimagepath'])){
										if (empty($_FILES['authorimagepath']['error']))
										{
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['authorimagepath']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['authorimagepath']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}

											$authorImgName	=	$images->uploadimage($_FILES['authorimagepath'], time().uniqid(), _TESTIMONIAL_AUTHOR_ORGS);
											if ($authorImgName!=NULL)
											{	$object->authorimagepath	=	$authorImgName;	}
											else
											{	$err	=	'Error: uploading Author Image.';		}
										}
									}

									if (!empty($_FILES['testimonialimagepath'])){
										if (empty($_FILES['testimonialimagepath']['error']))
										{
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['testimonialimagepath']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['testimonialimagepath']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}

											$testimonialImgName	=	$images->uploadimage($_FILES['testimonialimagepath'], time().uniqid(), _TESTIMONIAL_ORGS);
											if ($testimonialImgName!=NULL)
											{	$object->testimonialimagepath	=	$testimonialImgName;	}
											else
											{	$err	=	'Error: uploading Testimonial Image.';		}
										}
									}
									if ($err==NULL)
									{
										$testimonialId     =   $testimonial->addTestimonials($object);
										if (!empty($testimonialId))
										{	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'testimonials/view?id='.$testimonialId)));		}
									}
									$err    =   ($err==NULL)?'Unable to add testimonial.':$err;
								}
				break;
				case 'edit'	:	if ($err==NULL && empty($name))
								{	$err    =   'Please enter name.';	}
								if ($err==NULL && empty($publishing_date))
								{	$err    =   'Please enter publishing date.';	}
								if ($err==NULL && empty($testimonial_type))
								{	$err    =   'Please select testimonial type.';	}
								if ($err==NULL && $testimonial_type == 'S' && empty($student_type))
								{	$err    =   'Please select student type.';	}
								if ($err==NULL && empty($description))
								{	$err    =   'Please enter description.';	}


								if ($err==NULL)
                                {
									$object   					=   new stdClass();
									$object->testimonial_id		=   $testimonialId;
									$object->name  				=   $name;
									$object->tags  				=   $tags;
									$object->publishing_date 	=   date('Y-m-d H:i:s',strtotime($publishing_date));
									$object->description 		=   $description;
									$object->testimonial_type 	=   $testimonial_type;
									$object->student_type 		=   $student_type;
									$object->testimonial_order 		=   $testimonial_order;
									$object->authorimagepath  		=   $authorImageId;
									$object->testimonialimagepath  	=   $testimonialImageId;
                  					$object->slug           	=   Common::slugify($name);

									if (!empty($_FILES['authorimagepath'])){
										if (empty($_FILES['authorimagepath']['error']))
										{
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['authorimagepath']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['authorimagepath']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}

											$authorImgName	=	$images->uploadimage($_FILES['authorimagepath'], time().uniqid(), _TESTIMONIAL_AUTHOR_ORGS);
											if ($authorImgName!=NULL)
											{	$object->authorimagepath	=	$authorImgName;	}
											else
											{	$err	=	'Error: uploading Author Image.';		}
										}
									}

									if (!empty($_FILES['testimonialimagepath'])){
										if (empty($_FILES['testimonialimagepath']['error']))
										{
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['testimonialimagepath']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['testimonialimagepath']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}

											$testimonialImgName	=	$images->uploadimage($_FILES['testimonialimagepath'], time().uniqid(), _TESTIMONIAL_ORGS);
											if ($testimonialImgName!=NULL)
											{	$object->testimonialimagepath	=	$testimonialImgName;	}
											else
											{	$err	=	'Error: uploading Testimonial Image.';		}
										}
									}
                                	if ($err==NULL)
                                	{
	                                    if ($testimonial->updateTestimonials($object))
	                                    {	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'testimonials/view?id='.$testimonialId)));		}
                                	}
                                	$err    =   ($err==NULL)?'Unable to update testimonial.':$err;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			foreach($_GET as $key=>$val)
			{	$$key   = $val;		}
			$action     =   (isset($action))?(($action=='edit')?$action:'add'):'add';

			$this->current		=	'testimonials';
    		$this->pagetitle	=	'Testimonials';
			$this->formhead		=	'Testimonials';
			if (!empty($id))
			{	$object   =   $testimonial->getTestimonialsById($id);		}
			else
			{   $action     =   'add';	}
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'testimonials/add.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}

		if (!empty($id))
		{
			$testimonial		=	new Testimonial();
			if ($testimonial->deleteTestimonials($id,$status))
			{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Testimonial'.(($status=='N')?"disabled":"enabled").' successfully.')));	}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.')));
	}
}
?>
