<?php
class Gsoptionalsubjects extends Controller {

	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function index()
	{
		$this->islogged();
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}

    	$status     =   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
    	$this->current	=	'gsoptionalsubjects';
    	$this->pagetitle=	'GS Optional Subject';

    	$ajaxSource	=	_URL.'gsoptionalsubjects/getgsoptionalsubjects';
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'gsoptionalsubjects/index.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function getgsoptionalsubjects()
	{
		$this->islogged();
		foreach($_POST as $key=>$val)
		{	$$key   = $val;	}

		if (!empty($status))
		{
			$sTable			=	'gs_optional_subjects';
			$sIndexColumn	=	'gsoptionalsubject_id';
			$aColumns 		= 	array(	'gsoptionalsubject_id','name','created_on','updated_on','enabled','gsoptionalsubject_id');
			$condition		=	((isset($status))?" enabled = '".$status."'":'');
			$datatable		=	new DataTable($sTable, $aColumns, $sIndexColumn, $condition, $outType='array');
			$data			=	$datatable->output();

			echo json_encode($data);
		}
	}

	public function view()
	{
		$this->islogged();
		$this->current		=	'gsoptionalsubjects';
    	$this->pagetitle	=	'GS Optional Subject';

		foreach($_GET as $key=>$val)
		{	$$key   = $val;	}

		if (!empty($id))
		{
			$gsoptionalsubject		=	new Gsoptionalsubject();
			$object   			=   $gsoptionalsubject->getGsoptionalsubjectsById($id);
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'gsoptionalsubjects/view.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function add()
	{
		$this->islogged();
		$gsoptionalsubject		=	new Gsoptionalsubject();

		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{
				if(gettype($val)=='string')
				{	$$key	= trim(strip_tags($val));	}
				else
				{	$$key	= $val;		}
			}

			$name				=	trim(strip_tags($name));
			$description		= 	($_POST['description']);

			$err        =   NULL;
			switch($action)
			{
				case 'add'	:  	if ($err==NULL && empty($name))
								{	$err    =   'Please enter name.';	}
								if ($err==NULL && $gsoptionalsubject->checkGsoptionalsubjectSlug(Common::slugify($name)) )
								{ 	$err    =   'Name already in use.'; 	}
								if ($err==NULL && (empty($_FILES['thumb'])))
								{	$err	=	'Please upload Thumb Image.';		}
								if ($err==NULL && empty($description))
								{	$err    =   'Please enter description.';	}

								if ($err==NULL)
								{
									$object						=   (Object)'';
									$object->name  				=   $name;
									$object->description 		=   $description;
									$object->thumb  			=   null;
                  					$object->slug           	=   Common::slugify($name);

									if (!empty($_FILES['thumb'])){
										if (empty($_FILES['thumb']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['thumb']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['thumb']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL)
											{	$thumbName	=	$images->uploadimage($_FILES['thumb'], time().uniqid(), _GS_OPTIONALSUBJECT_THUMB_ORGS); }
											if ($thumbName!=NULL)
											{	$object->thumb	=	$thumbName;	}
											else
											{	$err	=	'Error: uploading Thumb Image.'; }
										}
									}

									if ($err==NULL)
									{
										$gsoptionalsubjectId     =   $gsoptionalsubject->addGsoptionalsubjects($object);
										if (!empty($gsoptionalsubjectId))
										{	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'gsoptionalsubjects/view?id='.$gsoptionalsubjectId)));		}
									}
									$err    =   ($err==NULL)?'Unable to add gsoptionalsubject.':$err;
								}
				break;
				case 'edit'	:	if ($err==NULL && empty($name))
								{	$err    =   'Please enter name.';	}
								if ($err==NULL && empty($description))
								{	$err    =   'Please enter description.';	}


								if ($err==NULL)
                                {
									$object   					=   new stdClass();
									$object->gsoptionalsubject_id		=   $gsoptionalsubjectId;
									$object->name  				=   $name;
									$object->description 		=   $description;
									$object->thumb  			=   $thumbId;
                  					$object->slug           	=   Common::slugify($name);

									if (!empty($_FILES['thumb'])){
										if (empty($_FILES['thumb']['error']))
										{
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['thumb']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['thumb']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL)
											{	$thumbName	=	$images->uploadimage($_FILES['thumb'], time().uniqid(), _GS_OPTIONALSUBJECT_THUMB_ORGS); }
											if ($thumbName!=NULL)
											{	$object->thumb	=	$thumbName;	}
											else
											{	$err	=	'Error: uploading Thumb Image.';		}
										}
									}

                                	if ($err==NULL)
                                	{
	                                    if ($gsoptionalsubject->updateGsoptionalsubjects($object))
	                                    {	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'gsoptionalsubjects/view?id='.$gsoptionalsubjectId)));		}
                                	}
                                	$err    =   ($err==NULL)?'Unable to update gsoptionalsubject.':$err;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			foreach($_GET as $key=>$val)
			{	$$key   = $val;		}
			$action     =   (isset($action))?(($action=='edit')?$action:'add'):'add';

			$this->current		=	'gsoptionalsubjects';
    		$this->pagetitle	=	'GS Optional Subject';
			$this->formhead		=	'GS Optional Subject';
			if (!empty($id))
			{	$object   =   $gsoptionalsubject->getGsoptionalsubjectsById($id);		}
			else
			{   $action     =   'add';	}
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'gsoptionalsubjects/add.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}

		if (!empty($id))
		{
			$gsoptionalsubject		=	new Gsoptionalsubject();
			if ($gsoptionalsubject->deleteGsoptionalsubjects($id,$status))
			{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Gsoptionalsubject'.(($status=='N')?"disabled":"enabled").' successfully.')));	}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.')));
	}
}
?>
