<?php
class Gallerynewsevents extends Controller {
	
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}
			
	public function index()
	{
		$this->islogged();
		$newsevent		=	new Newsandevent();
		
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}
		
    	/*if(empty($id))
		{	header('Location:'._URL.'newsevents/');	}*/
		
    	$status     =   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
    	$this->current	=	'newsevents';
		
		$newseventId 		 =  $id; 
		$obj_newsevent	 =	$newsevent->getNewsAndEventsById($newseventId);
		$this->pagetitle =	'Newsevent Images ('.$obj_newsevent->title.')';
		
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'newsgalleries/index.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}
	
	public function add()
	{
		$this->islogged();
		$newsevent		=	new Newsandevent();
		
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= trim(strip_tags($val));	}
			
			$err        =   NULL;
			switch($action)	
			{
				case 'add'	:  	if ($err==NULL && (empty($_FILES['imagepath'])))
								{	$err	=	'Please upload Image.';		}
								
								if ($err==NULL)
								{
									$obj_newsevent   =   (Object)'';
									$obj_newsevent->news_event_id	=	$newseventId;
									$obj_newsevent->title	=	$title;
									
									if (!empty($_FILES['imagepath'])){
										if (empty($_FILES['imagepath']['error']))
										{
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['imagepath']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['imagepath']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';		}
												
											$fileName	=	$images->uploadimage($_FILES['imagepath'], time().uniqid(), _NEWS_EVENT_GALLERY_ORGS);											
											chmod(_NEWS_EVENT_GALLERY_ORGS.$fileName,0777);
											if ($fileName!=NULL)
											{
												$thumbsizes	=   unserialize(_GALLERY_THUMB_SIZE);
												/*if (!empty($thumbsizes)){
													foreach ($thumbsizes as $size){
														$images->createthumnail(_NEWS_EVENT_GALLERY_ORGS, $fileName, _GALLERY_THUMBS_ORGS, 'ti_'.$size.'_'.$fileName, $size);
													}
												}*/
												$obj_newsevent->imagepath	=	$fileName;
											}
											else
											{	$err	=	'Error: uploading Newsevent picture.';	}
										}
									}
									if ($err==NULL)
									{	
										$imageId     =   $newsevent->addImage($obj_newsevent);
										if (!empty($imageId))
										{	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'gallerynewsevents/index?id='.$newseventId)));	}
									}
									$err    =   ($err==NULL)?'Unable to add Images.':$err;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			foreach($_GET as $key=>$val)
			{	$$key   = $val;		}
			$action     		=   'add';
			
			/*if(empty($id))
			{	
				header('Location:'._URL.'newsevents/');
				exit(json_encode(array('status'=>false, 'redirect'=>true, 'url'=>_URL.'newsevents/')));
    		}*/
			$this->current		=	'newsevents';
    		$newseventId 		= $id; 
			$newsevent		=	new Newsandevent();
			$obj_newsevent	=	$newsevent->getNewsAndEventsById($newseventId);
			$this->pagetitle =	'Newsevent Images ('.$obj_newsevent->title.')';
			$this->formhead		=	'image';
		}
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'newsgalleries/add.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}
	
	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}
           
		if (!empty($id))
		{	
			$newsevent	=	new Newsandevent();
			if ($newsevent->deleteImage($id,$status))
			{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Image '.(($status=='N')?"disabled":"enabled").' sccuessfully.')));	}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.'))); 
	}
	public function paginate()
	{
		$this->islogged();
		$newsevent	=	new Newsandevent();
		
		foreach($_GET as $key=>$val)
		{	$$key	= $val;	}
	
		$err	=	NULL;
		if(!empty($type))
		{
			switch($type)
			{
				case 'image' : 	
								$count			=	$newsevent->getImagesCount($id);
								$start			=	0;
								$end			=	_IMAGE_LIMIT;
								if ($count>0)
								{
									$max_page	=	ceil(($count)/_IMAGE_LIMIT);
									$page		=	($page<0)?0:$page;
									$start		=	($page-1)*_IMAGE_LIMIT;
															
									$next		=	($page<$max_page)?'<a href="javascript:void(0);" onclick="javascript:getimages('.($page+1).','.$id.')">Next</a>':'Next';
									$prev		=	($page>1)?'<a href="javascript:void(0);" onclick="javascript:getimages('.($page-1).','.$id.')">Previous</a>':'Previous';
												
									$obj_images	=	$newsevent->getImages($start,$end,$id);
									$rows		=	NULL;
									if (!empty($obj_images))
									{
										foreach($obj_images as $obj_image)
										{
											$rows	.= '<div class="imgbox">
															<span class="image"><img src="'._NEWS_EVENT_GALLERY_ORG_URL.$obj_image->imagepath.'" /></span>
															<span>
																<ul>
																	<li><a href="#" onclick="javascript:deleters(\'Newsevent Image\', \''.$obj_image->image_id.'\', \'N\', \'Delete\');">Delete</a></li>
																	<li><a href="'._NEWS_EVENT_GALLERY_ORG_URL.$obj_image->imagepath.'" rel="imagebox">Full Size</a></li>
																</ul>	
															</span>
														</div>';
										}
									}
									echo '	<div class="backpaginate">
												<ul>
													<li>'.$prev.'</li>
													<li>'.$next.'</li>
												</ul>
											</div>
											<div class="backimages">'.$rows.'</div>';
								}
								else
								{	echo '<div class="backimages textalignCenter">No Images Added.</div>';	}
								exit();
					break;
					
				default: exit('');
			}
		}
	}
} 
?>