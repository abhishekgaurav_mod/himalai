<?php
class Boot
{
	var $url		=	NULL;
	var $target		=	NULL;
	var $controller	=	NULL;
	var $method		=	NULL;
	
	public function __construct() {
		$this->url			=	'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$this->controller	=	'Base';
		$this->method		=	'index';
    }
    
    public function loadController()
    {
    	try 
    	{
	    	if ($this->url==_URL)
	    	{
				$this->controller	=	'Base';
				$this->method		=	'index';	
	    	}
	    	else
	    	{
	    		try 
	    		{
	    			$this->url	=	trim($this->url,'?/');
	    		    if (strpos($this->url,'?'))
	     			{
	     				$str		=	explode('?',$this->url);
	     				$this->url	=	(!empty($str))?$str[0]:$this->url;
	     			}
	     			$this->target =	trim(substr($this->url, strlen(_URL)),'/');
	    		    if (strpos($this->target,'/'))
	     			{
	     				$str	=	explode('/',$this->target);
						$this->controller	=	$str[0];
						$this->method		=	$str[1];	
					}
					else
					{	$this->controller	=	$this->target;	}
					$this->controller		=	ucfirst(strtolower($this->controller));
					if (!method_exists($this->controller,$this->method))
					{	
						$this->controller	=	'Base';
						$this->method		=	'error_404';
					}
	    		}
	    	    catch (Exception $e) 
	        	{   exit('Caught exception: '.$e->getMessage());    }
	    	}
	    	$instance = new $this->controller;
    		$instance->call($this->method);
    	}
    	catch (Exception $e)
    	{	exit('Caught exception: '.$e->getMessage());	}
	}
    
}
?>