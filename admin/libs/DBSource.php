<?php
class DBSource
{
    public $result;
    public $rows;
    public $fields;
    public $change_rows;
    public $fetch_rows;
    public $insertId;
    public $connection;

    public function __construct()
    {
        try
        {   $this->connection = DBConnect::getInstance()->getConnection();	}
        catch(Exception $e)
        {	throw new Exception('Could not get database Connection '.$e);	}
    }
    public function __destruct(){   /* mysqli_close($this->connection);  */  }
    public function closeConnection(){
    	mysqli_close($this->connection);
    }
    
    public function db_free_results($result)
    {   mysqli_free_result($result);   }
    public function db_query($query) 
    {   
        if(($this->result   =   mysqli_query($this->connection,$query))!=null)
        {
            $this->insertId =   mysqli_insert_id($this->connection);
            return($this->result);
        }
        else
        {   return false;   }
    }
    public function db_fetch_array($res)
    {
        @$temp = mysqli_fetch_array($res, MYSQLI_BOTH);
        return($temp);
    }
    public function db_fetch_object($res)
    {
        @$temp = mysqli_fetch_object($res);
        return ($temp);
    }
    public function db_fetch_row($res)
    {
        @$this->fetch_rows = mysqli_fetch_row($res);
        return($this->fetch_rows);
    }
    public function db_num_rows($res)
    {
        @$this->rows = mysqli_num_rows($res);
        return($this->rows);
    }
    public function db_num_fields($res)
    {
        @$this->fields = mysqli_num_fields($res);
        return($this->fields);
    }
    public function db_affected_rows($res)
    {
        $this->change_rows= mysqli_affected_rows($res);
        return($this->change_rows);
    }
    public function beginTransaction()
    {   return @mysql_query("BEGIN", $this->connection);    }

    public function commitTransaction()
    {   return @mysql_query("COMMIT", $this->connection);   }
    
    public function rollBackTransaction()
    {   return  @mysql_query("ROLLBACK", $this->connection);    }

    public function mysqlInsertId()
    {   return $this->insertId; }

    public function mysqlEscapeString($str)
    {   return mysqli_real_escape_string($this->connection, $str);  }
}
?>