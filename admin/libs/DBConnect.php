<?php
class DBConnect {
    private static $instance;
    private $connection; 
    private function __construct() 
    {	$this->connection = mysqli_connect(_DB_HOST, _DB_USER, _DB_PASS, _DB_NAME) or die('Connection Error : ' . mysqli_connect_error()) ;	}
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }
        return self::$instance;
    }
	public function getConnection() 
	{	return $this->connection;    }
    public function __clone() 
    {   trigger_error('Clone is not allowed.', E_USER_ERROR);   }
}
?>
