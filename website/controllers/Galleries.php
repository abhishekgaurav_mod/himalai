<?php
class Galleries extends Controller {
	
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}
			
	public function index()
	{		
		$this->current	=	'galleries';
			
		$gallery	=	new Gallery();
		$arr_gallery	=	$gallery->getGalleries();
		
		$this->metatitle 		= 'Galleries';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'galleries/index.php';
		include _PARTIAL_ROOT.'_footer.php';
	}
	
	
} 
?>