<?php
class Base extends Controller {
	var $current=	false;
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function __call($method, $args) {
		try{
			$helper = new Helpers();
			$page_code =  Common::urlToPageCode($method);
			$helper->setPage($page_code);
			exit();
		}catch (Exception $e){ $this->error_log($e); }
	}

	public function index() {
		$this->current	=	false;

		$sliders		=	new Slider();
		$cms			=	new Cmscontent();
		$testimonial	=	new Testimonial();

		$arr_sliders			=	$sliders->getSlidersByType('M');
		$cmsContent				=	$cms->getCmscontentByType('home');
		$arr_clearedtestimonial	=	$testimonial->getHomeTestimonialByType('CC');
		$arr_studenttestimonial	=	$testimonial->getHomeTestimonialByType('S');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'index.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function branches() {
		$this->current	=	'branches';

		$this->metatitle 		= 'Branches';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/branches.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function kascoaching() {
		$this->current	=	'kas-coaching';

		$sliders		=	new Slider();
		$cms			=	new Cmscontent();
		$testimonial	=	new Testimonial();

		$arr_sliders	=	$sliders->getSlidersByType('KAS');

		$this->metatitle 		= 'KAS Coaching';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'kascoaching/index.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function generalstudiesoptional() {
		$this->current	=	'general-studies-optional';

		$datacontent		=	new Datacontent();

		$arr_subjects		=	$datacontent->getGsOptionalSubjects();

		$this->metatitle 		= 'General Studies Optional Papers';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/gs/generalstudiesoptional.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function stagesofkasexam() {
		$this->current	=	'stages-of-kas-exam';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('stagesofkasexam');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'kascoaching/stagesofkasexam.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function kaseligibility() {
		$this->current	=	'stages-of-kas-exam';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('kaseligibility');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'kascoaching/kaseligibility.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function kasexam() {
		$this->current	=	'stages-of-kas-exam';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('kasexam');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'kascoaching/kasexam.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function aboutus() {
		$this->current	=	'about-us';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('aboutus');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/aboutus.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function ourguide() {
		$this->current	=	'our-guide';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('ourguide');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/ourguide.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function examtrends() {
		$this->current	=	'exam-trends';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('examtrends');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/examtrends.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function teachingmethodologyandachievements() {
		$this->current	=	'teaching-methodology-and-achievements';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('methodologyandachievements');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/methodologyandachievements.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function whyhimalai() {
		$this->current	=	'why-himalai';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('whyhimalai');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/whyhimalai.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function stagesofiasexam() {
		$this->current	=	'stages-of-ias-exam';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('stagesofiasexam');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/stagesofiasexam.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function generalstudiesmains() {
		$this->current	=	'general-studies-mains';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('generalstudiesmains');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/gs/generalstudiesmains.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function iasexameligibility() {
		$this->current	=	'general-studies-mains';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('iasexameligibility');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/iasexam/iasexameligibility.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function iasgeneralinstructions() {
		$this->current	=	'general-studies-mains';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('iasgeneralinstructions');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/iasexam/iasgeneralinstructions.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function iasreservationissues() {
		$this->current	=	'general-studies-mains';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('iasreservationissues');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/iasexam/iasreservationissues.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function iasphcategory() {
		$this->current	=	'general-studies-mains';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('iasphcategory');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/iasexam/iasphcategory.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function iasserviceallocation() {
		$this->current	=	'general-studies-mains';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('iasserviceallocation');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/iasexam/iasserviceallocation.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function iasmedicalexamofcandidates() {
		$this->current	=	'general-studies-mains';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('iasmedicalexamofcandidates');

		$this->metatitle 		= (!empty($cmsContent->title)?$cmsContent->title:'');
		$this->metakeyword 		= (!empty($cmsContent->metakeyword)?$cmsContent->metakeyword:'');
		$this->metadescription 	= (!empty($cmsContent->metadescription)?$cmsContent->metadescription:'');
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/iasexam/iasmedicalexamofcandidates.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function error_404()
	{
		$this->metatitle 		= '404 Page Not Found';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'error_404.php';
		include _PARTIAL_ROOT.'_footer.php';
		exit;
	}
}
?>
