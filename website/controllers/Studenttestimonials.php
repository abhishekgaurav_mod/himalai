<?php
class Studenttestimonials extends Controller {

	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function index()
	{
		$this->current	=	'studenttestimonials';

		$testimonial	=	new Testimonial();

		$arr_testimonial	=	$testimonial->getTestimonialByType('S');

		$this->metatitle 		= 'Student Testimonials';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';
		include _PARTIAL_ROOT.'_header.php';

		if (!empty($nav[1])) {
			$object			=   $testimonial->getTestimonialBySlug($nav[1]);
			if (!empty($object)) {
				include _VIEWS_ROOT.'studenttestimonials/testimonial-view.php';
			} else {
				include _VIEWS_ROOT.'error_404.php';
			}
		} else {
			include _VIEWS_ROOT.'studenttestimonials/index.php';
		}

		include _PARTIAL_ROOT.'_footer.php';
	}

}
?>
