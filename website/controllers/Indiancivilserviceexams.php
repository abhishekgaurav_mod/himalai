<?php
class Indiancivilserviceexams extends Controller {
	
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}
			
	public function index()
	{
		$this->current	=	'indian-civil-service-exams';

		$datacontent		=	new Datacontent();

		$arr_profiles		=	$datacontent->getCivilserviceprofiles();

		$this->metatitle 		= 'Indian Civil Service Exams';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/indiancivilserviceexams/index.php';
		include _PARTIAL_ROOT.'_footer.php';
	}
	
	
} 
?>