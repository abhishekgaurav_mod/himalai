<?php
class Contact extends Controller {

	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function index() {

		$this->current	=	'contact';
		$enquiry 		=	new Enquiry();
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= trim(strip_tags($val));	}

			$err        =   NULL;
			if ($err==NULL && empty($name))
			{	$err    =   'Please enter name.';	}
			if ($err==NULL && empty($phone))
			{	$err    =   'Please enter phone number.';	}
			if ($err==NULL && !preg_match("/^[0-9+ -]+$/", $phone))
	        {  $err    =   'Please enter valid contact number.';        }
	       if  ($err==NULL && !(strlen($phone) >= 10 && strlen($phone) < 14 ))
	        {  $err    =   'contact number should be min 10 digits.';        }
			if ($err==NULL && empty($email))
			{	$err    =   'Please enter email.';	}
			if ($err==NULL && !filter_var($email, FILTER_VALIDATE_EMAIL))
			{	$err    =   'Please enter valid email.';	}
			if ($err==NULL && empty($interest))
			{	$err    =   'Please enter Interest.';	}
			if ($err==NULL && empty($message))
			{	$err    =   'Please enter message.';	}

			if ($err==NULL)
			{
				$obj_enquiry   =   (Object)'';
				$obj_enquiry->name  		=   $name;
				$obj_enquiry->phone  	    =   $phone;
				$obj_enquiry->email  		=   $email;
				$obj_enquiry->qualification = 	$qualification;
				$obj_enquiry->interest  	=   $interest;
				$obj_enquiry->message  		= 	$message;

				if ($err==NULL)
				{
					$enquiryId     =   $enquiry->addEnquiry($obj_enquiry);
					if (!empty($enquiryId))
					{
						$mailer		=	new Mailer();

						/*$sendTo			=	'info@himalaiiasclasses.com';*/
						$sendTo				=	'abhishek@motherofdesign.in';
						$mailSubject		=	'Himalai - Received New Enquiry';
						$contactBody        =	file_get_contents(_EMAIL_TEMPLATE.'mail_to_admin.inc');
						$contactBody        =   str_replace('{name}',$obj_enquiry->name,$contactBody);
						$contactBody        =   str_replace('{phone}',$obj_enquiry->phone,$contactBody);
						$contactBody        =   str_replace('{email}',$obj_enquiry->email,$contactBody);
						$contactBody        =   str_replace('{qualification}',$obj_enquiry->qualification,$contactBody);
						$contactBody        =   str_replace('{interest}',$obj_enquiry->interest,$contactBody);
						$contactBody        =   str_replace('{message}',$obj_enquiry->message,$contactBody);

						$mailer->SendHTMLEMail($sendTo, $mailSubject, $contactBody);

						echo json_encode(array('status'=>true,'prompt'=>true, 'htmlrefresh'=>false, 'message'=>'Thank you for contacting us.....', 'redirect'=>true, 'url'=>_URL.'contact/thankyou'));
						exit();
					}
				}
				$err    =   ($err==NULL)?'Unable to Send Enquiry.':$err;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}

		$this->metatitle 		= 'Contact Us';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'contact/index.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function thankyou() {
		$this->current		=	'thankyou';

		$this->metatitle 	= 'Thank You';
		include _VIEWS_ROOT.'contact/thankyou.php';
		header('refresh:3;url='._URL); die;
	}


}
?>
