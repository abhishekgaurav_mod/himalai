<?php
class Testimonials extends Controller {

	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function index()
	{
		$this->current	=	'testimonials';

		$testimonial	=	new Testimonial();

		$arr_testimonial	=	$testimonial->getTestimonialByType('CC');

		$this->metatitle 		= 'Testimonials';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';
		include _PARTIAL_ROOT.'_header.php';

		if (!empty($nav[1])) {
			$object			=   $testimonial->getTestimonialBySlug($nav[1]);
			if (!empty($object)) {
				include _VIEWS_ROOT.'testimonials/testimonial-view.php';
			} else {
				include _VIEWS_ROOT.'error_404.php';
			}
		} else {
			include _VIEWS_ROOT.'testimonials/index.php';
		}

		include _PARTIAL_ROOT.'_footer.php';
	}

}
?>
