<?php
class Questions extends Controller {

	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function index() {

		$this->current	=	'questions';

		$questionyears		=	new Question();
		$arr_prelimsYear	=	$questionyears->getQuestionyearsByExamType('P');
		$arr_mainsYear		=	$questionyears->getQuestionyearsByExamType('M');

		if (!empty($arr_prelimsYear)){
			foreach ($arr_prelimsYear as $obj_prelimsYear) {

				$arr_prelimsQuestionType		=	$questionyears->getQuestionTypeByQuestionyearId($obj_prelimsYear->questionyear_id);
				$obj_prelimsYear->prelimsQuestionType		= $arr_prelimsQuestionType;
				if (!empty($arr_prelimsQuestionType)){
					foreach ($arr_prelimsQuestionType as $object) {
						$arr_prelimsSubjectByType		=	$questionyears->getSubjectsByQuestionType($obj_prelimsYear->questionyear_id, $object->question_type);
						$object->prelimsSubjectByType	=	$arr_prelimsSubjectByType;
					}
				}
				$arr_prelimsSubject			=	$questionyears->getSubjectsByQuestionyearId($obj_prelimsYear->questionyear_id);
				$obj_prelimsYear->prelimsSubjects	=	$arr_prelimsSubject;
			}
		}

		if (!empty($arr_mainsYear)){
			foreach ($arr_mainsYear as $obj_mainsYear) {

				$arr_mainsQuestionType		=	$questionyears->getQuestionTypeByQuestionyearId($obj_mainsYear->questionyear_id);
				$obj_mainsYear->mainsQuestionType		= $arr_mainsQuestionType;
				if (!empty($arr_mainsQuestionType)){
					foreach ($arr_mainsQuestionType as $object) {
						$arr_mainsSubjectByType		=	$questionyears->getSubjectsByQuestionType($obj_mainsYear->questionyear_id, $object->question_type);
						$object->mainsSubjectByType	=	$arr_mainsSubjectByType;
					}
				}
				$arr_mainsSubject			=	$questionyears->getSubjectsByQuestionyearId($obj_mainsYear->questionyear_id);
				$obj_mainsYear->mainsSubjects	=	$arr_mainsSubject;
			}
		}

		$this->metatitle 		= 'Question Papers';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'questions/index.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

}
?>