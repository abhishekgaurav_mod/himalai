<?php
class Datacontent extends DBSource
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

    public function getCivilserviceprofiles() {
    	try
    	{
    		$sql = "SELECT * FROM civil_service_profiles WHERE enabled = 'Y' ORDER BY created_on ASC";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			for($i=0;$row=$this->db_fetch_object($res);$i++)
				{	$arr[$i] = $row; 	}
				$this->db_free_results($res);
				return $arr;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }

    public function getGsOptionalSubjects() {
        try
        {
            $sql = "SELECT * FROM gs_optional_subjects WHERE enabled = 'Y' ORDER BY created_on ASC";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0)
            {
                $this->db_free_results($res);
                return 0;
            }
            else
            {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

   

}
?>
