<?php
class Question extends DBSource
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

    public function getQuestionyearsByExamType($exam_type) {
        try {
            $sql = "SELECT * FROM questionyears
                    WHERE enabled = 'Y' AND exam_type = '".$exam_type."' ORDER BY question_year DESC";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getSubjectsByQuestionyearId($questionyearId) {
        try {
            $sql = "SELECT * FROM questionsubjects
                    WHERE enabled = 'Y' AND questionyear_id = '".$this->mysqlEscapeString($questionyearId)."' ORDER BY questionsubject_name ASC";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getQuestionTypeByQuestionyearId($questionyearId) {
        try {
            $sql = "SELECT * FROM questionsubjects
                    WHERE enabled = 'Y' AND questionyear_id = '".$this->mysqlEscapeString($questionyearId)."' GROUP BY question_type ORDER BY question_type ASC";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getSubjectsByQuestionType($questionyear_id, $question_type) {
        try {
            $sql = "SELECT * FROM questionsubjects
                    WHERE enabled = 'Y' AND questionyear_id = '".$this->mysqlEscapeString($questionyear_id)."' AND question_type = '".$this->mysqlEscapeString($question_type)."' ORDER BY questionsubject_name ASC";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

}
?>
