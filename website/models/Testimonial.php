<?php
class Testimonial extends DBSource
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

    public function getTestimonialByType($testimonial_type)
    {
    	try
    	{
    		$sql = "SELECT * FROM testimonials WHERE enabled = 'Y' AND testimonial_type = '".$this->mysqlEscapeString($testimonial_type)."' ORDER BY testimonial_order ASC";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			for($i=0;$row=$this->db_fetch_object($res);$i++)
				{	$arr[$i] = $row; 	}
				$this->db_free_results($res);
				return $arr;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }

    public function getHomeTestimonialByType($testimonial_type)
    {
        try
        {
            $sql = "SELECT * FROM testimonials WHERE enabled = 'Y' AND testimonial_type = '".$this->mysqlEscapeString($testimonial_type)."' ORDER BY rand() LIMIT 6 ";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0)
            {
                $this->db_free_results($res);
                return 0;
            }
            else
            {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

	public function getTestimonialById($id)
    {
    	try
    	{
    		$sql = "SELECT	* FROM testimonials WHERE testimonials_id	= '".$this->mysqlEscapeString($id)."' AND enabled = 'Y' ";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }

    public function getTestimonialBySlug($slug) {
        try {
            $sql = "SELECT  * FROM testimonials
                    WHERE slug = '".$this->mysqlEscapeString($slug)."' AND enabled = 'Y' ";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                $row    =   $this->db_fetch_object($res);
                $this->db_free_results($res);
                return $row;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

}
?>
