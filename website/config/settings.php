<?php
	$functional	=	array(	'_TITLE' => 'Himalai',
							'_FOOTER' => 'Copyright &copy; 2014. All Rights Reserved&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Designed &amp; Developed by <a href="http://www.motherofdesign.in/" class="interaction" target="_blank">Mother of Design</a>',
							'_URL_SLUG_LENGTH'=>100);

	$smtpsetting=	array(	'_SMTP_SERVER' => 'smtp.gmail.com',
							'_EMAIL_LOGIN' => 'connect@motherofdesign.in',
							'_EMAIL_PASSWORD' => 'MOD@2017',
							'_EMAIL_FROM_NAME' => 'Himalai',
							'_FROM_EMAIL' => 'connect@motherofdesign.in',
							'_EMAIL_REPLY_TO' => 'connect@motherofdesign.in',
							'_EMAIL_SMTPSECURE' => 'ssl', // ssl/tls/nothing
							'_EMAIL_PORT' => '465');

	setdefined(array_merge($database, $userdefind, $functional, $smtpsetting));

	// ROOT  Settings
	define('_CONTROLLERS_ROOT', _ROOT.'controllers/');
	define('_LIBS_ROOT', _ROOT.'libs/');
	define('_MODEL_ROOT', _ROOT.'models/');
	define('_VIEWS_ROOT', _ROOT."views/");
	define('_PARTIAL_ROOT',_VIEWS_ROOT."partial/");
	define('_STATIC_BLOCK',_PARTIAL_ROOT."staticblock/");
	define('_WEBROOT_ROOT',_ROOT."webroot/");
	define('_EMAIL_TEMPLATE',_VIEWS_ROOT."email_templates/");

	// URL Settings
	define('_WEBROOT',_URL."webroot/");
	define('_IMAGES',_WEBROOT."images/");
	define("_CSS",_WEBROOT."css/");
	define("_JS",_WEBROOT."js/");
	define("_DOCS",_WEBROOT."docs/");
	define("_PLUGINS",_WEBROOT."plugins/");

	define('_MEDIA_ROOT', _WEBROOT_ROOT.'media/');
	define('_MEDIA_URL', _URL.'webroot/media/');


	//about us gallery images
    define('_GALLERY_ORG_URL',_MEDIA_URL.'galleries/');
    define('_GALLERY_THUMBS_ORG_URL',_MEDIA_URL.'galleries/thumb/');

	//Testimonials  images
	define('_TESTIMONIAL_ORG_URL',_MEDIA_URL.'testimonials/');
    define('_TESTIMONIAL_AUTHOR_ORG_URL',_MEDIA_URL.'testimonials/author/');

    // Questions
	define('_QUESTION_ORG_URL',_MEDIA_URL.'questions/');
    define('_QUESTION_THUMB_ORG_URL',_MEDIA_URL.'questions/thumb/');

	// GS optional subject thumbs
    define('_GS_OPTIONALSUBJECT_THUMB_ORG_URL',_MEDIA_URL.'gsoptionalsubjectthumbs/');

    //Sliders  images
	define('_SLIDER_ORG_URL',_MEDIA_URL.'sliders/');

	function setdefined($defines){
		if (!empty($defines))
		{
			foreach ($defines as $key=>$val)
			{	define($key, $val);	}
		}
	}
?>
