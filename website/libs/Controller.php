<?php
class Controller {
	var $current;
	var $common;
	public function __construct(){
		$this->common		=	new Common();
		$this->current		=	NULL;
	}
	public function __destruct(){
		$db	=	new DBSource();
		$db->closeConnection();
	}
		
	public function call($function) 
	{	$this->$function();	}
	
	public function error_404()
	{	
		header('Location: '._URL);
		exit;	
	}
	public function getparams($controller, $method){
		$controller	=	strtolower($controller);
		$method		=	strtolower($method);
		$url 		= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$url		=	str_replace(_URL, '', $url);
		$url		=	str_replace($controller.'/', '', $url);
		$url		=	str_replace($method.'/', '', $url);
		return explode('/',$url);
	}
}
?>