<?php
class Helpers extends Controller {
public function __construct()
   {  parent::__construct();  }

    public function __destruct()
   {  parent::__destruct();   }

   public function index(){

   }

   public function setPage($page_code){
      switch ($page_code) {
         case (preg_match('/NEWS_AND_EVENTS.*/', $page_code) ? true : false) :
            $newsevents = new Newsevents();
            $newsevents->index();
         break;
         case (preg_match('/TESTIMONIALS.*/', $page_code) ? true : false) :
            $testimonial = new Testimonials();
            $testimonial->index();
         break;
         case (preg_match('/STUDENT_TESTIMONIALS.*/', $page_code) ? true : false) :
            $studenttestimonial = new Studenttestimonials();
            $studenttestimonial->index();
         break;
         case (preg_match('/QUESTIONS.*/', $page_code) ? true : false) :
            $questions = new Questions();
            $questions->index();
         break;
         case (preg_match('/CONTACT_US.*/', $page_code) ? true : false) :
            $contact = new Contact();
            $contact->index();
            die;
         break;
         case (preg_match('/INDIAN_CIVIL_SERVICE_EXAMS.*/', $page_code) ? true : false) :
            $icse = new Indiancivilserviceexams();
            $icse->index();
         break;
         case (preg_match('/ABOUT_US.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->aboutus();
            die;
         break;
         case (preg_match('/OUR_GUIDE.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->ourguide();
         break;
         case (preg_match('/WHY_HIMALAI.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->whyhimalai();
            die;
         break;
         case (preg_match('/EXAM_TRENDS.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->examtrends();
            die;
         break;
         case (preg_match('/TEACHING_METHODOLOGY_AND_ACHIEVEMENTS.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->teachingmethodologyandachievements();
            die;
         break;
         case (preg_match('/KAS_COACHING.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->kascoaching();
            die;
         break;
         case (preg_match('/STAGES_OF_KAS_EXAM.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->stagesofkasexam();
         break;
         case (preg_match('/KAS_ELIGIBILITY.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->kaseligibility();
         break;
         case (preg_match('/KAS_EXAM.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->kasexam();
            die;
         break;
         case (preg_match('/CONTACT_US.*/', $page_code) ? true : false) :
            $contact = new Contact();
            $contact->index();
         break;
         case (preg_match('/STAGES_OF_IAS_EXAM.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->stagesofiasexam();
         break;
         case (preg_match('/IAS_EXAM_ELIGIBILITY.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->iasexameligibility();
         break;
         case (preg_match('/IAS_EXAM.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->iasexam();
            die;
         break;
         case (preg_match('/GENERAL_STUDIES_MAINS.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->generalstudiesmains();
         break;
         case (preg_match('/GENERAL_STUDIES_OPTIONAL.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->generalstudiesoptional();
         break;
         case (preg_match('/IAS_GENERAL_INSTRUCTIONS.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->iasgeneralinstructions();
         break;
         case (preg_match('/IAS_RESERVATION_ISSUES.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->iasreservationissues();
         break;
         case (preg_match('/IAS_PH_CATEGORY.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->iasphcategory();
         break;
         case (preg_match('/IAS_SERVICE_ALLOCATION.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->iasserviceallocation();
         break;
         case (preg_match('/IAS_MEDICAL_EXAM_OF_CANDIDATES.*/', $page_code) ? true : false) :
            $staticPage = new Base();
            $staticPage->iasmedicalexamofcandidates();
         break;
         default:

         break;
      }
   }
}
?>
