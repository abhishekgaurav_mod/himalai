<section id="home" class="homeBanner">

   <?php if (!empty($arr_sliders)) { ?>
            <ul class="masterSlider">
            <?php foreach ($arr_sliders as $sliders) { ?>
                     <li class="item"><img src="<?=_SLIDER_ORG_URL?><?=$sliders->imagepath?>"></li>           
            <?php } ?>
            </ul>
   <?php } ?>

</section>

<section class="section padded-vertical-40 homeContent">
   <div class="container">

      <?php
         if (!empty($cmsContent)) {
            $page_body = str_replace('{_IMAGES}', _IMAGES, $cmsContent->description);
            $page_body = str_replace('{_URL}', _URL, $page_body);
            $page_body = str_replace('{_MEDIA_URL}', _MEDIA_URL, $page_body);

            echo $page_body;
         }
      ?>
   </div>   
</section>


<?php if (!empty($arr_clearedtestimonial)) { ?>
<section class="section padded-vertical-20 container">
   <div class="row">
      <div class="col-lg-10">
         <div class="section-title margin-bottom-40">
            <h4 class="text-uppercase margin-bottom-5"> <strong>Our Cleared Candidate</strong> Testimonials </h4>
            <span class="sep sep-primary sep-animated"></span>
         </div>
      </div>
   </div>
   <div class="row">
      <?php foreach ($arr_clearedtestimonial as $testimonial) { ?>
      <div class="post post-horizontal col-md-4 clearfix">
         <div class="post-image-thumb animated" data-animation="fadeInLeft" data-animation-delay="400">
            <?php if (!empty($testimonial->authorimagepath)) { ?>
               <img src="<?=_TESTIMONIAL_AUTHOR_ORG_URL.$testimonial->authorimagepath?>" alt="<?=$testimonial->name?>" />
            <?php } else { ?>
               <img src="<?=_IMAGES?>author.jpg" alt="<?=$testimonial->name?>" />
            <?php } ?>
         </div>
         <div class="post-content">
            <h5 class="post-title">
               <a href="<?=_URL.'testimonials/'.$testimonial->slug?>"><?=$testimonial->name?></a>
            </h5>
            <div class="post-meta">
               <?=(!empty($testimonial->tags)?'<span><i class="fa fa-bookmark" aria-hidden="true"></i>'.$testimonial->tags.'</span>':'')?>
               <span><i class="fa fa-clock-o"></i> <?=date("d-m-Y", strtotime($testimonial->publishing_date))?></span>
            </div>
            <p class="font-alt">
               <?=((strlen($testimonial->description)>80)?substr(strip_tags($testimonial->description), 0,80).'. . .':strip_tags($testimonial->description))?>
            </p>
            <a href="<?=_URL.'testimonials/'.$testimonial->slug?>" class="btn btn-primary btn-xs">Read more</a>
         </div>
      </div>
      <?php } ?>
   </div>
   <div class="row">
      <a href="<?=_URL?>testimonials" class="btn btn-primary btn-filled btn-sm pull-right">View All !</a>
   </div>
</section>

<div class="container">
   <div class="imgQuote"><img src="<?=_IMAGES?>quote-ashwin.png"></div>
</div>
<?php } ?><?php if (!empty($arr_studenttestimonial)) { ?>
<section class="section padded-vertical-60 container">
   <div class="row">
      <div class="col-lg-10">
         <div class="section-title margin-bottom-40">
            <h4 class="text-uppercase margin-bottom-5"> <strong>Our Student</strong> Testimonials </h4>
            <span class="sep sep-primary sep-animated"></span>
         </div>
      </div>
   </div>
   <div class="row">
      <?php foreach ($arr_studenttestimonial as $testimonial) { ?>
      <div class="post post-horizontal col-md-4 clearfix">
         <div class="post-image-thumb animated" data-animation="fadeInLeft" data-animation-delay="400">
            <?php if (!empty($testimonial->authorimagepath)) { ?>
               <img src="<?=_TESTIMONIAL_AUTHOR_ORG_URL.$testimonial->authorimagepath?>" alt="<?=$testimonial->name?>" />
            <?php } else { ?>
               <img src="<?=_IMAGES?>author.jpg" alt="<?=$testimonial->name?>" />
            <?php } ?>
         </div>
         <div class="post-content">
            <h5 class="post-title">
               <a href="<?=_URL.'student-testimonials/'.$testimonial->slug?>"><?=$testimonial->name?></a>
            </h5>
            <div class="post-meta">
               <?=(!empty($testimonial->tags)?'<span><i class="fa fa-bookmark" aria-hidden="true"></i>'.$testimonial->tags.'</span>':'')?>
               <span><i class="fa fa-clock-o"></i> <?=date("d-m-Y", strtotime($testimonial->publishing_date))?></span>
            </div>
            <p class="font-alt">
               <?=((strlen($testimonial->description)>80)?substr(strip_tags($testimonial->description), 0,80).'. . .':strip_tags($testimonial->description))?>
            </p>
            <a href="<?=_URL.'student-testimonials/'.$testimonial->slug?>" class="btn btn-primary btn-xs">Read more</a>
         </div>
      </div>
      <?php } ?>
   </div>
   <div class="row">
      <a href="<?=_URL?>student-testimonials" class="btn btn-primary btn-filled btn-sm pull-right">View All !</a>
   </div>
</section>

<?php } ?>

<?php  include _STATIC_BLOCK.'_getQuestion.php'; ?>