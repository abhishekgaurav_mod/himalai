<section id="home" class="home-default">

   <div class="owl-slideshow bg-carousel home-full-carousel" data-auto-play="7000"  data-pagination="false">

      <?php if (!empty($arr_sliders)) {
               foreach ($arr_sliders as $sliders) { ?>
                  <div class="bg-image" style="background-image: url('<?=_SLIDER_ORG_URL?><?=$sliders->imagepath?>');">
                  </div>                  
      <?php    }
            } ?>
   </div>

   <div class="overlay overlay-black editable-alpha" data-alpha="30"></div>
   <?php if (!empty($arr_sliders)) { ?>
      <div class="bannerCaption container dark vertical-center">
         <div class="row">
            <div class="col-md-8 col-lg-7 vertical-center">
               <div class="owl-carousel margin-bottom-20 animated" data-animation="zoomIn" data-single-item="true" data-auto-play="7000" data-pagination="true">
                  <?php foreach ($arr_sliders as $sliders) { ?>
                     <div class="bannerCaptionContainer">
                        <span class="sep sep-primary"></span>                   
                        <?=$sliders->description?>                     
                     </div>
                  <?php } ?>
               </div>
            </div>
         </div>
      </div>
   <?php } ?>

</section>

<div class="container">
   <div class="row padded-top-40">
      <div class="col-md-12">
         <div class="cms pageContent">
            <div class="heading">KAS Coaching highlights</div>
            <div class="desc">
               <ul>
                  <li><strong>KAS Classes by IAS and KAS toppers.</strong></li>
                  <li>Himalai is an art of helping students to clear exams with simple plan. </li>
                  <li>Focus on what to read and what not to read.</li>
                  <li>Focus on most important topics of current affairs.</li>
                  <li>Over come time management through multiple tests, exam series revision classes.</li>
                  <li>Simple notes from standard text books, Government documentaries etc.</li>
                  <li>Helping students for better understanding of syllabus, subjects, current affairs, for fast track preparation.</li>
                  <li>We will help you to understand what Government and exam conducting body expects from the students, first step in your success.</li>
                  <li>Our expertise since 1998 should be at your help in clearing exam your dream is our success.</li>
               </ul>
            </div>

            <div class="heading">Stages and Syllabus of KAS Exam</div>

            <div class="desc">
               <ul>
                  <li>Stage &ndash; 1: Preliminary Examination</li>
                  <li>Stage &ndash; 2: Main Examination</li>
                  <li>Stage &ndash; 3: Interview (Personality Test)</li>
                  <li>Eligibility</li>
                  <li>For Detailed info <a class="btn btn-primary" href="<?=_URL?>stages-of-kas-exam">Click Here</a></li>
               </ul>
            </div>

            <div class="subHeading">KAS Prelims classes commence.</div>
            <div class="subHeading">KAS Mains classes announced.</div>
         </div>
      </div>
   </div>
</div>


<?php  include _STATIC_BLOCK.'_getQuestion.php'; ?>




