<?php if (_ENV == 'prod') { ?>
	<!-- Google Code for enquiry Conversion Page -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 986592530;
	var google_conversion_language = "en";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "lxryCLj61GAQkuq41gM";
	var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/986592530/?label=lxryCLj61GAQkuq41gM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
<?php } ?>

<!DOCTYPE html>
<html lang="en">
<head>

<title><?=ucwords(!empty($this->metatitle)?$this->metatitle:_TITLE)?> - Himalai IAS Coaching Centre</title>
<style type="text/css">
	h1 { text-align: center; margin-top: 28vh; margin-bottom: 10px; }
	h2 { font-size: 14px; text-align: center; margin-bottom: 45px; }
	img { width: 200px; max-width: 60%; display: block; margin: 0 auto; }
</style>
</head>
<body>
	<div>
		<h1>Thank You !!</h1>
		<h2>You will be redirected to Himalai home page . . .</h2>
		<img src="<?=_IMAGES?>himalai-logo.svg">
	</div>
</body>
</html>