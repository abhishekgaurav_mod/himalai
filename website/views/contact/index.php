<div id="page-title">

   <div class="title-bar dark bg-black padded-vertical-60 pageHeadingBar">
        <div class="bg-image bg-image-3"></div>
        <div class="overlay overlay-black editable-alpha" data-alpha="70"></div> 
        <div class="container">  
            <span class="sep sep-primary sep-no-top"></span>
            <h2 class="text-uppercase margin-bottom-0"><strong>Contact</strong> Us</h2>
            <span class="text-xs pageSubHeading text-uppercase">Failure comes only when we forget our ideals and objectives and principles. - Jawaharlal Nehru</span>
        </div>
    </div>

   <?php  include _PARTIAL_ROOT.'_breadcrumb.php'; ?>

</div>

<div id="content">

   <div class="container">
      <div class="row padded-vertical-40">
         <div class="col-md-5">
            <h4>Visit <strong>Head Office</strong></h4><br>
            <address>
               <h5><strong class="red">RAJAJINAGAR</strong></h5>
               #458/A27/1, 1st floor, 7th main, 4th Block<br>
               Near Vijaya bank, ESI hospital road<br>
               Near old police station bus stop<br>
               Rajajinagar, Bangalore - 560 010.
            </address>
            <address>
               <strong>Email-id : </strong> 
               <a href="mailto:info@himalaiiasclasses.com">info@himalaiiasclasses.com</a>
            </address>
            <address>
               <strong>Phone : </strong> 
               <a href="tel:+91-9742692750">+91-9742692750</a>, <a href="tel:+91-9740346715">+91-9740346715</a>, <a href="tel:+91-9844609250">+91-9844609250</a>
            </address>
            <address>
               <strong>Time : </strong> 
               <span>11:00 AM &#8212; 8:00 PM on all days</span>
            </address>
         </div>
         <div class="col-md-7">
            <h4>Give wings to your dreams - <strong>Get in touch!</strong></h4><br>
            <div class="form">
               <form name="frmcontactus" id="frmcontactus" action="<?=_URL?>contact" method="POST">
                  <div class="form-group">
                     <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" >
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone No." >
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <input type="email" class="form-control" id="email" name="email" placeholder="Email Id" >
                        </div>
                     </div>
                   </div>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <input type="text" class="form-control" id="qualification" name="qualification" placeholder="Qualification" >
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <select name="interest" class="form-control" id="interest" >
                              <option value="">-Select Interested Exam-</option>
                              <option value="IAS Prelims">IAS Prelims</option>
                              <option value="KAS Prelims">KAS Prelims</option>
                              <option value="IAS Mains">IAS Mains</option>
                              <option value="KAS Mains">KAS Mains</option>
                              <option value="IAS Interview">IAS Interview</option>
                              <option value="KAS Interview">KAS Interview</option>
                              <option value="Others">Others</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <textarea class="form-control" id="message" name="message" rows="3" placeholder="Message"></textarea>
                  </div>
                  <div class="form-group clearfix">
                     <button type="submit" class="btn btn-primary btn-filled btn-sm pull-right">Submit !</button>
                  </div>
               </form>
            </div>
         </div>
      </div>

      <div class="row padded-vertical-40">
         <div class="col-md-5">
               <address>
                  <h5><strong class="red">HIMALAI BRANCH OFFICE - KORAMANGALA</strong></h5>
                  #251/1, KHB Colony,<br>
                  4th Cross, Near Ganapathi Temple,<br>
                  Van Huesen Showroom, 5th block,<br>
                  Koramangala, Bengaluru,<br>
                  Karnataka - 560 096<br><br>
                  <strong>Land Mark:</strong> Besides VLCC Wellness Centre.
               </address>
               <address>
                  <strong>Email-id : </strong> 
                  <a href="mailto:info@himalaiiasclasses.com">info@himalaiiasclasses.com</a>
               </address>
               <address>
                  <strong>Phone : </strong> 
                  <a href="tel:+919008093000">+91 90080 93000</a>, <a href="tel:+917353302603">+91 73533 02603</a>, <a href="tel:+918722248932">+91 87222 48932</a>
               </address>
               <address>
                  <strong>Time : </strong> 
                  <span>11 AM &#8212; 6 PM on all days</span>
               </address>    
         </div>

         <div class="col-md-7">
               <div id="mapCanvas"> </div>
         </div>
      </div>
   </div>

   <div class="container marginbtm50">
      <h3 class="padded-vertical-40 red">WE'RE AT A LOCATION <strong>NEAR YOU</strong></h3>
      <h4><strong>BANGALORE</strong> BRANCHES</h4>
      <span class="sep sep-primary sep-animated">&nbsp;</span>

      <div class="row">
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; RT NAGAR</strong></h5>
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; SAHAKARNAGAR</strong></h5>
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; VIJAYANAGAR</strong></h5>
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; MALLESHWARAM</strong></h5>
            </address>
         </div>
      </div>

      <div class="row padded-vertical-30 marginbtm30">
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; INDIRANAGAR</strong></h5>
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; MARATHAHALLI</strong></h5>
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; JAYANAGAR</strong></h5>
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; KALYAN NAGAR</strong></h5>
            </address>
         </div>
      </div>
      <h4><strong>OUTSTATION</strong> BRANCHES</h4>
      <span class="sep sep-primary sep-animated">&nbsp;</span>

      <div class="row">
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; New Delhi</strong></h5>
            </address>
         </div>
      </div>
   </div>    
</div>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA7IZt-36CgqSGDFK8pChUdQXFyKIhpMBY&sensor=true" type="text/javascript"></script>
<script type="text/javascript">
   var map;
   var geocoder;
   var marker;
   var people = new Array();
   var latlng;
   var infowindow;
   $(document).ready(function() {
      ViewCustInGoogleMap();
   });
   function ViewCustInGoogleMap() {
      var mapOptions = {
         center: new google.maps.LatLng(12.97472,77.582195),
         zoom: 12,
         mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);

      var data = '[{ "DisplayText": "Himalai IAS, RAJAJINAGAR", "ADDRESS": "#458/A27/1, 1st floor, 7th main, 4th Block Near Vijaya bank, ESI hospital road Near old police station bus stop Rajajinagar, Bangalore - 560 010", "LatitudeLongitude": "12.990352,77.5547343", "MarkerId": "Customer" },{ "DisplayText": "Himalai IAS, KORAMANGALA", "ADDRESS": "No. 352, 3rd Floor, 1st B Main,7th Block, Koramangala, Bangalore - 560 095", "LatitudeLongitude": "12.9360551,77.612686", "MarkerId": "Customer"}]';
      people = JSON.parse(data);
      for (var i = 0; i < people.length; i++) {
         setMarker(people[i]);
      }
   }

   function setMarker(people) {
      geocoder = new google.maps.Geocoder();
      infowindow = new google.maps.InfoWindow();
      if ((people["LatitudeLongitude"] == null) || (people["LatitudeLongitude"] == 'null') || (people["LatitudeLongitude"] == '')) {
         geocoder.geocode({ 'address': people["Address"] }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
               latlng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
               marker = new google.maps.Marker({
                  position: latlng,
                  map: map,
                  draggable: false,
                  html: people["DisplayText"]+'<br>'+people["ADDRESS"],
                  icon: "images/marker/" + people["MarkerId"] + ".png"
               });
               google.maps.event.addListener(marker, 'click', function(event) {
                  infowindow.setContent(this.html);
                  infowindow.setPosition(event.latLng);
                  infowindow.open(map, this);
               });
            }
            else {
               alert(people["DisplayText"] + " -- " + people["Address"] + ". This address couldn't be found");
            }
         });
      }
      else {
         var latlngStr = people["LatitudeLongitude"].split(",");
         var lat = parseFloat(latlngStr[0]);
         var lng = parseFloat(latlngStr[1]);
         latlng = new google.maps.LatLng(lat, lng);
         marker = new google.maps.Marker({
            position: latlng,
            map: map,
            draggable: false,
            html: people["DisplayText"]+'<br>'+people["ADDRESS"]
            //icon: "images/marker.png"
         });
         google.maps.event.addListener(marker, 'click', function(event) {
            infowindow.setContent(this.html);
            infowindow.setPosition(event.latLng);
            infowindow.open(map, this);
         });
      }
   }
 </script>