<section class="section bg-black dark padded-vertical-60 bottomQuestion">
    <div class="bg-image parallax bg-question"></div>
    <div class="overlay overlay-black editable-alpha" data-alpha="10"></div>
    <div class="container"> 
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 text-center">
                <span class="icon icon-lg animated bounce-in"><i class="fa fa-download text-primary"></i></span>
                <div class="margin-bottom-20">
                    <h4 class="text-uppercase margin-bottom-0 red bold">Get access to question papers!</h4>
                    <!-- <p class="lead text-muted font-alt">Lorem Ipsum is a dummy text for any dummy content.</p> -->
                </div>
                <form id="sign-in-form">
                    <a href="<?=_URL?>questions" class="btn btn-primary btn-filled btn-lg"><i class="fa fa-download white"></i> &nbsp; Get Questions</a>
                </form>
            </div>
        </div>
    </div>
</section>