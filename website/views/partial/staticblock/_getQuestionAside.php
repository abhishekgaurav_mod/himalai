<div class="sign-in-box margin-bottom-40 animated" data-animation="flipInY" data-animation-delay="200">
	<div class="sign-in-head bg-grey">
		<span class="icon margin-bottom-10 pull-left"><i class="fa fa-download text-primary"></i></span>
		<div class="left-space-md">
			<h5 class="text-uppercase margin-bottom-0"><strong>Get Access</strong> to question papers!</h5>
			<!-- <p class="lead text-muted font-alt margin-bottom-0">Lorem Ipsum is a dummy text for any dummy content.</p> -->
		</div>
	</div>
	<div class="sign-in-content bg-primary dark">
		<div class="form-bottom-bar clearfix">
			<a href="<?=_URL?>questions" class="btn btn-default pull-right"><span class="btn-inner">Get Questions</span></a>
		</div>
	</div>
</div> 