<section class="section bg-black dark padded-vertical-70 bottomQuestion">
    <div class="bg-image parallax bg-question"></div>
    <div class="overlay overlay-black editable-alpha" data-alpha="10"></div>
    <div class="container"> 
        <span class="pull-left icon icon-lg margin-bottom-0 animated" data-animation="bounceIn" data-animation-delay="400"><i class="fa fa-download text-primary">&nbsp;</i> </span>
		<div class="left-space-lg animated" data-animation="fadeInRight" data-animation-delay="400">
			<h4 class="text-uppercase margin-bottom-0 red bold">Get access to question papers!</h4>
			<!-- <p class="lead white font-alt">Lorem Ipsum is a dummy text for any dummy content.</p> -->
			<a class="btn btn-primary btn-filled" href="<?=_URL?>questions">Get Questions</a>
		</div>
    </div>
</section>