<!--    <div class="breadcrumb container1160 innerContainer">
      <div class="flex flexAlignItemsCenter flexWrap">
         <div class="item"><a href="<?=_URL?>">Home</a></div>
         <?php $navLink = _URL;
               foreach ($nav as $slug) {
                  $navLink = $navLink.$slug.'/'; ?>
                  <div class="item">/</div>
                  <div class="item <?=($slug == end($nav))?'active':''?>">
                     <a href="<?=$navLink?>"><?=(ucwords(str_replace('-', ' ', $slug)))?></a>
                  </div>
         <?php } ?>
      </div>
   </div> -->


   <div class="bordered-bottom"> 
      <div class="container">
         <ol class="breadcrumb text-uppercase">
                  <li><a href="<?=_URL?>">Home</a></li>

                  <?php $navLink = _URL;
                        foreach ($nav as $slug) {
                           $navLink = $navLink.$slug.'/'; ?>
                        <li class="<?=($slug == end($nav))?'active':''?>"><a href="<?=$navLink?>"><?=(ucwords(str_replace('-', ' ', $slug)))?></a></li>
                  <?php } ?>

            </ol>
        </div>
    </div>