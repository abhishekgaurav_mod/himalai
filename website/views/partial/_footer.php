
<!-- Footer -->
<footer id="footer" class="bg-black dark">

	<div class="container padded-vertical-40">
		<div class="row">
			<div class="col-md-5 col-sm-6">
				<h5 class="text-light">
					<strong>Indian Civil Service Exam</strong>
				</h5>
				<!-- Navigation list -->
				<ul class="list-navigation margin-bottom-20">
					<li><a href="<?=_URL?>ias-general-instructions">General Instructions</a></li>
					<li><a href="<?=_URL?>ias-exam-eligibility">Eligibility Criteria</a></li>
					<li><a href="<?=_URL?>stages-of-ias-exam">Stages and Syllabus</a></li>
					<li><a href="<?=_URL?>galleries">Gallery</a></li>
					<li><a href="<?=_URL?>contact-us">Contact Us</a></li>
				</ul>
			</div>

			<div class="col-md-5 col-sm-6">
				<h5 class="text-light"> <strong>Contact</strong> Me </h5>
				<!-- List with Icons -->
				<ul class="list-unstyled list-unstyled-icons margin-bototm-20">
					<li><i class="fa fa-map-marker inline-icon red"></i><strong>Address: #458/A27/1, 1st Floor, 7th Main, 4th Block, Near Vijaya Bank, ESI Hospital Road, Near Old Police Station Bus Stop
               Rajajinagar, Bangalore - 560 010.</strong></li>
					<li><i class="fa fa-envelope inline-icon red" aria-hidden="true"></i> <strong>E-mail: <a href="mailto:info@himalaiiasclasses.com">info@himalaiiasclasses.com</a> </strong> </li>
					<li><i class="fa fa-phone inline-icon red"></i><strong>Phone: <a href="tel:+91-9742692750">+91-9742692750</a>, <a href="tel:+91-9740346715">+91-9740346715</a>, <a href="tel:+91-9844609250">+91-9844609250</a></strong></li>
				</ul>
			</div>
			<div class="col-md-2 col-sm-6">
				<h5 class="text-light"> <strong>Social</strong> Media </h5>
				<!-- Social Icons -->
				<div class="margin-bototm-20">
					<a href="#" class="icon icon-circle icon-filled icon-xs icon-facebook"><i class="fa fa-facebook"></i> </a> <a href="#" class="icon icon-circle icon-filled icon-xs icon-twitter"><i class="fa fa-twitter"></i> </a> <a href="#" class="icon icon-circle icon-filled icon-xs icon-google-plus"><i class="fa fa-google-plus"></i> </a>
				</div>
			</div>
		</div>
	</div>

	<!-- Copyright -->
	<div class="copyright bordered-top">
		<div class="container padded-vertical-30">
			<span class="white">&copy; <?=date('Y')?> <strong>Himalai IAS</strong></span>
			<ul class="list-inline pull-right text-muted margin-bottom-0">
				<li><a href="#" class="white">Privacy Policy</a></li>
				<li><a href="#" class="white">Terms and Conditions</a></li>
			</ul>
		</div>
	</div>

</footer>
<!-- Footer / End -->

<!-- Alert Modal -->
<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title text-center" id="alertModalLabel"></h4>
			</div>
			<div class="modal-body text-center">
				<div class="modal-alert-content margin-bottom-20"></div>
				<button type="button" class="btn btn-sm btn-default" data-dismiss="modal"> <i class="icon-before fa fa-times"></i>Close </button>
			</div>
		</div>
	</div>
</div>
<!-- Alert Modal / End -->

<!-- Rules Modal -->
<div class="modal fade" id="rulesModal" tabindex="-1" role="dialog" aria-labelledby="rulesModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="rulesModalLabel">Trener - Sign In Demo - Rules</h4>
			</div>
			<div class="modal-body">
				<ol>
					<li>This form will send you a <strong>demo message</strong> on your e-mail address. </li>
					<li>Your e-mail address <strong>won't be used for any purpose</strong> other than the one specified in the first point. </li>
				</ol>
				<p class="margin-bottom-0"> If you accept those rules please fill the form and press <strong>"Sign In"</strong> button. </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Rules Modal / End -->

<!-- Page Curtain -->
<div id="page-curtain" class="curtain-white"></div>

<?php include _PARTIAL_ROOT.'_footerScript.php'; ?>