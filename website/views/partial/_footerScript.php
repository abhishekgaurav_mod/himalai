<!-- Scripts -->
<script type="text/javascript" src="<?=_JS?>jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?=_JS?>jquery.form.js"></script>
<script type="text/javascript" src="<?=_JS?>notify.min.js"></script>
<script type="text/javascript" src="<?=_JS?>bootstrap.min.js"></script>
<script type="text/javascript" src="<?=_JS?>jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?=_JS?>page-loader.js"></script>
<script type="text/javascript" src="<?=_JS?>jquery.appear.js"></script>
<script type="text/javascript" src="<?=_JS?>jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=_JS?>owl.carousel.min.js"></script>
<script type="text/javascript" src="<?=_JS?>jquery.counterup.min.js"></script>
<script type="text/javascript" src="<?=_JS?>jquery.colorbox-min.js"></script>
<script type="text/javascript" src="<?=_JS?>waypoints.min.js"></script>
<script type="text/javascript" src="<?=_JS?>jquery.localscroll-1.2.7-min.js"></script>
<script type="text/javascript" src="<?=_JS?>jquery.scrollTo-1.4.3.1-min.js"></script>
<script type="text/javascript" src="<?=_JS?>jquery.tweet.js"></script>
<script type="text/javascript" src="<?=_JS?>jquery.parallax.min.js"></script>
<script type="text/javascript" src="<?=_JS?>jquery.fitvids.js"></script>
<script type="text/javascript" src="<?=_PLUGINS?>leanModal/jquery.leanModal.min.js"></script>

<!-- Custom Script -->
<script type="text/javascript" src="<?=_JS?>custom.js"></script>

<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->
<!--
<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
-->
</body>

</html>