<?php
$currentUrl = "http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI];
$uri = str_replace(_URL, '', $currentUrl);
$uri = str_replace('.php', '', $uri);
$uri = trim($uri, '/');
$nav = explode('/', $uri);
?>
<!DOCTYPE html>
<html lang="en">
<head>

<!-- Title -->
<title><?=ucwords(!empty($this->metatitle)?$this->metatitle:_TITLE)?> - Himalai IAS Coaching Centre</title>

<!-- Meta -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no,width=device-width" />

<meta name="keywords" content="<?=(!empty($this->metakeyword)?$this->metakeyword:'')?>">
<meta name="description" content="<?=(!empty($this->metadescription)?$this->metadescription:_TITLE)?>">
<meta name="author" content="<?=((defined('AUTHOR'))?AUTHOR:'Mother Of Design')?>" />
<meta name="copyright" content="<?=((defined('_TITLE'))?_TITLE:'')?>" />

<!--[if (gt IE 8) | (IEMobile)]><!-->
<!--<![endif]-->
<!--[if (lt IE 9) & (!IEMobile)]>
<link rel="stylesheet" href="assets/css/ie.css">
<![endif]-->

<meta property="al:ios:url" content="" />
<meta property="al:ios:app_store_id" content="" />
<meta property="al:ios:app_name" content="" />
<meta property="og:title" content="<?=ucwords(!empty($this->metatitle)?$this->metatitle:_TITLE)?> - Himalai IAS Coaching Centre" />
<meta property="fb:app_id" content=""/>
<meta property="og:type" content="Website" />
<meta property="og:url" content="<?=($currentUrl)?>"/>
<meta property="og:image" content="<?=_IMAGES?>himalai-logo.png"/>
<meta property="og:site_name" content="<?=((defined('_TITLE'))?_TITLE:'')?>"/>
<meta property="og:description" content="<?=(!empty($this->metadescription)?$this->metadescription:_TITLE)?>"/>

<link rel="canonical" href="<?=($currentUrl)?>">

<!-- Favicons -->
<link rel="shortcut icon" href="<?=_IMAGES?>favicon.png">
<link rel="apple-touch-icon" href="<?=_IMAGES?>favicon_60x60.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?=_IMAGES?>favicon_76x76.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?=_IMAGES?>favicon_120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?=_IMAGES?>favicon_152x152.png">

<!-- Google Web Fonts -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Noto+Serif:400i" rel="stylesheet">

<!-- Stylesheets -->
<link rel="stylesheet" type="text/css" href="<?=_CSS?>bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?=_CSS?>style.css" />
<link rel="stylesheet" type="text/css" href="<?=_CSS?>animations.css" />
<link rel="stylesheet" type="text/css" href="<?=_CSS?>owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="<?=_CSS?>owl.transitions.css" />
<link rel="stylesheet" type="text/css" href="<?=_CSS?>owl.theme.css" />
<link rel="stylesheet" type="text/css" href="<?=_CSS?>colorbox.css" />
<link rel="stylesheet" type="text/css" href="<?=_CSS?>font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?=_CSS?>red.css" />
<link rel="stylesheet" type="text/css" href="<?=_PLUGINS?>leanModal/leanModal.css" />
<link rel="stylesheet" type="text/css" href="<?=_CSS?>jquery.bxslider.css" />
<link rel="stylesheet" type="text/css" href="<?=_CSS?>notify.css" />
<link rel="stylesheet" type="text/css" href="<?=_CSS?>custom-style.css" />

<script type="text/javascript" src="<?=_JS?>jquery-1.11.1.min.js"></script>

</head>
<body class="sticky-nav">