<?php include _PARTIAL_ROOT.'_metaHeader.php'; ?>

<!-- Loader -->
<div id="page-loader" class="loader-white"></div>
<!-- Header -->
<header id="header" class="header header-white wide clearfix">

    <div class="row">
        <div class="col-xs-12">
            <div class="nav-bar clearfix">
                <!-- Logo -->
                <div class="logo pull-left">
                    <a href="<?=_URL?>"><img src="<?=_IMAGES?>himalai-logo.svg" alt="Himalai" /></a>
                </div>
                <!-- Mobile Nav Toggle -->
                <a href="" class="mobile-menu-toggle pull-right"></a>
                <!-- Social Icons -->
                <!-- <div class="social-icons text-muted pull-right margin-left-20 hidden-sm hidden-xs">
                   <a href="#" class="icon icon-xs icon-facebook"><i class="fa fa-facebook"></i></a>
           		   <a href="#" class="icon icon-xs icon-twitter"><i class="fa fa-twitter"></i></a>
          		   <a href="#" class="icon icon-xs icon-google-plus"><i class="fa fa-google-plus"></i></a> 
                </div>  -->
                <!-- Main Navigation-->
                <ul class="menu nav nav-main text-uppercase pull-right margin-left-20 dropdown-dark">
                    <li><a href="<?=_URL?>exam-trends">Exam Trends</a></li>
                    <li><a href="<?=_URL?>why-himalai">Why Himalai</a></li>
                    <li><a href="<?=_URL?>about-us">About Us</a></li>
                    <li class="has-dropdown">
                        <a href="#">Indian Civil Service Exam</a>
                        <div class="nav-main-dropdown">
                            <ul>
                                <li><a href="<?=_URL?>indian-civil-service-exams"><i class="icon-before fa fa-bolt text-muted"></i>Overview</a></li>
                                <li class="has-dropdown">
                                    <a href="#"><i class="icon-before fa fa-star text-muted"></i>General Instructions</a>
                                    <ul>
                                        <li><a href="<?=_URL?>ias-general-instructions"><i class="icon-before fa fa-bolt text-muted"></i>General Instructions</a></li>
                                        <li><a href="<?=_URL?>ias-exam-eligibility"><i class="icon-before fa fa-bolt text-muted"></i>Eligibility Criteria</a></li>
                                        <li><a href="<?=_URL?>ias-service-allocation"><i class="icon-before fa fa-bolt text-muted"></i>Service Allocation</a></li>
                                        <li><a href="<?=_URL?>ias-reservation-issues"><i class="icon-before fa fa-bolt text-muted"></i>Reservation Issues</a></li>
                                        <li><a href="<?=_URL?>ias-ph-category"><i class="icon-before fa fa-bolt text-muted"></i>PH Category</a></li>
                                        <li><a href="<?=_URL?>ias-medical-exam-of-candidates"><i class="icon-before fa fa-bolt text-muted"></i>Medical Test</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?=_URL?>stages-of-ias-exam"><i class="icon-before fa fa-bolt text-muted"></i>Stages and Syllabus</a></li>
                                <li><a href="<?=_URL?>general-studies-mains"><i class="icon-before fa fa-bolt text-muted"></i>Syllabus for Mains</a></li>
                                <li><a href="<?=_URL?>general-studies-optional"><i class="icon-before fa fa-bolt text-muted"></i>List Of Optional Subject</a></li>
                                <li><a href="<?=_URL?>questions"><i class="icon-before fa fa-bolt text-muted"></i>Question Papers</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="has-dropdown">
                        <a href="#">KAS Coaching</a>
                        <div class="nav-main-dropdown">
                            <ul>
                                <li><a href="<?=_URL?>kas-coaching"><i class="icon-before fa fa-bolt text-muted"></i>KAS Coaching</a></li>
                                <li><a href="<?=_URL?>kas-exam"><i class="icon-before fa fa-bolt text-muted"></i>KAS Exam</a></li>
                                <li><a href="<?=_URL?>kas-eligibility"><i class="icon-before fa fa-bolt text-muted"></i>KAS ELIGIBILITY</a></li>
                                <li><a href="<?=_URL?>stages-of-kas-exam"><i class="icon-before fa fa-bolt text-muted"></i>Stages of KAS Exam</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="has-dropdown">
                        <a href="#">Topper's Testimonial</a>
                        <div class="nav-main-dropdown">
                            <ul>
                                <li><a href="<?=_URL?>testimonials"><i class="icon-before fa fa-bolt text-muted"></i>Cleared Candidates</a></li>
                                <li><a href="<?=_URL?>student-testimonials"><i class="icon-before fa fa-bolt text-muted"></i>Student Testimonials</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="<?=_URL?>galleries">Gallery</a></li>
                    <li><a href="<?=_URL?>contact-us">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="mobile-menu text-uppercase"></div>

</header>
<a href="<?=_URL?>contact-us" class="floatingEnquiry"> Register / Enquiry </a>
<!-- Header / End -->