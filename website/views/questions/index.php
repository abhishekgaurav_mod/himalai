<div id="page-title">

   <div class="title-bar dark bg-black padded-vertical-70 pageHeadingBar">
        <div class="bg-image bg-image-3"></div>
        <div class="overlay overlay-black editable-alpha" data-alpha="80"></div> 
        <div class="container text-center">  
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <span class="icon icon-lg icon-circle icon-primary margin-bottom-20 animated" data-animation="bounceIn"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span>
                    <h1 class="text-uppercase margin-bottom-0 animated" data-animation="flipInX" data-animation-delay="200"><strong>Question Papers</strong></h1>
                    <span class="sep sep-primary"></span>
                    <div class="font-alt pageSubHeading margin-bottom-20">Dream, Dream Dream, Dreams transform into thoughts. And thoughts result in action. - A.P.J. Abdul Kalam </div>
                </div>
            </div>
        </div>
    </div>

   <?php  include _PARTIAL_ROOT.'_breadcrumb.php'; ?>

</div>

<div id="content">
   <div class="container">
        <div class="row padded-top-40">
			<div class="col-md-3">

				<?php if(!empty($arr_prelimsYear)){ ?>
					<h5 class="red margin-bottom-10">Civil service examination &#8212; Prelims</h5>
					<ul class="nav nav-sidebar margin-bottom-40">
						<?php foreach ($arr_prelimsYear as $key => $prelimsYear) { ?>
							<li class="<?=($key=='0')?'active':''?>">
								<a href="#prelimstab<?=$key?>" data-toggle="tab"><i class="icon-before fa fa-user fa-fw text-muted-2x"></i>Question Paper <?=$prelimsYear->question_year?></a>
							</li>
						<?php } ?>
					</ul>
				<?php } ?>

				<?php if(!empty($arr_mainsYear)){ ?>
					<h5 class="red margin-bottom-10">Civil service examination &#8212; Mains</h5>
					<ul class="nav nav-sidebar margin-bottom-40">
						<?php foreach ($arr_mainsYear as $key => $mainsYear) { ?>
							<li>
								<a href="#mainstab<?=$key?>" data-toggle="tab"><i class="icon-before fa fa-user fa-fw text-muted-2x"></i>Question Paper <?=$mainsYear->question_year?></a>
							</li>
						<?php } ?>
					</ul>
				<?php } ?>

			</div>

			<div class="col-md-9">               
				<div class="tab-content margin-bottom-40 questionPapers">

					<?php if(!empty($arr_prelimsYear)){
						foreach ($arr_prelimsYear as $key => $prelimsYear) { ?>
							<div id="prelimstab<?=$key?>" class="tab-pane fade <?=($key=='0')?'in':''?> <?=($key=='0')?'active':''?>">
								<?php if (!empty($prelimsYear->prelimsSubjects)){
									foreach ($prelimsYear->prelimsQuestionType as $prelimsQuestionType) {
										if (!empty($prelimsQuestionType->question_type)) { ?>
											<?php if (!empty($prelimsQuestionType->question_type) && $prelimsQuestionType->question_type != 'GS') { ?>
												<h5 class="red">
													<?php
														if ($prelimsQuestionType->question_type == 'POS') { echo 'Optional Subjects for Prelims Examination'; }
														else if ($prelimsQuestionType->question_type == 'MIL') { echo 'Indian Languages (Compulsory) for Main Examination'; }
														else if ($prelimsQuestionType->question_type == 'MOS') { echo 'Optional Subjects for Main Examination'; }
														else if ($prelimsQuestionType->question_type == 'MLS') { echo 'Literature Subjects for Main Examination'; }
													?>
												</h5>
											<?php } ?>
											<div class="row">
												<?php if (!empty($prelimsQuestionType->prelimsSubjectByType)){
													foreach ($prelimsQuestionType->prelimsSubjectByType as $prelimsSubjectByType) { ?>
														<a href="<?=_QUESTION_ORG_URL.$prelimsSubjectByType->question_paper?>" target="_blank" class="gallery-item col-lg-2 col-md-2 col-sm-2 col-xs-4">
															<div class="gallery-item-image">
																<?php if (!empty($prelimsSubjectByType->thumb)) { ?>
																<img src="<?=_QUESTION_THUMB_ORG_URL?><?=$prelimsSubjectByType->thumb?>" alt="<?=$prelimsSubjectByType->questionsubject_name?>" />
																<?php } else { ?>
																<i class="fa fa-file-pdf-o pdfPlaceholder" aria-hidden="true"></i>
																<?php } ?>
																<div class="gallery-item-overlay overlay-black"></div>
															</div>
															<div class="gallery-item-title text-center">
																<h6 class="margin-bottom-0"><?=$prelimsSubjectByType->questionsubject_name?></h6>
																<span class="text-uppercase text-muted text-xs">Year <?=$prelimsYear->question_year?></span>
															</div>
														</a>
													<?php }
												} ?>
											</div>
										<?php }
									} ?>

								<?php } else { ?>
									<h3>No Questions Found !!</h3>
								<?php } ?>
							</div>
						<?php }
					} ?>

					<?php if(!empty($arr_mainsYear)){
						foreach ($arr_mainsYear as $key => $mainsYear) { ?>
							<div id="mainstab<?=$key?>" class="tab-pane fade">
								<?php if (!empty($mainsYear->mainsSubjects)){
									foreach ($mainsYear->mainsQuestionType as $mainsQuestionType) {
										if (!empty($mainsQuestionType->question_type)) { ?>
											<?php if (!empty($mainsQuestionType->question_type) && $mainsQuestionType->question_type != 'GS') { ?>
												<h5 class="red">
													<?php
														if ($mainsQuestionType->question_type == 'POS') { echo 'Optional Subjects for Prelims Examination'; }
														else if ($mainsQuestionType->question_type == 'MIL') { echo 'Indian Languages (Compulsory) for Main Examination'; }
														else if ($mainsQuestionType->question_type == 'MOS') { echo 'Optional Subjects for Main Examination'; }
														else if ($mainsQuestionType->question_type == 'MLS') { echo 'Mains Literature Subjects for Main Examination'; }
													?>
												</h5>
											<?php } ?>
											<div class="row">
												<?php if (!empty($mainsQuestionType->mainsSubjectByType)){
													foreach ($mainsQuestionType->mainsSubjectByType as $mainsSubjectByType) { ?>
														<a href="<?=_QUESTION_ORG_URL.$mainsSubjectByType->question_paper?>" target="_blank" class="gallery-item col-lg-2 col-md-2 col-sm-2 col-xs-4">
															<div class="gallery-item-image">
																<?php if (!empty($mainsSubjectByType->thumb)) { ?>
																<img src="<?=_QUESTION_THUMB_ORG_URL?><?=$mainsSubjectByType->thumb?>" alt="<?=$mainsSubjectByType->questionsubject_name?>" />
																<?php } else { ?>
																<i class="fa fa-file-pdf-o pdfPlaceholder" aria-hidden="true"></i>
																<?php } ?>
																<div class="gallery-item-overlay overlay-black"></div>
															</div>
															<div class="gallery-item-title text-center">
																<h6 class="margin-bottom-0"><?=$mainsSubjectByType->questionsubject_name?></h6>
																<span class="text-uppercase text-muted text-xs">Year <?=$mainsYear->question_year?></span>
															</div>
														</a>
													<?php }
												} ?>
											</div>
										<?php }
									} ?>

								<?php } else { ?>
									<h3>No Questions Found !!</h3>
								<?php } ?>
							</div>
						<?php }
					} ?>

				</div>
			</div>
       	</div>
    </div>

</div>
