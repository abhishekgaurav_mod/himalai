<div id="page-title">

	<div class="title-bar dark bg-black padded-vertical-60 pageHeadingBar">
        <div class="bg-image bg-image-3"></div>
        <div class="overlay overlay-black editable-alpha" data-alpha="70"></div> 
        <div class="container">	
            <span class="sep sep-primary sep-no-top"></span>
            <h2 class="text-uppercase margin-bottom-0"><strong>Galleries</strong></h2>
            <span class="text-xs pageSubHeading text-uppercase">Anyone who says they are not interested in politics is like a drowning man who insists he is not interested in water. - Mahatma Gandhi</span>
        </div>
    </div>

   <?php  include _PARTIAL_ROOT.'_breadcrumb.php'; ?>

</div>

<div id="content" class="galleryPage">
    <div class="container">
        <h2>Video</h2>
        <div class="row padded-top-40 videoGallery flexResponsive flexJustifyBetween flexWrap">
            <div class="item flex1"><iframe width="360" height="215" src="https://www.youtube.com/embed/y1mliJbrsZc" frameborder="0" allowfullscreen></iframe></div>
            <div class="item flex1"><iframe width="360" height="215" src="https://www.youtube.com/embed/P4AeHv4ZPk0" frameborder="0" allowfullscreen></iframe></div>
            <div class="item flex1"><iframe width="360" height="215" src="https://www.youtube.com/embed/9U4J2N-FOVs" frameborder="0" allowfullscreen></iframe></div>
        </div>
        <div class="row padded-top-40">
            <a href="https://www.youtube.com/channel/UCi3LsSOL89VrA6zhPlX-R-Q" class="btn btn-primary btn-filled btn-sm pull-right" target="_blank">Subscribe To Our Channel !</a>
        </div>
    </div> 

	<div class="container">
        <h2>Gallery</h2>
    	<div class="padded-top-40">
    		<?php if (!empty($arr_gallery)) { ?>
					<ul class="gallery">
		          	<?php foreach ($arr_gallery as $key => $image) { ?>
							<li class="item"><img src="<?=_GALLERY_ORG_URL.$image->imagepath?>" alt="<?=$image->title?>" class="img-responsive" /></li>
            		<?php } ?>
					</ul>
					<div id="galleryPager">
			          	<?php foreach ($arr_gallery as $key => $image) { ?>
							<a data-slide-index="<?=$key?>" href=""><img src="<?=_GALLERY_ORG_URL.$image->imagepath?>" alt="thumb-<?=$image->title?>" /></a>
	            		<?php } ?>
					</div>
            <?php 	} ?>
        </div>
    </div>  

    <?php  include _STATIC_BLOCK.'_getQuestionCenter.php'; ?>

</div>
