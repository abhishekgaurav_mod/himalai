<div id="page-title">

    <div class="title-bar dark bg-black padded-vertical-60 pageHeadingBar">
        <div class="bg-image bg-image-3"></div>
        <div class="overlay overlay-black editable-alpha" data-alpha="70"></div> 
        <div class="container"> 
            <span class="sep sep-primary sep-no-top"></span>
            <h2 class="text-uppercase margin-bottom-0">Our <strong>Cleared Candidate Testimonials</strong></h2>
            <span class="text-xs pageSubHeading text-uppercase">You should never let your fears prevent you from doing what is right. - Aung San Suu Kyi</span>
        </div>
    </div>

    <?php  include _PARTIAL_ROOT.'_breadcrumb.php'; ?>

</div>

<div id="content">

    <div class="container">
        <section class="section paddingtop50">
            <div class="section-title">
                <h4 class="text-uppercase margin-bottom-5"><strong>4 Achievements</strong> in the Top 14, <strong>900+ selections</strong> in Elite Services </h4>
                <span class="sep sep-primary sep-animated"></span>
            </div>
        </section>
        <div class="row padded-top-40 margin-bottom-20">

            <?php   if (!empty($arr_testimonial)) {
                        foreach ($arr_testimonial as $key => $testimonial) {
                            $testimonialUrl    =  _URL.'testimonials/'.$testimonial->slug;
                            ?>
                            <div class="col-md-4">                              
                                <div class="testimonial">
                                    <div class="testimonial-content <?=($key % 2 == 0)?'bg-grey':'bg-black dark'?> font-alt">
                                        <?=((strlen($testimonial->description)>140)?substr(strip_tags($testimonial->description), 0,140).'. . .':strip_tags($testimonial->description))?>
                                    </div>
                                    <a href="<?=$testimonialUrl?>">
                                        <div class="testimonial-author testimonial-with-photo">
                                            <div class="testimonial-photo">
                                                <?php if (!empty($testimonial->authorimagepath)) { ?>
                                                    <img class="img-circle" src="<?=_TESTIMONIAL_AUTHOR_ORG_URL.$testimonial->authorimagepath?>" alt="<?=$testimonial->name?>" />
                                                <?php } else { ?>
                                                    <img class="img-circle" src="<?=_IMAGES?>author.jpg" alt="<?=$testimonial->name?>" />
                                                <?php } ?>
                                            </div>
                                           
                                            <div class="testimonial-author-txt text-uppercase">
                                                <span class="author-name"><?=$testimonial->name?></span>
                                                <span class="text-muted text-xs"><?=(!empty($testimonial->tags)?$testimonial->tags:'')?></span>
                                                <span class="text-muted readMore">Click here for original testimonial</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
            <?php       }
                    } ?>
        </div>
    </div>

    <?php  include _STATIC_BLOCK.'_getQuestion.php'; ?>
        
</div>
