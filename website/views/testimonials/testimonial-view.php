<div id="page-title">

    <div class="title-bar dark bg-black padded-vertical-60 pageHeadingBar">
        <div class="bg-image bg-image-3"></div>
        <div class="overlay overlay-black editable-alpha" data-alpha="70"></div> 
        <div class="container"> 
            <span class="sep sep-primary sep-no-top"></span>
            <h2 class="text-uppercase margin-bottom-0">Our <strong>Testimonial</strong></h2>
            <!-- <span class="text-xs pageSubHeading text-uppercase"><?=$cmsContent->subtitle?></span> -->
        </div>
    </div>

   <?php  include _PARTIAL_ROOT.'_breadcrumb.php'; ?>

</div>

<div id="content">
    <?php if (!empty($object)): ?>
        <div class="container">
            <div class="row padded-top-40">

                <div class="col-md-8">
                    <article class="post">
                        <h2 class="post-title"> <?=$object->name?> </h2>
                        <div class="post-meta">
                            <?=(!empty($object->tags)?'<span><i class="fa fa-bookmark" aria-hidden="true"></i>'.$object->tags.'</span>':'')?>
                            <span><i class="fa fa-clock-o"></i> <?=date("d-m-Y", strtotime($object->publishing_date))?> </span>
                        </div>
                        <div>
                            <?php if (!empty($object->authorimagepath)) { ?>
                                <img src="<?=_TESTIMONIAL_AUTHOR_ORG_URL.$object->authorimagepath?>" class="img-rounded margin-right-20 margin-bottom-20 pull-left" alt="<?=$object->name?>" />
                            <?php } else { ?>
                                <img src="<?=_IMAGES?>author.jpg" class="img-rounded margin-right-20 margin-bottom-20 pull-left" alt="<?=$object->name?>" />
                            <?php } ?>

                            <?php if (!empty($object->testimonialimagepath)) { ?>
                            <img src="<?=_TESTIMONIAL_ORG_URL.$object->testimonialimagepath?>" class="img-rounded margin-right-20 margin-bottom-20 pull-left" alt="<?=$object->name?>" />
                            <?php } ?>
                        </div>
                        <div class="clear"></div>
                        <div class="testimonial margin-bottom-30">
                            <div class="testimonial-content bg-grey font-alt">
                                <?=$object->description?>
                            </div>
                        </div>
                    </article>
                </div>
                
                <div class="col-md-4">
                    <?php  include _STATIC_BLOCK.'_getQuestionAside.php'; ?>
                </div>

            </div>
        </div>
    <?php endif; ?>
</div>





