
<!-- Title Bar -->
<div id="page-title">

   <div class="title-bar dark bg-black padded-vertical-60 pageHeadingBar">
        <div class="bg-image bg-image-3"></div>
        <div class="overlay overlay-black editable-alpha" data-alpha="70"></div> 
        <div class="container">  
            <span class="sep sep-primary sep-no-top"></span>
            <h2 class="text-uppercase margin-bottom-0">Our<strong> Branches</strong></h2>
            <span class="text-xs pageSubHeading text-uppercase">A great man is different from an eminent one in that he is ready to be the servant of the society. - B.R.Ambedkar</span>
        </div>
    </div>

   <?php  include _PARTIAL_ROOT.'_breadcrumb.php'; ?>

</div>
<!-- Title Bar / End -->

<!-- Content -->
<div id="content">

   <div class="container marginbtm40">
      <h3 class="padded-vertical-40 red"><strong>WE'RE AT A LOCATION NEAR YOU</strong></h3>
      <h4 class="red"><strong>BANGALORE</strong> BRANCHES</h4>
      <span class="sep sep-primary sep-animated">&nbsp;</span>

      <div class="row">
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; RT NAGAR</strong></h5>
               No 39/47, Kamala Tower,<br>
               5th Main Road, Ganganagar Main Road,<br>
               Bangalore - 560 032<br><br>
               <strong>Land Mark:</strong> Above Bata Showroom, Next to CBI & Madura Darshini Hotel.
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; SAHAKARNAGAR</strong></h5>
               No. 433, Cellar Floor,<br>
               Sri Shirdi Sai Baba Nilayam,<br>
               11th Cross, Next road to McDonalds,<br>
               17th Main Road, F Block,<br>
               Bangalore - 560 092<br><br>
               <strong>Land Mark:</strong> Opp Axis Bank Road.
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; VIJAYANAGAR</strong></h5>
               No. 48E, 3rd Floor,<br>
               15th Main, East of Chord,<br>
               Bangalore - 560 040<br><br>
               <strong>Land Mark:</strong> Near Vijayanagar Police Station, Kodandarama Temple, Service Road, Above Anand Medicals.
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; MALLESHWARAM</strong></h5>
               Near 18th Cross,<br>
               Sampige Road,<br>
               Bangalore - 560 003<br><br>
               <strong>Land Mark:</strong> Near CEG Cell Above Eye Life Optical Building, Beside Kavya Xerox, Sangeetha Mobiles.
            </address>
         </div>
      </div>

      <div class="row padded-vertical-30">
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; INDIRANAGAR</strong></h5>
               No 5/1, 2nd Floor,<br>
               Krishna Temple Street, 1st Stage,<br>
               Bangalore - 560 038<br><br>
               <strong>Land Mark:</strong> Above Bata Showroom, Next to CBI & Madura Darshini Hotel.
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; MARATHAHALLI</strong></h5>
               Ramanjaneya Complex,<br>
               2nd Floor, Opp. Thulsi Theatre,<br>
               Beside Brand Factory,<br>
               Bangalore - 560 037<br><br>
               <strong>Land Mark:</strong> Opp. Thulsi Theatre.
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; JAYANAGAR</strong></h5>
               No 14/A, 27th Cross,<br>
               3rd Floor, 4th Block,<br>
               Bangalore - 560 037<br><br>
               <strong>Land Mark:</strong> Beside Sapna Book House, Beside Nandini Hotel, Above Nandhana Shelters.
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; KALYAN NAGAR</strong></h5>
               No. 416, 4th Main,<br>
               HRBR 2nd Block, 4th Floor,<br>
               Bangalore - 560 043<br><br>
               <strong>Land Mark:</strong> Behind Snap Fitness, Kammanahalli Entrance (Road), Opp. ICICI Bank Above Vision 360.
            </address>
         </div>
      </div>


      <h4 class="red"><strong>OUTSTATION</strong> BRANCHES</h4>
      <span class="sep sep-primary sep-animated">&nbsp;</span>

      <div class="row">
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; MYSORE</strong></h5>
               #423, 3rd Floor,<br>
               Above Muthoot Finance,<br>
               Chamaraja Double Road,<br>
               Near Ramaswamy Circle,<br>
               Mysore.<br><br>
               <strong>Land Mark:</strong> Above Muthoot Finance, Near Krishna Bakery.
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; MANGALORE</strong></h5>
               Ram Bhavan Complex,<br>
               Door No. 13-12-1394/66 (4),<br>
               5th Floor, B-wing,<br>
               PVR Circle, Kodialbail,<br>
               Mangalore - 575 003<br><br>
               <strong>Land Mark:</strong> Opp. to Bharath Petroleum.
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; HUBLI</strong></h5>
               1st Floor, Maniyar Building,<br>
               Old Income Tax Office Road,<br>
               Vidyanagar, Hubli - 580 021<br><br>
               <strong>Land Mark:</strong> Near Old Income Tax Office, Opp. to Gurudatt Bhavan, Next to Canara Bank Lane.
            </address>
         </div>
         <div class="col-md-3">
            <address>
               <h5><strong class=""><i class="fa fa-map-marker red" aria-hidden="true"></i> &nbsp; BELGAUM</strong></h5>
               #10432, 3rd Floor,<br>
               Mujawar Arcade, Nehru Nagar,<br>
               Mujawar Arcade Road,<br>
               Belgaum - 590 010<br><br>
               <strong>Land Mark:</strong> Pune Bangalore (PB) Road, Mujawar Arcade.
            </address>
         </div>
      </div>

   </div>

   <!-- Google Map -->
   <!-- <div id="google-map-dark" class="google-map"></div> -->
    
</div>
<!-- Content / End -->
