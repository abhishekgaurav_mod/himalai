<div id="page-title">

	<div class="title-bar dark bg-black padded-vertical-60 pageHeadingBar">
        <div class="bg-image bg-image-3"></div>
        <div class="overlay overlay-black editable-alpha" data-alpha="70"></div> 
        <div class="container">	
            <span class="sep sep-primary sep-no-top"></span>
            <h2 class="text-uppercase margin-bottom-0"><strong><?=$cmsContent->title?></strong></h2>
            <span class="text-xs pageSubHeading text-uppercase"><?=$cmsContent->subtitle?></span>
        </div>
    </div>

   <?php  include _PARTIAL_ROOT.'_breadcrumb.php'; ?>

</div>

<div id="content">

	<div class="container">
    	<div class="row padded-top-40">
	        <div class="col-md-12">
	        	<div class="cms pageContent">
					<?php
						if (!empty($cmsContent)) {
							$page_body = str_replace('{_IMAGES}', _IMAGES, $cmsContent->description);
							$page_body = str_replace('{_URL}', _URL, $page_body);
							$page_body = str_replace('{_MEDIA_URL}', _MEDIA_URL, $page_body);

							echo $page_body;
						}
					?>
				</div>
	        </div>
        </div>
    </div>

    <?php  include _STATIC_BLOCK.'_getQuestionCenter.php'; ?>
    
</div>