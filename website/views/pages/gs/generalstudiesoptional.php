<div id="page-title">

	<div class="title-bar dark bg-black padded-vertical-60 pageHeadingBar">
        <div class="bg-image bg-image-3"></div>
        <div class="overlay overlay-black editable-alpha" data-alpha="70"></div> 
        <div class="container">	
            <span class="sep sep-primary sep-no-top"></span>
            <h2 class="text-uppercase margin-bottom-0"><strong>List Of Optional Subjects</strong></h2>
            <span class="text-xs pageSubHeading text-uppercase">Every citizen of India must remember that he is an Indian and he has every right in this country but with certain duties<br>- Sardar Patel</span>
        </div>
    </div>

   <?php  include _PARTIAL_ROOT.'_breadcrumb.php'; ?>

</div>

<div id="content">
	<div class="container">
    	<div class="row padded-top-40">
    		<?php if (!empty($arr_subjects)) {
		          	foreach ($arr_subjects as $key => $subject) { ?>

		          		<a href="#description<?=$subject->gsoptionalsubject_id?>" rel="leanModal" class="gallery-item col-md-4 col-sm-6">
				            <div class="gallery-item-image">
				                <img src="<?=_GS_OPTIONALSUBJECT_THUMB_ORG_URL.$subject->thumb?>" alt="" />
				                <div class="gallery-item-overlay overlay-black"></div>
				            </div>
				            <div class="gallery-item-title text-center">
				                <h6 class="margin-bottom-0"><?=$subject->name?></h6>
				            </div>
				        </a>
				        <div id="description<?=$subject->gsoptionalsubject_id?>" class="contentPopup leanModal displaynone">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close popupClose">X</button>
									<h4 class="modal-title" id="myModalLabel"><?=$subject->name?></h4>
								</div>
								<div class="modal-body pageContent">
									<?=$subject->description?>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary popupClose">Close</button>
								</div>
							</div>
				        </div>
            <?php 	}
            	} ?>
        </div>
    </div>  

    <?php  include _STATIC_BLOCK.'_getQuestionCenter.php'; ?>

</div>
