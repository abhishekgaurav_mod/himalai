<div id="page-title">

	<div class="title-bar dark bg-black padded-vertical-60 pageHeadingBar">
        <div class="bg-image bg-image-3"></div>
        <div class="overlay overlay-black editable-alpha" data-alpha="70"></div> 
        <div class="container">	
            <span class="sep sep-primary sep-no-top"></span>
            <h2 class="text-uppercase margin-bottom-0"><strong>Indian civil service exam</strong></h2>
            <span class="text-xs pageSubHeading text-uppercase">A great man is different from an eminent one in that he is ready to be the servant of the society. - B.R.Ambedkar</span>
        </div>
    </div>

   <?php  include _PARTIAL_ROOT.'_breadcrumb.php'; ?>

</div>

<div id="content">

	<div class="container">
    	<div class="row padded-top-40">
	        <div class="col-md-12">
	          <!-- Accordion -->
	          	<div class="panel-group margin-bottom-40" id="accordion">

	          	<?php if (!empty($arr_profiles)) {
		          		foreach ($arr_profiles as $key => $profile) { ?>
				            <div class="expandAll panel panel-default">
				              <div class="panel-heading" data-toggleid="toggleid<?=$key?>">
				                <h5 class="panel-title">
				                    <?=$profile->name?>
				                </h5>
				              </div>
				              <div id="toggleid<?=$key?>" class="expandAllContent">
				                <div class="panel-body pageContent">
				                	<?=$profile->description?>
				                </div>
				              </div>
				            </div>
	            <?php 	}
	            	} ?>

	          </div>
	        </div>
        </div>
    </div>

    <?php  include _STATIC_BLOCK.'_getQuestionCenter.php'; ?>
    
</div>