<!DOCTYPE html>
<html lang="en">
<head>

<!-- Meta -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<!-- Title -->
<title>Trener - Responsive HTML/CSS Template</title>

<!-- Favicons -->
<link rel="shortcut icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/favicon_60x60.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/favicon_76x76.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/favicon_120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/favicon_152x152.png">

<!-- Google Web Fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

<!-- Stylesheets -->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/animations.css" />
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="css/owl.transitions.css" />
<link rel="stylesheet" type="text/css" href="css/owl.theme.css" />
<link rel="stylesheet" type="text/css" href="colors/red.css" id="colors" />
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />

</head>

<body class="sticky-nav">

<!-- Loader -->
<div id="page-loader" class="loader-white"></div>
<!-- Loader / End -->

<!-- Header -->
<header id="header" class="header header-white clearfix">

	<div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="top-bar bordered-bottom clearfix hidden-sm hidden-xs">
                	<span class="text-xs text-uppercase"><strong>Welcome</strong> on my website!</span>
                    <div class="pull-right">
                        <a href="#" class="btn btn-default btn-xs"><i class="icon-before fa fa-comment"></i>Contact Me</a> <a href="#" class="btn btn-primary btn-filled btn-xs"><i class="icon-before fa fa-download"></i>Get free files!</a>
                    </div>
                </div>
                <div class="nav-bar clearfix">
                    <!-- Logo -->
                    <div class="logo pull-left">
                        <a href="index.html"><img src="images/logo-black.png" alt="Trener" /></a>
                    </div>
                    <!-- Mobile Nav Toggle -->
                    <a href="" class="mobile-menu-toggle pull-right"></a>
                    <!-- Social Icons -->
                    <div class="social-icons text-muted pull-right margin-left-20 hidden-sm hidden-xs">
                       <a href="#" class="icon icon-xs icon-facebook"><i class="fa fa-facebook"></i></a>
                       <a href="#" class="icon icon-xs icon-twitter"><i class="fa fa-twitter"></i></a>
                       <a href="#" class="icon icon-xs icon-google-plus"><i class="fa fa-google-plus"></i></a> 
                    </div> 
                    <!-- Main Navigation-->
                    <ul class="menu nav nav-main pull-right margin-left-20 dropdown-dark">
                        <li class="has-dropdown">
                            <a href="#">Home</a>
                            <div class="nav-main-dropdown">
                                <ul>
                                    <li><a href="index.html">Trainer</a></li>
                                    <li><a href="index-marketer.html">Marketer</a></li>
                                    <li><a href="index-professional.html">Professional</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="has-dropdown">
                            <a href="#">Features</a>
                            <div class="nav-main-dropdown">
                                <ul>
                                    <li class="has-dropdown">
                                        <a href="#"><i class="icon-before fa fa-star text-muted"></i>Headers</a>
                                        <ul>
                                            <li><a href="headers-light.html">Headers Light</a></li>    
                                            <li><a href="headers-dark.html">Headers Dark</a></li>   
                                            <li><a href="headers-light-wide.html">Headers Light &amp; Wide</a></li>    
                                            <li><a href="headers-dark-wide.html">Headers Dark &amp; Wide</a></li>     
                                        </ul>
                                    </li>
                                    <li class="has-dropdown">
                                        <a href="#"><i class="icon-before fa fa-desktop text-muted"></i>Sliders</a>
                                        <ul>
                                            <li><a href="slider-default.html">Default Slider</a></li>    
                                            <li><a href="slider-text.html">Text Slider</a></li>      
                                            <li><a href="slider-photo.html">Photo Slider</a></li>
                                            <li class="has-dropdown">
                                                <a href="#">Slider Sizes</a>
                                                <ul>
                                                    <li><a href="slider-size-default.html">Deafult</a></li>
                                                    <li><a href="slider-size-small.html">Small</a></li>
                                                    <li><a href="slider-size-fullheight.html">Fullheight</a></li>
                                                </ul>
                                            </li>  
                                        </ul>
                                    </li>
                                    <li class="has-dropdown">
                                        <a href="#"><i class="icon-before fa fa-road text-muted"></i>Footers</a>
                                        <ul>
                                            <li><a href="footers-light.html">Footers Light</a></li>    
                                            <li><a href="footers-dark.html">Footers Dark</a></li>      
                                        </ul>
                                    </li>
                                    <li><a href="animations.html"><i class="icon-before fa fa-bolt text-muted"></i>Animations</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="has-dropdown-wide">
                             <a href="#">Templates</a>
                             <div class="nav-main-dropdown">
                                <div class="row">
                                    <div class="col-md-3">
                                        <ul>
                                            <li><a href="about-1.html">About 1</a></li>
                                            <li><a href="about-2.html">About 2</a></li>
                                            <li><a href="services-1.html">Services 1</a></li>
                                            <li><a href="services-2.html">Services 2</a></li>
                                            <li><a href="services-3.html">Services 3</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                        <ul>
                                            <li><a href="portfolio-1.html">Portfolio 1</a></li>
                                            <li><a href="portfolio-2.html">Portfolio 2</a></li>
                                            <li><a href="blog-horizontal.html">Blog - Horizontal</a></li>
                                            <li><a href="blog-vertical.html">Blog - Vertical</a></li>
                                            <li><a href="blog-post.html">Blog Single Post</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                        <ul>
                                            <li><a href="clients.html">Clients</a></li>
                                            <li><a href="team.html">Team</a></li>
                                            <li><a href="testimonial.html">Testimonial</a></li>
                                            <li><a href="faq.html">FAQ</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                        <ul>
                                            <li><a href="contact.html">Contact</a></li>
                                            <li><a href="404.html">404 Error</a></li>
                                            <li><a href="page-fullwidth.html">Fullwidth page</a></li>
                                            <li><a href="page-with-sidebar.html">Page with sidebar</a></li>
                                        </ul>
                                    </div>
                                </div>
                             </div>
                        </li>
                        <li class="has-dropdown-wide">
                            <a href="#">Elements</a>
                             <div class="nav-main-dropdown">
                                <div class="row">
                                    <div class="col-md-3">
                                        <ul>
                                            <li><a href="typography.html">Typography</a></li>
                                            <li><a href="tables.html">Tables</a></li>
                                            <li><a href="forms.html">Forms</a></li>
                                            <li><a href="buttons-labels.html">Buttons &amp; Labels</a></li>
                                            <li><a href="alerts-wells.html">Alerts &amp; Wells</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                        <ul>
                                            <li><a href="progress-bars.html">Progress Bars</a></li>
                                            <li><a href="tooltips-popovers-modals.html">Tooltips, Popovers &amp; Modals</a></li>
                                            <li><a href="tabs-accordions.html">Tabs &amp; Accordions</a></li>
                                            <li><a href="carousels.html">Carousels</a></li>
                                            <li><a href="maps.html">Maps</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                        <ul>
                                            <li><a href="icons.html">Icons</a></li>
                                            <li><a href="icon-boxes.html">Icon Boxes</a></li>
                                            <li><a href="counters.html">Counters</a></li>
                                            <li><a href="testimonials.html">Testimonials</a></li>
                                            <li><a href="pricing-tables.html">Pricing Tables</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3">
                                        <ul>
                                            <li><a href="title-bars-default.html">Title Bars - Default</a></li>
                                            <li><a href="title-bars-big.html">Title Bars - Big</a></li>
                                            <li><a href="title-bars-breadcrumb.html">Title Bars - Breadcrumb Only</a></li>
                                            <li><a href="sections.html">Sections</a></li>
                                            <li><a href="images.html">Images</a></li>
                                        </ul>
                                    </div>
                                </div>
                             </div>
                        </li>
                        <li><a href="#">Buy it!</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="mobile-menu"></div>

</header>
<!-- Header / End -->

<!-- Home -->
<section id="home" class="home-sm">
	
    <div class="owl-slideshow home-full-carousel bg-carousel" data-auto-play="4500">
    	<div class="bg-image bg-image-7"></div>
        <div class="bg-image bg-image-8"></div>
        <div class="bg-image bg-image-9"></div>
    </div>
    <div class="overlay overlay-white editable-alpha" data-alpha="30"></div>  
    <div class="container vertical-center">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 vertical-center">
            	<div class="owl-carousel text-center animated" data-animation="flipInX" data-single-item="true" data-auto-play="3500" data-pagination="true">
                    <div>
                        <span class="sep sep-primary"></span>
                        <h4 class="text-uppercase"><strong>Get</strong> some great <strong>Coaching materials</strong> for <strong>free</strong>.</h4>
                        <a href="#" class="btn btn-primary">Sign in!</a><a href="#" class="btn btn-link">Read More</a>
                    </div>
                    <div>
                        <span class="sep sep-primary"></span>
                        <h4 class="text-uppercase">Do you <strong>like</strong> this <strong>awesome</strong> theme? <strong>Buy it</strong> right now!</h4>
                        <a href="#" class="btn btn-primary">Purchase it!</a><a href="#" class="btn btn-link">Read More</a>
                    </div>
                    <div>
                        <span class="sep sep-primary"></span>
                        <h4 class="text-uppercase"><strong>Get</strong> some great <strong>Coaching materials</strong> for <strong>free</strong></h4>
                        <a href="#" class="btn btn-primary">Purchase it!</a><a href="#" class="btn btn-link">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</section>
<!-- Home / End -->

<!-- Content -->
<div id="content">

	<div class="container">
    	<div class="row padded-top-40">
        
        	<!-- Left Column -->
        	<div class="col-md-8">
            
            	<section>
                    <!-- Section Title -->
                    <div class="section-title margin-bottom-40">
                        <h4 class="text-uppercase margin-bottom-0"><strong>What’s</strong> going on?</h4>
                        <span class="sep sep-primary sep-animated"></span>
                        <p class="lead font-alt">Cardigan Blue Bottle Shoreditch sartorial. Sriracha chia blog, church-key plaid pop-up next level post-ironic PBRB drinking vinegar Helvetica photo booth cornhole art party Etsy.</p>
                    </div>
                    <div class="row">
                        <!-- Icon Box -->
                        <div class="col-md-4 col-sm-6 icon-box icon-box-hover">
                            <span class="icon icon-sm icon-circle icon-primary margin-bottom-20 animated" data-animation="bounceIn" data-animation-delay="400"><i class="fa fa-book"></i></span>
                            <h5 class="margin-bottom-0">5 Premium <strong>Ebooks</strong></h5>
                            <p class="text-muted">Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
                            <a href="#" class="underline-link text-uppercase text-primary">Read more</a>
                        </div>
                        <!-- Icon Box -->
                        <div class="col-md-4 col-sm-6 icon-box icon-box-hover">
                            <span class="icon icon-sm icon-circle icon-primary margin-bottom-20 animated" data-animation="bounceIn" data-animation-delay="600"><i class="fa fa-video-camera"></i></span>
                            <h5 class="margin-bottom-0">6 <strong>Video</strong> Courses</h5>
                            <p class="text-muted">Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
                            <a href="#" class="underline-link text-uppercase text-primary">Read more</a>
                        </div>
                        <!-- Icon Box -->
                        <div class="col-md-4 col-sm-12 icon-box icon-box-hover">
                            <span class="icon icon-sm icon-circle icon-primary margin-bottom-20 animated" data-animation="bounceIn" data-animation-delay="800"><i class="fa fa-camera"></i></span>
                            <h5 class="margin-bottom-0">5 <strong>PSD Files</strong></h5>
                            <p class="text-muted">Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
                            <a href="#" class="underline-link text-uppercase text-primary">Read more</a>
                        </div>
                    </div>
                </section>
                
                <section class="padded-top-40">
                	<!-- Section Title -->
                    <div class="section-title margin-bottom-40">
                        <h4 class="text-uppercase margin-bottom-5"><strong>Trendnig</strong> articles</h4>
                        <span class="sep sep-primary"></span>
                        <p class="lead font-alt">Sriracha chia blog, church-key plaid pop-up next level post-ironic PBRB drinking vinegar Helvetica photo booth cornhole art party Etsy.</p>
                    </div>
                    <div class="row">
                    	<div class="col-sm-6">
                        	<div class="row">
                                <!-- Post  -->
                                <div class="post post-vertical col-md-12 clearfix">
                                    <div class="post-image-thumb animated" data-animation="fadeInUp" data-animation-delay="400">
                                        <img src="images/blog/post01-thumb.jpg" alt="" />
                                    </div>
                                    <div class="post-content">
                                        <div class="post-labels">
                                            <a href="#" class="label label-primary animated" data-animation="flipInX" data-animation-delay="400">Webdesign</a>
                                            <a href="#" class="label label-primary animated" data-animation="flipInX" data-animation-delay="600">Webdevelopment</a>
                                        </div>
                                        <h5 class="post-title"><a href="#">Donec molestie in libero nec lobortis nec</a></h5>
                                        <div class="post-meta">
                                            <span><i class="fa fa-clock-o"></i><a href="#">24.05.2014</a></span>
                                            <span><i class="fa fa-comment"></i><a href="#">2 Comments</a></span>
                                        </div>
                                        <p class="font-alt">Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. </p>
                                        <a href="#" class="btn btn-primary btn-xs">Read more</a>
                                    </div>
                                </div>
                                <!-- Post  -->
                                <div class="post post-vertical col-md-12 clearfix">
                                    <div class="post-image-thumb animated" data-animation="fadeInUp" data-animation-delay="600">
                                        <img src="images/blog/post02-thumb.jpg" alt="" />
                                    </div>
                                    <div class="post-content">
                                        <div class="post-labels">
                                            <a href="#" class="label label-primary animated" data-animation="flipInX" data-animation-delay="400">Webdesign</a>
                                            <a href="#" class="label label-primary animated" data-animation="flipInX" data-animation-delay="600">Webdevelopment</a>
                                        </div>
                                        <h5 class="post-title"><a href="#">Donec molestie in libero nec lobortis nec</a></h5>
                                        <div class="post-meta">
                                            <span><i class="fa fa-clock-o"></i><a href="#">24.05.2014</a></span>
                                            <span><i class="fa fa-comment"></i><a href="#">2 Comments</a></span>
                                        </div>
                                        <p class="font-alt">Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. </p>
                                        <a href="#" class="btn btn-primary btn-xs">Read more</a>
                                    </div>
                                </div>
                       		</div>
                        </div>
                        <div class="col-sm-6">
                        	<div class="row">
                                <!-- Post  -->
                                <div class="post post-vertical col-md-12 clearfix">
                                    <div class="post-image-thumb animated" data-animation="fadeInUp" data-animation-delay="800">
                                        <img src="images/blog/post03-thumb.jpg" alt="" />
                                    </div>
                                    <div class="post-content">
                                        <div class="post-labels">
                                            <a href="#" class="label label-primary animated" data-animation="flipInX" data-animation-delay="400">Webdesign</a>
                                            <a href="#" class="label label-primary animated" data-animation="flipInX" data-animation-delay="600">Webdevelopment</a>
                                        </div>
                                        <h5 class="post-title"><a href="#">Donec molestie in libero nec lobortis nec</a></h5>
                                        <div class="post-meta">
                                            <span><i class="fa fa-clock-o"></i><a href="#">24.05.2014</a></span>
                                            <span><i class="fa fa-comment"></i><a href="#">2 Comments</a></span>
                                        </div>
                                        <p class="font-alt">Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. </p>
                                        <a href="#" class="btn btn-primary btn-xs">Read more</a>
                                    </div>
                                </div>
                                <!-- Post  -->
                                <div class="post post-vertical col-md-12 clearfix">
                                    <div class="post-image-thumb animated" data-animation="fadeInUp" data-animation-delay="1000">
                                        <img src="images/blog/post04-thumb.jpg" alt="" />
                                    </div>
                                    <div class="post-content">
                                        <div class="post-labels">
                                            <a href="#" class="label label-primary animated" data-animation="flipInX" data-animation-delay="400">Webdesign</a>
                                            <a href="#" class="label label-primary animated" data-animation="flipInX" data-animation-delay="600">Webdevelopment</a>
                                        </div>
                                        <h5 class="post-title"><a href="#">Donec molestie in libero nec lobortis nec</a></h5>
                                        <div class="post-meta">
                                            <span><i class="fa fa-clock-o"></i><a href="#">24.05.2014</a></span>
                                            <span><i class="fa fa-comment"></i><a href="#">2 Comments</a></span>
                                        </div>
                                        <p class="font-alt">Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. </p>
                                        <a href="#" class="btn btn-primary btn-xs">Read more</a>
                                    </div>
                                </div>
                       		</div>
                        </div>
                    </div>
                </section>

                <!-- Ad -->
                <div class="padded-top-40">
                    <img src="images/ads/ad_600x120.jpg" class="margin-bottom-40 center-block" alt="" />
                </div>
                
                <section class="padded-vertical-40">
                	<!-- Section Title -->
                    <div class="section-title margin-bottom-40">
                        <h4 class="text-uppercase margin-bottom-5"><strong>What</strong> people say?</h4>
                        <span class="sep sep-primary"></span>
                        <p class="lead font-alt">Sriracha chia blog, church-key plaid pop-up next level post-ironic PBRB drinking vinegar Helvetica photo booth cornhole art party Etsy.</p>
                    </div>
                    <div class="row">
                    	<div class="owl-carousel owl-pagination-center animated" data-animation="zoomIn" data-animation-delay="400" data-items="2" data-items-desktop='[1200,2]' data-items-tablet='[979,1]' data-items-mobile='[479,1]'>
                            <!-- Testimonial -->
                            <div class="col-md-12 testimonial">
                                <div class="testimonial-content bg-grey font-alt">
                                    Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. 
                                </div>
                                <div class="testimonial-author testimonial-with-photo">
                                    <div class="testimonial-photo"><img class="img-circle" src="images/avatars/avatar01.jpg" alt="" /></div>
                                    <div class="testimonial-author-txt text-uppercase">
                                        <span class="author-name">Jessica Mary Doe</span>
                                        <span class="text-muted text-xs">Google Inc.</span>
                                    </div>
                                </div>
                            </div>
                            <!-- Testimonial -->
                            <div class="col-md-12 testimonial">
                                <div class="testimonial-content bg-grey font-alt">
                                    A dentist recommending chocolate? Yes, that’s right, you read correctly.
                                </div>
                                <div class="testimonial-author testimonial-with-photo">
                                    <div class="testimonial-photo"><img class="img-circle" src="images/avatars/avatar02.jpg" alt="" /></div>
                                    <div class="testimonial-author-txt text-uppercase">
                                        <span class="author-name">Mark Bow</span>
                                        <span class="text-muted text-xs">Google Inc.</span>
                                    </div>
                                </div>
                            </div>
                            <!-- Testimonial -->
                            <div class="col-md-12 testimonial">
                                <div class="testimonial-content bg-grey font-alt">
                                    Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. 
                                </div>
                                <div class="testimonial-author testimonial-with-photo">
                                    <div class="testimonial-photo"><img class="img-circle" src="images/avatars/avatar03.jpg" alt="" /></div>
                                    <div class="testimonial-author-txt text-uppercase">
                                        <span class="author-name">Gary Flick</span>
                                        <span class="text-muted text-xs">Google Inc.</span>
                                    </div>
                                </div>
                            </div>
                            <!-- Testimonial -->
                            <div class="col-md-12 testimonial">
                                <div class="testimonial-content bg-grey font-alt">
                                    A dentist recommending chocolate? Yes, that’s right, you read correctly.
                                </div>
                                <div class="testimonial-author testimonial-with-photo">
                                    <div class="testimonial-photo"><img class="img-circle" src="images/avatars/avatar04.jpg" alt="" /></div>
                                    <div class="testimonial-author-txt text-uppercase">
                                        <span class="author-name">John Doe</span>
                                        <span class="text-muted text-xs">Google Inc.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
            </div>
            
            <!-- Right Column -->
        	<div class="col-md-4">

                <!-- Sign In -->
                <div class="sign-in-box margin-bottom-40 animated" data-animation="flipInY" data-animation-delay="200">
                    <div class="sign-in-head bg-grey">
                        <span class="icon margin-bottom-10 pull-left"><i class="fa fa-download text-primary"></i></span>
                        <div class="left-space-md">
                            <h5 class="text-uppercase margin-bottom-0"><strong>Get Access</strong> to free files!</h5>
                            <p class="lead text-muted font-alt margin-bottom-0">Sign up to be the first to get all of my best business advices.</p>
                        </div>
                    </div>
                    <div class="sign-in-content bg-primary dark">
                    	<form id="sign-in-form">
                            <div class="form-group margin-bottom-0">
                                <input type="text" id="emailSigned" name="emailSigned" class="form-control bg-white margin-bottom-5" placeholder="Tap here your e-mail address...">
                            </div>
                            <div class="form-bottom-bar clearfix">
                              <span class="checkbox-group pull-left"><input id="acceptRules" name="acceptRules" type="checkbox" /><label for="acceptRules" class="checkbox-label">I accept all the <a href="#" data-toggle="modal" data-target="#rulesModal">rules</a></label></span>
                              <button type="submit" class="btn btn-filled btn-primary-dark pull-right"><i class="icon-before fa fa-plus"></i>Sign In!</button>
                            </div>
                        </form>
                    </div>
                </div>

                <!-- Ad -->
                <img src="images/ads/ad_250x250.jpg" class="margin-bottom-40 center-block" alt="" />

                <!-- Navigation list -->
                <h5 class="text-uppercase">Categories</h5>
                <ul class="nav nav-sidebar margin-bottom-40">
                    <li><a href="#">Webdesign</a></li>
                    <li><a href="#">E-Commercie</a></li>
                    <li><a href="#">Social Media</a></li>
                    <li><a href="#">Webdevelopment</a></li>
                    <li><a href="#">Graphic Design</a></li>
                </ul>
                
                <!-- Tweets -->
                <h5 class="text-uppercase">Latest <strong>Tweets</strong></h5>
                <div class="tweet font-alt margin-bottom-40"></div>
            	
            </div>
        	
    	</div>
    </div>

    <!-- Section -->
    <section class="section dark padded-vertical-70">
        <div class="bg-image bg-image-3"></div>
        <div class="overlay overlay-black editable-alpha" data-alpha="40"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Twitter -->
                    <div class="tweet tweet-horizontal"></div>
                </div>
            </div>
        </div>
    </section>
    
</div>
<!-- Content / End -->

<!-- Footer -->
<footer id="footer">

    <div class="container padded-vertical-40">
        <div class="row">
            <div class="col-sm-4 margin-bottom-20-xs">
                  <h5 class="text-light">Say <strong>Hello</strong>!</h5>
                <!-- Contact Form -->
                <form id="contact-form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="message" name="message" rows="3"  placeholder="Message"></textarea>
                    </div>
                    <div class="form-group clearfix">
                    	<button type="submit" class="btn btn-primary btn-filled btn-sm pull-right">Send it!</button>
                    </div>
                </form>
            </div>
            <div class="col-sm-4 margin-bottom-20-xs">
                  <h5 class="text-light"><strong>Contact</strong> Me</h5>
                <!-- List with Icons -->
                <ul class="list-unstyled list-unstyled-icons">
                  <li><i class="text-muted inline-icon fa fa-map-marker"></i><strong>Address:</strong> <span class="text-muted">1111-A Nowhere Lane, Outta Sight, State 90378, USA</span></li>
                  <li><i class="text-muted inline-icon fa fa-comment"></i><strong>E-mail:</strong> <span class="text-muted"><a href="#">johnathandoe@suelo.pl</a></span></li>
                  <li><i class="text-muted inline-icon fa fa-phone"></i><strong>Phone:</strong> <span class="text-muted">+0(31)6 89764536</span></li>
                </ul>
            </div>
            <div class="col-sm-4 margin-bottom-20-xs">
                  <h5 class="text-light"><strong>About</strong> me</h5>
                <p class="lead font-alt text-muted">rbi a risus eu magna tempus sollicitudin. Maecenas ullamcorper sem at velit condimentum.</p>
                <p class="font-alt text-muted">Suspendisse faucibus ante nec elit laoreet, non porta sapien facilisis. In et ipsum eu lacus condimentum faucibus ac bibendum tortor.</p>
                <a href="#" class="btn btn-primary btn-filled btn-sm">Read more</a>
            </div>
        </div>
    </div>
    
    <!-- Copyright -->
    <div class="copyright bg-grey">
        <div class="container padded-vertical-30 text-center">
        <div class="text-muted margin-bottom-20">Copyright <strong>Trener - Multipurpose Coaching / Trainer Bootstrap Theme</strong> 2014&copy;</div>
          <a href="#" class="icon icon-circle icon-filled icon-xs icon-facebook"><i class="fa fa-facebook"></i></a>
          <a href="#" class="icon icon-circle icon-filled icon-xs icon-twitter"><i class="fa fa-twitter"></i></a>
          <a href="#" class="icon icon-circle icon-filled icon-xs icon-google-plus"><i class="fa fa-google-plus"></i></a>
      </div>
    </div>

</footer>
<!-- Footer / End -->

<!-- Alert Modal -->
<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title text-center" id="alertModalLabel"></h4>
      </div>
      <div class="modal-body text-center">
        <div class="modal-alert-content margin-bottom-20"></div>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="icon-before fa fa-times"></i>Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Alert Modal / End -->

<!-- Scripts -->
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/page-loader.js"></script>
<script type="text/javascript" src="js/jquery.appear.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="js/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="js/waypoints.min.js"></script>
<script type="text/javascript" src="js/jquery.localscroll-1.2.7-min.js"></script>
<script type="text/javascript" src="js/jquery.scrollTo-1.4.3.1-min.js"></script>
<script type="text/javascript" src="js/jquery.tweet.js"></script>
<script type="text/javascript" src="js/jquery.parallax.min.js"></script>
<script type="text/javascript" src="js/jquery.fitvids.js"></script>

<!-- Custom Script -->
<script type="text/javascript" src="js/custom.js"></script>

<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->
<!--
<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
-->

</body>

</html>
