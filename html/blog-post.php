<!DOCTYPE html>
<html lang="en">
<head>

<!-- Meta -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<!-- Title -->
<title>Trener - Responsive HTML/CSS Template</title>

<!-- Favicons -->
<link rel="shortcut icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/favicon_60x60.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/favicon_76x76.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/favicon_120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/favicon_152x152.png">

<!-- Google Web Fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

<!-- Stylesheets -->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/animations.css" />
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="css/owl.transitions.css" />
<link rel="stylesheet" type="text/css" href="css/owl.theme.css" />
<link rel="stylesheet" type="text/css" href="colors/red.css" id="colors" />
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />

</head>

<body class="sticky-nav">

<!-- Loader -->
<div id="page-loader" class="loader-white"></div>

<!-- Header -->
<header id="header" class="header header-white wide clearfix">

    <div class="row">
        <div class="col-xs-12">
            <div class="nav-bar clearfix">
                <!-- Logo -->
                <div class="logo pull-left">
                    <a href="index.html"><img src="images/logo-black.png" alt="Trener" /></a>
                </div>
                <!-- Mobile Nav Toggle -->
                <a href="" class="mobile-menu-toggle pull-right"></a>
                <!-- Social Icons -->
                <div class="social-icons text-muted pull-right margin-left-20 hidden-sm hidden-xs">
                   <a href="#" class="icon icon-xs icon-facebook"><i class="fa fa-facebook"></i></a>
           		   <a href="#" class="icon icon-xs icon-twitter"><i class="fa fa-twitter"></i></a>
          		   <a href="#" class="icon icon-xs icon-google-plus"><i class="fa fa-google-plus"></i></a> 
                </div> 
                <!-- Main Navigation-->
                <ul class="menu nav nav-main text-uppercase pull-right margin-left-20 dropdown-dark">
	                    <li class="has-dropdown">
	                        <a href="#">Home</a>
	                        <div class="nav-main-dropdown">
	                            <ul>
	                                <li><a href="index.html">Trainer</a></li>
	                                <li><a href="index-marketer.html">Marketer</a></li>
	                                <li><a href="index-professional.html">Professional</a></li>
	                            </ul>
	                        </div>
	                    </li>
	                    <li class="has-dropdown">
	                        <a href="#">Features</a>
	                        <div class="nav-main-dropdown">
	                            <ul>
	                                <li class="has-dropdown">
	                                    <a href="#"><i class="icon-before fa fa-star text-muted"></i>Headers</a>
	                                    <ul>
	                                        <li><a href="headers-light.html">Headers Light</a></li>    
	                                        <li><a href="headers-dark.html">Headers Dark</a></li>   
	                                        <li><a href="headers-light-wide.html">Headers Light &amp; Wide</a></li>    
	                                        <li><a href="headers-dark-wide.html">Headers Dark &amp; Wide</a></li>     
	                                    </ul>
	                                </li>
	                                <li class="has-dropdown">
	                                    <a href="#"><i class="icon-before fa fa-desktop text-muted"></i>Sliders</a>
	                                    <ul>
	                                        <li><a href="slider-default.html">Default Slider</a></li>    
	                                        <li><a href="slider-text.html">Text Slider</a></li>      
	                                        <li><a href="slider-photo.html">Photo Slider</a></li>
	                                        <li class="has-dropdown">
	                                            <a href="#">Slider Sizes</a>
	                                            <ul>
	                                                <li><a href="slider-size-default.html">Deafult</a></li>
	                                                <li><a href="slider-size-small.html">Small</a></li>
	                                                <li><a href="slider-size-fullheight.html">Fullheight</a></li>
	                                            </ul>
	                                        </li>  
	                                    </ul>
	                                </li>
	                                <li class="has-dropdown">
	                                    <a href="#"><i class="icon-before fa fa-road text-muted"></i>Footers</a>
	                                    <ul>
	                                        <li><a href="footers-light.html">Footers Light</a></li>    
	                                        <li><a href="footers-dark.html">Footers Dark</a></li>      
	                                    </ul>
	                                </li>
	                                <li><a href="animations.html"><i class="icon-before fa fa-bolt text-muted"></i>Animations</a></li>
	                            </ul>
	                        </div>
	                    </li>
	                    <li class="has-dropdown-wide">
	                         <a href="#">Templates</a>
	                         <div class="nav-main-dropdown">
	                            <div class="row">
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="about-1.html">About 1</a></li>
	                                        <li><a href="about-2.html">About 2</a></li>
	                                        <li><a href="services-1.html">Services 1</a></li>
	                                        <li><a href="services-2.html">Services 2</a></li>
	                                        <li><a href="services-3.html">Services 3</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="portfolio-1.html">Portfolio 1</a></li>
	                                        <li><a href="portfolio-2.html">Portfolio 2</a></li>
	                                        <li><a href="blog-horizontal.html">Blog - Horizontal</a></li>
	                                        <li><a href="blog-vertical.html">Blog - Vertical</a></li>
	                                        <li><a href="blog-post.html">Blog Single Post</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="clients.html">Clients</a></li>
	                                        <li><a href="team.html">Team</a></li>
	                                        <li><a href="testimonial.html">Testimonial</a></li>
	                                        <li><a href="faq.html">FAQ</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="contact.html">Contact</a></li>
	                                        <li><a href="404.html">404 Error</a></li>
	                                        <li><a href="page-fullwidth.html">Fullwidth page</a></li>
	                                        <li><a href="page-with-sidebar.html">Page with sidebar</a></li>
	                                    </ul>
	                                </div>
	                            </div>
	                         </div>
	                    </li>
	                    <li class="has-dropdown-wide">
	                        <a href="#">Elements</a>
	                         <div class="nav-main-dropdown">
	                            <div class="row">
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="typography.html">Typography</a></li>
	                                        <li><a href="tables.html">Tables</a></li>
	                                        <li><a href="forms.html">Forms</a></li>
	                                        <li><a href="buttons-labels.html">Buttons &amp; Labels</a></li>
	                                        <li><a href="alerts-wells.html">Alerts &amp; Wells</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="progress-bars.html">Progress Bars</a></li>
	                                        <li><a href="tooltips-popovers-modals.html">Tooltips, Popovers &amp; Modals</a></li>
	                                        <li><a href="tabs-accordions.html">Tabs &amp; Accordions</a></li>
	                                        <li><a href="carousels.html">Carousels</a></li>
                                            <li><a href="maps.html">Maps</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="icons.html">Icons</a></li>
	                                        <li><a href="icon-boxes.html">Icon Boxes</a></li>
	                                        <li><a href="counters.html">Counters</a></li>
	                                        <li><a href="testimonials.html">Testimonials</a></li>
	                                        <li><a href="pricing-tables.html">Pricing Tables</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="title-bars-default.html">Title Bars - Default</a></li>
	                                        <li><a href="title-bars-big.html">Title Bars - Big</a></li>
	                                        <li><a href="title-bars-breadcrumb.html">Title Bars - Breadcrumb Only</a></li>
	                                        <li><a href="sections.html">Sections</a></li>
	                                        <li><a href="images.html">Images</a></li>
	                                    </ul>
	                                </div>
	                            </div>
	                         </div>
	                    </li>
	                    <li><a href="#">Buy it!</a></li>
	                </ul>
            </div>
        </div>
    </div>
    <div class="mobile-menu text-uppercase"></div>

</header>
<!-- Header / End -->

<!-- Title Bar -->
<div id="page-title">
        
    <!-- Breadcrumbs -->
	<div class="bg-grey">	
    	<div class="container">
        	<ol class="breadcrumb text-uppercase">
                  <li><a href="#">Home</a></li>
                  <li class="active">Working on new awesome themes for everyone!</li>
            </ol>
        </div>
    </div>

</div>
<!-- Title Bar / End -->

<!-- Content -->
<div id="content">

	<div class="container">
    	<div class="row padded-top-40">
        
        	<!-- Left Column -->
        	<div class="col-md-8">
            
                <!-- Post  -->
                <article class="post">
                    <div class="post-image margin-bottom-30">
                        <img src="images/blog/post05.jpg" alt="" />
                    </div>
                    <div class="post-labels">
                        <a href="#" class="label label-primary animated" data-animation="flipInX" data-animation-delay="400">Webdesign</a>
                        <a href="#" class="label label-primary animated" data-animation="flipInX" data-animation-delay="600">Webdevelopment</a>
                    </div>
                    <h2 class="post-title">Working on new awesome themes for everyone!</h2>
                    <div class="post-meta">
                        <span><i class="fa fa-clock-o"></i><a href="#">24.05.2014</a></span>
                        <span><i class="fa fa-comment"></i><a href="#">2 Comments</a></span>
                    </div>
                    <p class="lead font-alt">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi pretium enim lacus, eu fermentum velit sagittis eget. Quisque finibus magna in scelerisque dapibus. In finibus sem eget vestibulum tincidunt. Quisque varius turpis luctus augue accumsan egestas.</p>
                    <h5>Introduction</h5>
           			<img src="images/gallery/about01.jpg" class="img-rounded margin-right-20 margin-bottom-20 pull-left" alt="" />
           			<p class="lead">Morbi pretium enim lacus, eu fermentum velit sagittis eget. Quisque finibus magna in scelerisque dapibus. In finibus sem eget vestibulum tincidunt. Quisque varius turpis luctus augue accumsan egestas.</p>
			    	<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tristique suscipit volutpat. Suspendisse iaculis iaculis enim. Integer mattis lorem ut tincidunt egestas. Donec eu lorem et libero semper mollis non id nibh. Duis non nunc sit amet nulla dictum mollis in vitae quam. Sed vitae erat eu massa interdum dignissim. Sed egestas eu eros non dapibus. In libero libero, tempus in semper et, vehicula in lacus. Suspendisse pharetra, dui quis pellentesque tincidunt, diam tortor maximus urna, quis tempor quam eros at metus. Proin commodo sem sed scelerisque scelerisque. Pellentesque felis ligula, blandit et cursus non, blandit at turpis.</p>
			    	<h5>Main Part</h5>
			    	<img src="images/gallery/about02.jpg" class="img-rounded margin-left-20 margin-bottom-20 pull-right" alt="" />
			    	<p class="lead">Morbi pretium enim lacus, eu fermentum velit sagittis eget. Quisque finibus magna in scelerisque dapibus. In finibus sem eget vestibulum tincidunt. Quisque varius turpis luctus augue accumsan egestas.</p>
			    	<p> Donec eu lorem et libero semper mollis non id nibh. Duis non nunc sit amet nulla dictum mollis in vitae quam. Sed vitae erat eu massa interdum dignissim. Sed egestas eu eros non dapibus. In libero libero, tempus in semper et, vehicula in lacus. Suspendisse pharetra, dui quis pellentesque tincidunt, diam tortor maximus urna, quis tempor quam eros at metus. Proin commodo sem sed scelerisque scelerisque. Pellentesque felis ligula, blandit et cursus non, blandit at turpis. </p>
			    	<!-- Testimonial -->
					<div class="testimonial margin-bottom-30">
					  <div class="testimonial-content bg-grey font-alt">
					      Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. 
					  </div>
					  <div class="testimonial-author testimonial-with-photo">
					      <div class="testimonial-photo"><img class="img-circle" src="images/avatars/avatar01.jpg" alt="" /></div>
					      <div class="testimonial-author-txt text-uppercase">
					          <span class="author-name">Jessica Mary Doe</span>
					          <span class="text-muted text-xs">Google Inc.</span>
					      </div>
					  </div>
					</div>
			    	<h5>Summary</h5>
			    	<p class="lead"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tristique suscipit volutpat. Suspendisse iaculis iaculis enim. Integer mattis lorem ut tincidunt egestas. Donec eu lorem et libero semper mollis non id nibh.</p>
					<ul class="text-primary">
					  <li>Lorem ipsum dolor sit amet</li>
					  <li>Consectetur adipiscing elit</li>
					  <li>Integer molestie lorem at massa
					    <ul>
					      <li>Phasellus iaculis neque</li>
					      <li>Purus sodales ultricies</li>
					      <li>Vestibulum laoreet porttitor sem</li>
					    </ul>
					  </li>
					  <li>Faucibus porta lacus fringilla vel</li>
					  <li>Aenean sit amet erat nunc</li>
					</ul>
			    	<p>Duis non nunc sit amet nulla dictum mollis in vitae quam. Sed vitae erat eu massa interdum dignissim. Sed egestas eu eros non dapibus. In libero libero, tempus in semper et, vehicula in lacus. Suspendisse pharetra, dui quis pellentesque tincidunt, diam tortor maximus urna, quis tempor quam eros at metus. Proin commodo sem sed scelerisque scelerisque. Pellentesque felis ligula, blandit et cursus non, blandit at turpis.</p>
                </article>

				<hr />

                <!-- Author -->
                <h4>About the <strong>author</strong></h4>
                <div class="post-author">
                	<div class="post-author-photo"><img src="images/avatars/avatar01.jpg" alt="" class="img-circle" /></div>
                	<div class="post-author-content">
                		<h5 class="margin-bottom-5">Jessica Webber</h5>
                		<p class="font-alt margin-bottom-10">In libero libero, tempus in semper et, vehicula in lacus. Suspendisse pharetra, dui quis pellentesque tincidunt, diam tortor maximus urna, quis tempor quam eros at metus.</p>
                		<a href="#" class="icon icon-circle icon-filled icon-xs icon-facebook"><i class="fa fa-facebook"></i></a>
            		    <a href="#" class="icon icon-circle icon-filled icon-xs icon-twitter"><i class="fa fa-twitter"></i></a>
           			    <a href="#" class="icon icon-circle icon-filled icon-xs icon-google-plus"><i class="fa fa-google-plus"></i></a>
                	</div>
                </div>

                <hr />

                <!-- Comments -->
                <h4><strong>Comments</strong> (3)</h4>
                <!-- Comments -->
                <ul class="comments">
                    <li class="single-comment">
                        <div class="comment-avatar"><img src="images/avatars/avatar01.jpg" alt="" /></div>
                        <div class="comment-content">
                            <div class="comment-head">
                                <h5 class="comment-title margin-bottom-0">Really awesome theme!</h5>
                                <span class="text-xs"><span class="text-muted">Added</span> 5 days ago <span class="text-muted">by</span> <a href="#">Michael Kors</a></span>
                            </div>
                            <div class="comment-text font-alt">
                                In ut risus at risus ultricies sodales. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum porttitor congue accumsan. Cum sociis natoque penatibus et magnis dis parturient montes 
                            </div>
                            <a href="#" class="btn btn-xs btn-primary"><i class="icon-before fa fa-pencil"></i>Write a reply</a>
                        </div>
                        <ul class="comments">
		                    <li class="single-comment">
		                        <div class="comment-avatar"><img src="images/avatar-default.jpg" alt="" /></div>
		                        <div class="comment-content">
		                            <div class="comment-head">
		                                <h5 class="comment-title margin-bottom-0">Yes, you are right!</h5>
		                                <span class="text-xs"><span class="text-muted">Added</span> 5 days ago <span class="text-muted">by</span> <a href="#">Michael Kors</a></span>
		                            </div>
		                            <div class="comment-text font-alt">
		                               Vestibulum porttitor congue accumsan. Cum sociis natoque penatibus et magnis dis parturient montes 
		                            </div>
		                        </div>
		                    </li>
		                    <li class="single-comment">
		                        <div class="comment-avatar"><img src="images/avatars/avatar02.jpg" alt="" /></div>
		                        <div class="comment-content">
		                            <div class="comment-head">
		                                <h5 class="comment-title margin-bottom-0">Yep!</h5>
		                                <span class="text-xs"><span class="text-muted">Added</span> 5 days ago <span class="text-muted">by</span> <a href="#">Michael Kors</a></span>
		                            </div>
		                            <div class="comment-text font-alt">
		                                Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum porttitor congue accumsan. Cum sociis natoque penatibus et magnis dis parturient montes 
		                            </div>
		                        </div>
		                    </li>
		                </ul>
                    </li>
                    <li class="single-comment">
                        <div class="comment-avatar"><img src="images/avatar-default.jpg" alt="" /></div>
                        <div class="comment-content">
                            <div class="comment-head">
                                <h5 class="comment-title margin-bottom-0">Lots of good wishes!</h5>
                                <span class="text-xs"><span class="text-muted">Added</span> 5 days ago <span class="text-muted">by</span> <a href="#">Michael Kors</a></span>
                            </div>
                            <div class="comment-text font-alt">
                                In ut risus at risus ultricies sodales. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum porttitor congue accumsan. Cum sociis natoque penatibus et magnis dis parturient montes 
                            </div>
                            <a href="#" class="btn btn-xs btn-primary"><i class="icon-before fa fa-pencil"></i>Write a reply</a>
                        </div>
                    </li>
                    <li class="single-comment">
                        <div class="comment-avatar"><img src="images/avatars/avatar01.jpg" alt="" /></div>
                        <div class="comment-content">
                            <div class="comment-head">
                                <h5 class="comment-title margin-bottom-0">Really awesome theme!</h5>
                                <span class="text-xs"><span class="text-muted">Added</span> 5 days ago <span class="text-muted">by</span> <a href="#">Michael Kors</a></span>
                            </div>
                            <div class="comment-text font-alt">
                                In ut risus at risus ultricies sodales.  Vestibulum porttitor congue accumsan. Cum sociis natoque penatibus et magnis dis parturient montes 
                            </div>
                            <a href="#" class="btn btn-xs btn-primary"><i class="icon-before fa fa-pencil"></i>Write a reply</a>
                        </div>
                    </li>
                </ul>

                <hr />

                <!-- Write a comment -->
                <h4><strong>Write</strong> a comment</h4>
                <form class="margin-bottom-40 clearfix">
                	<div class="form-group">
				    	<input type="text" class="form-control" id="commentName" name="name" placeholder="Name">
				    </div>
				    <div class="form-group">
				    	<textarea class="form-control" id="comment" name="message" rows="3"  placeholder="Message"></textarea>
				    </div>
				    <div class="form-group clearfix">
                    	<button type="submit" class="btn btn-primary btn-filled btn-sm pull-right">Send it!</button>
                    </div>
                </form>

            </div>
            
            <!-- Right Column -->
        	<div class="col-md-4">

                <!-- Sign In -->
                <div class="sign-in-box margin-bottom-40 animated" data-animation="flipInY" data-animation-delay="200">
                    <div class="sign-in-head bg-grey">
                        <span class="icon margin-bottom-10 pull-left"><i class="fa fa-download text-primary"></i></span>
                        <div class="left-space-md">
                            <h5 class="text-uppercase margin-bottom-0"><strong>Get Access</strong> to free files!</h5>
                            <p class="lead text-muted font-alt margin-bottom-0">Sign up to be the first to get all of my best business advices.</p>
                        </div>
                    </div>
                    <div class="sign-in-content bg-primary dark">
                    	<form id="sign-in-form">
                            <div class="form-group margin-bottom-0">
                                <input type="text" id="emailSigned" name="emailSigned" class="form-control bg-white margin-bottom-5" placeholder="Tap here your e-mail address...">
                            </div>
                            <div class="form-bottom-bar clearfix">
                              <span class="checkbox-group pull-left"><input id="acceptRules" name="acceptRules" type="checkbox" /><label for="acceptRules" class="checkbox-label">I accept all the <a href="#" data-toggle="modal" data-target="#rulesModal">rules</a></label></span>
                              <button type="submit" class="btn btn-filled btn-primary-dark pull-right"><i class="icon-before fa fa-plus"></i>Sign In!</button>
                            </div>
                        </form>
                    </div>
                </div>

                <!-- Ad -->
                <img src="images/ads/ad_250x250.jpg" class="margin-bottom-40 center-block" alt="" />

                <!-- Navigation list -->
                <h5 class="text-uppercase">Categories</h5>
                <ul class="nav nav-sidebar margin-bottom-40">
                    <li><a href="#">Webdesign</a></li>
                    <li><a href="#">E-Commercie</a></li>
                    <li><a href="#">Social Media</a></li>
                    <li><a href="#">Webdevelopment</a></li>
                    <li><a href="#">Graphic Design</a></li>
                </ul>
                
                <!-- Tweets -->
                <h5 class="text-uppercase">Latest <strong>Tweets</strong></h5>
                <div class="tweet font-alt margin-bottom-40"></div>
            	
            </div>
            
        </div>
    </div>
  
    <!-- Section -->
    <section class="section padded-vertical-70">
    	<div class="bg-image bg-image-4"></div>
        <div class="overlay overlay-white editable-alpha" data-alpha="10"></div>
        <div class="container">
          <span class="pull-left icon icon-lg margin-bottom-0 animated bounce-in"><i class="fa fa-play-circle-o text-primary"></i></span>
          <div class="left-space-lg animated fade-in-right">
              <h4 class="text-uppercase margin-bottom-0"><strong>Get Access</strong> to free files!</h4>
              <p class="lead text-muted font-alt">Sign up to be the first to get all of my best business advices for free to your inbox.</p>
              <a href="#" class="btn btn-primary btn-filled">Purchase it!</a><a href="#" class="btn btn-link">Read More</a>
          </div>
        </div>
    </section>
    
</div>
<!-- Content / End -->

<!-- Footer -->
<footer id="footer" class="bg-black dark">
  
  <div class="container padded-vertical-40">
      <div class="row">
          <div class="col-md-2 col-sm-6">
          	  <h5 class="text-light"><strong>Navigation</strong></h5>
              <!-- Navigation list -->
              <ul class="list-navigation margin-bottom-20">
              	<li><a href="#">Home Page</a></li>
                <li><a href="#">Features</a></li>
                <li><a href="#">Templates</a></li>
                <li><a href="#">Elements</a></li>
                <li><a href="#">Documentation</a></li>
                <li><a href="#">Buy it!</a></li>
              </ul>
          </div>
          <div class="col-md-4 col-sm-6">
          	  <h5 class="text-light">Say <strong>Hello</strong>!</h5>
              <!-- Contact Form -->
              <form id="contact-form" class="margin-bottom-20">
              		<div class="row">
              			<div class="col-md-6">
		                  	<div class="form-group">
		                    	<input type="text" class="form-control" id="name" name="name" placeholder="Name">
		                    </div>
	                    </div>
	                    <div class="col-md-6">
		                    <div class="form-group">
		                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
		                    </div>
	                    </div>
                    </div>
                    <div class="form-group">
                    	<textarea class="form-control" id="message" name="message" rows="3"  placeholder="Message"></textarea>
                    </div>
                    <div class="form-group clearfix">
						<button type="submit" class="btn btn-primary btn-filled btn-sm pull-right">Send it!</button>
                    </div>
              </form>
          </div>
          <div class="col-md-4 col-sm-6">
          	  <h5 class="text-light"><strong>Contact</strong> Me</h5>
              <!-- List with Icons -->
              <ul class="list-unstyled list-unstyled-icons margin-bototm-20">
                <li><i class="text-muted inline-icon fa fa-map-marker"></i><strong>Address:</strong> <span class="text-muted">1111-A Nowhere Lane, Outta Sight, State 90378, USA</span></li>
                <li><i class="text-muted inline-icon fa fa-comment"></i><strong>E-mail:</strong> <span class="text-muted"><a href="#">johnathandoe@suelo.pl</a></span></li>
                <li><i class="text-muted inline-icon fa fa-phone"></i><strong>Phone:</strong> <span class="text-muted">+0(31)6 89764536</span></li>
              </ul>
          </div>
          <div class="col-md-2 col-sm-6">
          	  <h5 class="text-light"><strong>Social</strong> Media</h5>
              <!-- Social Icons -->
              <div class="margin-bototm-20">
                  <a href="#" class="icon icon-circle icon-filled icon-xs icon-facebook"><i class="fa fa-facebook"></i></a>
                  <a href="#" class="icon icon-circle icon-filled icon-xs icon-twitter"><i class="fa fa-twitter"></i></a>
                  <a href="#" class="icon icon-circle icon-filled icon-xs icon-google-plus"><i class="fa fa-google-plus"></i></a>
              </div>
          </div>
      </div>
  </div>
  
  <!-- Copyright -->
  <div class="copyright bordered-top">
  	<div class="container padded-vertical-30">
    	<span class="text-muted">Copyright <strong>Trener - Multipurpose Coaching / Trainer Bootstrap Theme</strong> 2014&copy;</span>
        <ul class="list-inline pull-right text-muted margin-bottom-0">
        	<li><a href="#">Home page</a></li>
            <li><a href="#">Documentation</a></li>
            <li><a href="#">Buy it!</a></li>
        </ul>
    </div>
  </div>
  
</footer>
<!-- Footer / End -->

<!-- Alert Modal -->
<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title text-center" id="alertModalLabel"></h4>
      </div>
      <div class="modal-body text-center">
        <div class="modal-alert-content margin-bottom-20"></div>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="icon-before fa fa-times"></i>Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Alert Modal / End -->

<!-- Rules Modal -->
<div class="modal fade" id="rulesModal" tabindex="-1" role="dialog" aria-labelledby="rulesModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="rulesModalLabel">Trener - Sign In Demo - Rules </h4>
      </div>
      <div class="modal-body">
        <ol>
        	<li>This form will send you a <strong>demo message</strong> on your e-mail address.</li>
        	<li>Your e-mail address <strong>won't be used for any purpose</strong> other than the one specified in the first point.</li>
        </ol>
        <p class="margin-bottom-0">If you accept those rules please fill the form and press <strong>"Sign In"</strong> button.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Rules Modal / End -->

<!-- Page Curtain -->
<div id="page-curtain" class="curtain-white"></div>

<!-- Scripts -->
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/page-loader.js"></script>
<script type="text/javascript" src="js/jquery.appear.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="js/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="js/waypoints.min.js"></script>
<script type="text/javascript" src="js/jquery.localscroll-1.2.7-min.js"></script>
<script type="text/javascript" src="js/jquery.scrollTo-1.4.3.1-min.js"></script>
<script type="text/javascript" src="js/jquery.tweet.js"></script>
<script type="text/javascript" src="js/jquery.parallax.min.js"></script>
<script type="text/javascript" src="js/jquery.fitvids.js"></script>

<!-- Custom Script -->
<script type="text/javascript" src="js/custom.js"></script>

<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->
<!--
<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
-->

</body>

</html>
