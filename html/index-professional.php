<!DOCTYPE html>
<html lang="en">
<head>

<!-- Meta -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<!-- Title -->
<title>Trener - Responsive HTML/CSS Template</title>

<!-- Favicons -->
<link rel="shortcut icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/favicon_60x60.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/favicon_76x76.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/favicon_120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/favicon_152x152.png">

<!-- Google Web Fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

<!-- Stylesheets -->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/animations.css" />
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="css/owl.transitions.css" />
<link rel="stylesheet" type="text/css" href="css/owl.theme.css" />
<link rel="stylesheet" type="text/css" href="css/colorbox.css" />
<link rel="stylesheet" type="text/css" href="colors/red.css" id="colors" />
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />

</head>

<body class="sticky-nav">

<!-- Loader -->
<div id="page-loader" class="loader-white"></div>

<!-- Header -->
<header id="header" class="header header-white transparent wide dark clearfix">

    <div class="row">
        <div class="col-xs-12">
            <div class="nav-bar clearfix">
                <!-- Logo -->
                <div class="logo pull-left">
                    <a href="index.html"><img src="images/logo-black.png" alt="Trener" /></a>
                </div>
                <!-- Mobile Nav Toggle -->
                <a href="" class="mobile-menu-toggle pull-right"></a>
                <!-- Social Icons -->
                <div class="social-icons text-muted pull-right margin-left-20 hidden-sm hidden-xs">
                   <a href="#" class="icon icon-xs icon-facebook"><i class="fa fa-facebook"></i></a>
           		   <a href="#" class="icon icon-xs icon-twitter"><i class="fa fa-twitter"></i></a>
          		   <a href="#" class="icon icon-xs icon-google-plus"><i class="fa fa-google-plus"></i></a> 
                </div> 
                <!-- Main Navigation-->
                <ul class="menu nav nav-main text-uppercase pull-right margin-left-20 dropdown-dark">
                    <li class="has-dropdown">
                        <a href="#home">Home</a>
                        <div class="nav-main-dropdown">
                            <ul>
                                <li><a href="index.html">Trainer</a></li>
                                <li><a href="index-marketer.html">Marketer</a></li>
                                <li><a href="index-professional.html">Professional</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><span class="bang"><a href="#about">About</a></span></li>
                    <li><a href="#latestPosts">Latest Posts</a></li>
                    <li><a href="#gallery">Gallery</a></li>
                    <li><a href="#contact">Contact</a></li>
                    <li><a href="#signIn"><i class="icon-before fa fa-plus text-primary"></i>Sign In</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="mobile-menu text-uppercase"></div>

</header>
<!-- Header / End -->

<!-- Home -->
<section id="home" class="home-default home-viewport dark">
	
    <div class="owl-carousel home-full-carousel" data-single-item="true" data-auto-play="6500">
    	<!-- Slide slide -->
    	<div class="photo-slide">
            <div class="bg-image bg-image-1"></div>
            <div class="overlay overlay-black editable-alpha" data-alpha="70"></div>  
            <div class="container vertical-center">
                <div class="slide-txt text-center">
                    <span class="sep sep-primary"></span>
                    <h4 class="text-uppercase"><strong>Get</strong> some great <strong>Coaching materials</strong> for <strong>free</strong>.</h4>
                    <a href="#" class="btn btn-primary">Sign in!</a><a href="#" class="btn btn-link">Read More</a>
                </div>
             </div>
        </div>
        <!-- Slide slide -->
    	<div class="photo-slide">
            <div class="bg-image bg-image-2"></div>
            <div class="overlay overlay-black editable-alpha" data-alpha="70"></div>  
            <div class="container vertical-center">
                <div class="slide-txt text-center">
                    <span class="sep sep-primary"></span>
                    <h4 class="text-uppercase">Do you <strong>like</strong> this <strong>awesome</strong> theme? <strong>Buy it</strong> right now!</h4>
                    <a href="#" class="btn btn-primary">Sign in!</a><a href="#" class="btn btn-link">Read More</a>
                </div>
             </div>
        </div>
        <!-- Slide slide -->
    	<div class="photo-slide">
            <div class="bg-image bg-image-3"></div>
            <div class="overlay overlay-black editable-alpha" data-alpha="70"></div>  
            <div class="container vertical-center">
                <div class="slide-txt text-center">
                    <span class="sep sep-primary"></span>
                    <h4 class="text-uppercase"><strong>More</strong> great stuff<strong> Comming Soon!</strong></h4>
                    <a href="#" class="btn btn-primary">Sign in!</a><a href="#" class="btn btn-link">Read More</a>
                </div>
             </div>
        </div>
    </div>
    
</section>
<!-- Home / End -->

<!-- Section -->
<section id="about" class="section padded-vertical-70 container">
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="section-title text-center margin-bottom-40">
                <h4 class="text-uppercase margin-bottom-0"><strong>What’s</strong> going on?</h4>
                <span class="sep sep-primary sep-animated"></span>
                <p class="lead font-alt">Cardigan Blue Bottle Shoreditch sartorial. Sriracha chia blog, church-key plaid pop-up next level post-ironic PBRB drinking vinegar Helvetica photo booth cornhole art party Etsy.</p>
            </div>
         </div>
    </div>
    <div class="row">
        <!-- Icon Box -->
        <div class="col-md-3 col-sm-6 icon-box icon-box-hover text-center">
            <span class="icon icon-sm icon-circle icon-primary margin-bottom-20 animated" data-animation="bounceIn" data-animation-delay="400"><i class="fa fa-book"></i></span>
            <h5 class="margin-bottom-0">5 Premium <strong>Ebooks</strong></h5>
            <p class="text-muted">Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
        </div>
        <!-- Icon Box -->
        <div class="col-md-3 col-sm-6 icon-box icon-box-hover text-center">
            <span class="icon icon-sm icon-circle icon-primary margin-bottom-20 animated" data-animation="bounceIn" data-animation-delay="600"><i class="fa fa-video-camera"></i></span>
            <h5 class="margin-bottom-0">6 <strong>Video</strong> Courses</h5>
            <p class="text-muted">Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
        </div>
        <!-- Icon Box -->
        <div class="col-md-3 col-sm-6 icon-box icon-box-hover text-center">
            <span class="icon icon-sm icon-circle icon-primary margin-bottom-20 animated" data-animation="bounceIn" data-animation-delay="800"><i class="fa fa-camera"></i></span>
            <h5 class="margin-bottom-0">5 <strong>PSD Files</strong></h5>
            <p class="text-muted">Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
        </div>
        <!-- Icon Box -->
        <div class="col-md-3 col-sm-6 icon-box icon-box-hover text-center">
            <span class="icon icon-sm icon-circle icon-filled icon-primary margin-bottom-20 animated" data-animation="bounceIn" data-animation-delay="1000"><i class="fa fa-heart"></i></span>
            <h5 class="margin-bottom-0">+<strong>Much more</strong> in future!</h5>
            <p class="text-muted">Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
        </div>
    </div>
</section>
<!-- Section / End -->

<!-- Section -->
<section id="signIn" class="section padded-vertical-70 dark">
    <div class="bg-image bg-image-1"></div>
    <div class="overlay overlay-black editable-alpha" data-alpha="60"></div>
    <div class="container">	
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 text-center">
                <span class="con icon-lg margin-bottom-20 animated" data-animation="bounceIn" data-animation-delay="400"><i class="fa fa-download text-primary"></i></span>
                <div class="margin-bottom-20">
                    <h4 class="text-uppercase margin-bottom-0"><strong>Get Access</strong> to free files!</h4>
                    <p class="lead text-muted font-alt">Sign up to be the first to get all of my best business advices for free to your inbox.</p>
                </div>
                <form id="sign-in-form">
                    <div class="form-group form-group-lg">
                        <input type="text" id="emailSigned" name="emailSigned" class="form-control bg-white margin-bottom-5" placeholder="Tap here your e-mail address...">
                    </div>
                    <span class="checkbox-group margin-bottom-10"><input id="acceptRules" name="acceptRules" type="checkbox" checked/><label for="acceptRules" class="checkbox-label">I accept all the <a href="#" data-toggle="modal" data-target="#rulesModal">rules</a></label></span>
                    <button type="submit" class="btn btn-primary btn-filled btn-lg"><i class="icon-before fa fa-plus"></i>Sign In!</button>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Section / End -->

<!-- Section -->
<section class="section padded-vertical-40">
    <div class="container">	
        <!-- Counter Box -->
        <div class="col-md-3 col-sm-6 counter-box">
            <span class="counter-number text-primary"><span class="counter-value">1.5</span>k</span>
            <h5 class="margin-bottom-5 text-light">Happy <strong>subscribers</strong></h5>
            <p class="text-muted margin-bottom-0 font-alt">Class aptent taciti sociosqu ad litora.</p>
            <span class="sep sep-default sep-animated"></span>
        </div>
        <!-- Counter Box -->
        <div class="col-md-3 col-sm-6 counter-box">
            <span class="counter-number text-primary"><span class="counter-value">23</span></span>
            <h5 class="margin-bottom-5 text-light"><strong>Files</strong> in the package</h5>
            <p class="text-muted margin-bottom-0 font-alt">Class aptent taciti sociosqu ad litora.</p>
            <span class="sep sep-default sep-animated"></span>
        </div>
        <!-- Counter Box -->
        <div class="col-md-3 col-sm-6 counter-box">
            <span class="counter-number text-primary"><span class="counter-value">2</span></span>
            <h5 class="margin-bottom-5 text-light"><strong>Months</strong> of my work</h5>
            <p class="text-muted margin-bottom-0 font-alt">Class aptent taciti sociosqu ad litora.</p>
            <span class="sep sep-default sep-animated"></span>
        </div>
        <!-- Counter Box -->
        <div class="col-md-3 col-sm-6 counter-box">
            <span class="counter-number text-primary"><span class="counter-value">60</span></span>
            <h5 class="margin-bottom-5 text-light"><strong>Cups</strong> of coffee</h5>
            <p class="text-muted margin-bottom-0 font-alt">Class aptent taciti sociosqu ad litora.</p>
            <span class="sep sep-default sep-animated"></span>
        </div>
    </div>
</section>
<!-- Section / End -->

<!-- Section -->
<section class="section padded-vertical-70">
    <div class="bg-image bg-image-4"></div>
    <div class="overlay overlay-white editable-alpha" data-alpha="10"></div>
    <div class="container text-center">
      <span class="icon icon-lg margin-bottom-10 animated" data-animation="zoomOut" data-animation-delay="400"><i class="fa fa-play-circle-o text-primary"></i></span>
      <div class="animated" data-animation="zoomOut" data-animation-delay="400">
          <h4 class="text-uppercase margin-bottom-0"><strong>Get Access</strong> to free files!</h4>
          <p class="lead text-muted font-alt">Sign up to be the first to get all of my best business advices for free to your inbox.</p>
          <a href="#" class="btn btn-primary btn-filled">Purchase it!</a><a href="#" class="btn btn-link">Read More</a>
      </div>
    </div>
</section>
<!-- Section / End -->

<!-- Section -->
<section id="latestPosts" class="section padded-vertical-70 bordered-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="section-title margin-bottom-40 text-center">
                    <h4 class="text-uppercase margin-bottom-5"><strong>Trendnig</strong> articles</h4>
                    <span class="sep sep-primary sep-animated"></span>
                    <p class="lead font-alt">Sriracha chia blog, church-key plaid pop-up next level post-ironic PBRB drinking vinegar Helvetica photo booth cornhole art party Etsy.</p>
                </div>
             </div>
        </div>
        <div class="row">
            <div class="owl-carousel owl-multiple-items owl-pagination-center post-carousel" data-items="2" data-items-desktop='[1200,2]' data-items-tablet='[979,1]'>
                <!-- Post  -->
                <div class="post post-horizontal clearfix">
                    <div class="post-image-thumb" data-animation="fadeInLeft" data-animation-delay="400">
                        <img src="images/blog/post01-thumb.jpg" alt="" />
                    </div>
                    <div class="post-content">
                        <div class="post-labels">
                            <a href="#" class="label label-primary animated" data-animation="flipInX" data-animation-delay="400">Webdesign</a>
                            <a href="#" class="label label-primary animated" data-animation="flipInX" data-animation-delay="600">Webdevelopment</a>
                        </div>
                        <h5 class="post-title"><a href="#">Donec molestie in libero nec lobortis nec</a></h5>
                        <div class="post-meta">
                            <span><i class="fa fa-clock-o"></i><a href="#">24.05.2014</a></span>
                            <span><i class="fa fa-comment"></i><a href="#">2 Comments</a></span>
                        </div>
                        <p class="font-alt">Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. </p>
                        <a href="#" class="btn btn-primary btn-xs">Read more</a>
                    </div>
                </div>
                <!-- Post  -->
                <div class="post post-horizontal clearfix">
                    <div class="post-image-thumb animated" data-animation="fadeInLeft" data-animation-delay="600">
                        <img src="images/blog/post02-thumb.jpg" alt="" />
                    </div>
                    <div class="post-content">
                        <div class="post-labels">
                            <span class="label label-primary animated" data-animation="flipInX" data-animation-delay="400">E-marketing</span>
                        </div>
                        <h5 class="post-title"><a href="#">Donec molestie in libero nec lobortis nec</a></h5>
                        <div class="post-meta">
                            <span><i class="fa fa-clock-o"></i><a href="#">24.05.2014</a></span>
                            <span><i class="fa fa-comment"></i><a href="#">2 Comments</a></span>
                        </div>
                        <p class="font-alt">Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. </p>
                        <a href="#" class="btn btn-primary btn-xs">Read more</a>
                    </div>
                </div>
                <!-- Post  -->
                <div class="post post-horizontal clearfix">
                    <div class="post-image-thumb">
                        <img src="images/blog/post03-thumb.jpg" alt="" />
                    </div>
                    <div class="post-content">
                        <div class="post-labels">
                            <span class="label label-primary animated" data-animation="flipInX" data-animation-delay="400">Webdesign</span>
                        </div>
                        <h5 class="post-title"><a href="#">Donec molestie in libero nec lobortis nec</a></h5>
                        <div class="post-meta">
                            <span><i class="fa fa-clock-o"></i><a href="#">24.05.2014</a></span>
                            <span><i class="fa fa-comment"></i><a href="#">2 Comments</a></span>
                        </div>
                        <p class="font-alt">Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. </p>
                        <a href="#" class="btn btn-primary btn-xs">Read more</a>
                    </div>
                </div>
                <!-- Post  -->
                <div class="post post-horizontal clearfix">
                    <div class="post-image-thumb">
                        <img src="images/blog/post04-thumb.jpg" alt="" />
                    </div>
                    <div class="post-content">
                        <div class="post-labels">
                            <span class="label label-primary animated" data-animation="flipInX" data-animation-delay="400">Social Media</span>
                        </div>
                        <h5 class="post-title"><a href="#">Donec molestie in libero nec lobortis nec</a></h5>
                        <div class="post-meta">
                            <span><i class="fa fa-clock-o"></i><a href="#">24.05.2014</a></span>
                            <span><i class="fa fa-comment"></i><a href="#">2 Comments</a></span>
                        </div>
                        <p class="font-alt">Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. </p>
                        <a href="#" class="btn btn-default btn-xs">Read more</a>
                    </div>
                </div>
             </div>
        </div>
    </div>
</section>
<!-- Section / End -->

<!-- Section -->
<section id="gallery" class="section padded-top-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="section-title margin-bottom-40 text-center">
                    <h4 class="text-uppercase margin-bottom-5">My<strong>Gallery</strong></h4>
                    <span class="sep sep-primary sep-animated"></span>
                    <p class="lead font-alt">Cardigan Blue Bottle Shoreditch sartorial. Sriracha chia blog, church-key plaid pop-up next level post-ironic PBRB drinking vinegar Helvetica photo booth cornhole art party Etsy..</p>
                </div>
             </div>
        </div>
    </div>
    <div class="row">
        <!-- Portfolio Item -->
        <a href="images/portfolio/portfolio_01-thumb.jpg" data-target="lightbox" class="gallery-item col-md-3 col-sm-6 animated" data-animation="zoomIn" data-animation-delay="400">
            <div class="gallery-item-image">
                <img src="images/portfolio/portfolio_01-thumb.jpg" alt="" />
                <div class="gallery-item-overlay overlay-black"></div>
            </div>
            <div class="gallery-item-title text-center">
                <h6 class="margin-bottom-0">Chockolate &amp; Nails</h6>
                <span class="text-uppercase text-muted text-xs">Webdesign / E-commerce</span>
            </div>
        </a>
        <!-- Portfolio Item -->
        <a href="images/portfolio/portfolio_02-thumb.jpg" data-target="lightbox" class="gallery-item col-md-3 col-sm-6 animated" data-animation="zoomIn" data-animation-delay="800">
            <div class="gallery-item-image">
                <img src="images/portfolio/portfolio_02-thumb.jpg" alt="" />
                <div class="gallery-item-overlay overlay-black"></div>
            </div>
            <div class="gallery-item-title text-center">
                <h6 class="margin-bottom-0">3 Chairs</h6>
                <span class="text-uppercase text-muted text-xs">Social Media</span>
            </div>
        </a>
        <!-- Portfolio Item -->
        <a href="images/portfolio/portfolio_03-thumb.jpg" data-target="lightbox" class="gallery-item col-md-3 col-sm-6 animated" data-animation="zoomIn" data-animation-delay="600">
            <div class="gallery-item-image">
                <img src="images/portfolio/portfolio_03-thumb.jpg" alt="" />
                <div class="gallery-item-overlay overlay-black"></div>
            </div>
            <div class="gallery-item-title text-center">
                <h6 class="margin-bottom-0">Designing</h6>
                <span class="text-uppercase text-muted text-xs">Webdesign / E-commerce</span>
            </div>
        </a>
        <!-- Portfolio Item -->
        <a href="images/portfolio/portfolio_04-thumb.jpg" data-target="lightbox" class="gallery-item col-md-3 col-sm-6 animated" data-animation="zoomIn" data-animation-delay="1000">
            <div class="gallery-item-image">
                <img src="images/portfolio/portfolio_04-thumb.jpg" alt="" />
                <div class="gallery-item-overlay overlay-black"></div>
            </div>
            <div class="gallery-item-title text-center">
                <h6 class="margin-bottom-0">Inspirations</h6>
                <span class="text-uppercase text-muted text-xs">Photoshop</span>
            </div>
        </a>
    </div>
</section>
<!-- Section / End -->

<!-- Section -->
<section class="section padded-vertical-70">
    <div class="bg-image bg-image-5"></div>
    <div class="overlay overlay-white editable-alpha" data-alpha="40"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Twitter -->
                <div class="tweet tweet-horizontal"></div>
            </div>
        </div>
    </div>
</section>
<!-- Section / End -->

<!-- Section -->
<section id="contact" class="bg-black dark">
  
  <!-- Google Map -->
  <div id="google-map-dark" class="google-map"></div>
  
  <div class="container padded-vertical-70">
      <div class="row">
          <div class="col-md-6 animated fade-in-left">
          	  <h4 class="text-uppercase">Say <strong>Hello</strong>!</h4>
              <!-- Contact Form -->
              <form id="contact-form" class="clearfix">
              		<div class="row">
              			<div class="col-md-6">
		                  	<div class="form-group">
		                    	<input type="text" class="form-control" id="name" name="name" placeholder="Name">
		                    </div>
	                    </div>
	                    <div class="col-md-6">
		                    <div class="form-group">
		                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
		                    </div>
	                    </div>
                    </div>
                    <div class="form-group">
                    	<textarea class="form-control" id="message" name="message" rows="3"  placeholder="Message"></textarea>
                    </div>
                    <div class="form-group clearfix">
                    	<button type="submit" class="btn btn-primary btn-filled btn-sm pull-right">Send it!</button>
                    </div>
              </form>
          </div>
          <div class="col-md-6 animated fade-in-right">
          	  <h4 class="text-uppercase"><strong>Contact</strong> Me</h4>
              <!-- List with Icons -->
              <ul class="list-unstyled list-unstyled-icons">
                <li><i class="text-muted inline-icon fa fa-map-marker"></i><strong>Address:</strong> <span class="text-muted">1111-A Nowhere Lane, Outta Sight, State 90378, USA</span></li>
                <li><i class="text-muted inline-icon fa fa-comment"></i><strong>E-mail:</strong> <span class="text-muted"><a href="#">johnathandoe@suelo.pl</a></span></li>
                <li><i class="text-muted inline-icon fa fa-phone"></i><strong>Phone:</strong> <span class="text-muted">+0(31)6 89764536</span></li>
              </ul>
          </div>
      </div>
  </div>
  
  <!-- Copyright -->
  <div class="copyright bordered-top">
  	<div class="padded-vertical-30 text-center">
    	<div class="text-muted margin-bottom-10">Copyright <strong>Trener - Multipurpose Coaching / Trainer Bootstrap Theme</strong> 2014&copy;</div>
        <!-- Social Icons -->
        <a href="#" class="icon icon-circle icon-filled icon-xs icon-facebook"><i class="fa fa-facebook"></i></a>
        <a href="#" class="icon icon-circle icon-filled icon-xs icon-twitter"><i class="fa fa-twitter"></i></a>
        <a href="#" class="icon icon-circle icon-filled icon-xs icon-google-plus"><i class="fa fa-google-plus"></i></a>
    </div>
  </div>
  
</section>
<!-- Section / End -->

<!-- Alert Modal -->
<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title text-center" id="alertModalLabel"></h4>
      </div>
      <div class="modal-body text-center">
        <div class="modal-alert-content margin-bottom-20"></div>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="icon-before fa fa-times"></i>Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Alert Modal / End -->

<!-- Rules Modal -->
<div class="modal fade" id="rulesModal" tabindex="-1" role="dialog" aria-labelledby="rulesModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="rulesModalLabel">Trener - Sign In Demo - Rules </h4>
      </div>
      <div class="modal-body">
        <ol>
            <li>This form will send you a <strong>demo message</strong> on your e-mail address.</li>
            <li>Your e-mail address <strong>won't be used for any purpose</strong> other than the one specified in the first point.</li>
        </ol>
        <p class="margin-bottom-0">If you accept those rules please fill the form and press <strong>"Sign In"</strong> button.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Rules Modal / End -->

<!-- Page Curtain -->
<div id="page-curtain" class="curtain-white"></div>

<!-- Scripts -->
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/page-loader.js"></script>
<script type="text/javascript" src="js/jquery.appear.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="js/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="js/waypoints.min.js"></script>
<script type="text/javascript" src="js/jquery.localscroll-1.2.7-min.js"></script>
<script type="text/javascript" src="js/jquery.scrollTo-1.4.3.1-min.js"></script>
<script type="text/javascript" src="js/jquery.tweet.js"></script>
<script type="text/javascript" src="js/jquery.parallax.min.js"></script>
<script type="text/javascript" src="js/jquery.fitvids.js"></script>

<!-- Google Map -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript" src="js/GoogleMap-dark.js"></script>

<!-- Custom Script -->
<script type="text/javascript" src="js/custom.js"></script>

<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->
<!--
<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
-->

</body>

</html>
