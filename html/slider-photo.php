<!DOCTYPE html>
<html lang="en">
<head>

<!-- Meta -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<!-- Title -->
<title>Trener - Responsive HTML/CSS Template</title>

<!-- Favicons -->
<link rel="shortcut icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/favicon_60x60.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/favicon_76x76.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/favicon_120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/favicon_152x152.png">

<!-- Google Web Fonts -->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

<!-- Stylesheets -->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/animations.css" />
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="css/owl.transitions.css" />
<link rel="stylesheet" type="text/css" href="css/owl.theme.css" />
<link rel="stylesheet" type="text/css" href="colors/red.css" id="colors" />
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />

</head>

<body class="sticky-nav">

<!-- Loader -->
<div id="page-loader" class="loader-white"></div>

<!-- Header -->
<header id="header" class="header header-white wide clearfix">

    <div class="row">
        <div class="col-xs-12">
            <div class="nav-bar clearfix">
                <!-- Logo -->
                <div class="logo pull-left">
                    <a href="index.html"><img src="images/logo-black.png" alt="Trener" /></a>
                </div>
                <!-- Mobile Nav Toggle -->
                <a href="" class="mobile-menu-toggle pull-right"></a>
                <!-- Social Icons -->
                <div class="social-icons text-muted pull-right margin-left-20 hidden-sm hidden-xs">
                   <a href="#" class="icon icon-xs icon-facebook"><i class="fa fa-facebook"></i></a>
           		   <a href="#" class="icon icon-xs icon-twitter"><i class="fa fa-twitter"></i></a>
          		   <a href="#" class="icon icon-xs icon-google-plus"><i class="fa fa-google-plus"></i></a> 
                </div> 
                <!-- Main Navigation-->
                <ul class="menu nav nav-main text-uppercase pull-right margin-left-20 dropdown-dark">
	                    <li class="has-dropdown">
	                        <a href="#">Home</a>
	                        <div class="nav-main-dropdown">
	                            <ul>
	                                <li><a href="index.html">Trainer</a></li>
	                                <li><a href="index-marketer.html">Marketer</a></li>
	                                <li><a href="index-professional.html">Professional</a></li>
	                            </ul>
	                        </div>
	                    </li>
	                    <li class="has-dropdown">
	                        <a href="#">Features</a>
	                        <div class="nav-main-dropdown">
	                            <ul>
	                                <li class="has-dropdown">
	                                    <a href="#"><i class="icon-before fa fa-star text-muted"></i>Headers</a>
	                                    <ul>
	                                        <li><a href="headers-light.html">Headers Light</a></li>    
	                                        <li><a href="headers-dark.html">Headers Dark</a></li>   
	                                        <li><a href="headers-light-wide.html">Headers Light &amp; Wide</a></li>    
	                                        <li><a href="headers-dark-wide.html">Headers Dark &amp; Wide</a></li>     
	                                    </ul>
	                                </li>
	                                <li class="has-dropdown">
	                                    <a href="#"><i class="icon-before fa fa-desktop text-muted"></i>Sliders</a>
	                                    <ul>
	                                        <li><a href="slider-default.html">Default Slider</a></li>    
	                                        <li><a href="slider-text.html">Text Slider</a></li>      
	                                        <li><a href="slider-photo.html">Photo Slider</a></li>
	                                        <li class="has-dropdown">
	                                            <a href="#">Slider Sizes</a>
	                                            <ul>
	                                                <li><a href="slider-size-default.html">Deafult</a></li>
	                                                <li><a href="slider-size-small.html">Small</a></li>
	                                                <li><a href="slider-size-fullheight.html">Fullheight</a></li>
	                                            </ul>
	                                        </li>  
	                                    </ul>
	                                </li>
	                                <li class="has-dropdown">
	                                    <a href="#"><i class="icon-before fa fa-road text-muted"></i>Footers</a>
	                                    <ul>
	                                        <li><a href="footers-light.html">Footers Light</a></li>    
	                                        <li><a href="footers-dark.html">Footers Dark</a></li>      
	                                    </ul>
	                                </li>
	                                <li><a href="animations.html"><i class="icon-before fa fa-bolt text-muted"></i>Animations</a></li>
	                            </ul>
	                        </div>
	                    </li>
	                    <li class="has-dropdown-wide">
	                         <a href="#">Templates</a>
	                         <div class="nav-main-dropdown">
	                            <div class="row">
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="about-1.html">About 1</a></li>
	                                        <li><a href="about-2.html">About 2</a></li>
	                                        <li><a href="services-1.html">Services 1</a></li>
	                                        <li><a href="services-2.html">Services 2</a></li>
	                                        <li><a href="services-3.html">Services 3</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="portfolio-1.html">Portfolio 1</a></li>
	                                        <li><a href="portfolio-2.html">Portfolio 2</a></li>
	                                        <li><a href="blog-horizontal.html">Blog - Horizontal</a></li>
	                                        <li><a href="blog-vertical.html">Blog - Vertical</a></li>
	                                        <li><a href="blog-post.html">Blog Single Post</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="clients.html">Clients</a></li>
	                                        <li><a href="team.html">Team</a></li>
	                                        <li><a href="testimonial.html">Testimonial</a></li>
	                                        <li><a href="faq.html">FAQ</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="contact.html">Contact</a></li>
	                                        <li><a href="404.html">404 Error</a></li>
	                                        <li><a href="page-fullwidth.html">Fullwidth page</a></li>
	                                        <li><a href="page-with-sidebar.html">Page with sidebar</a></li>
	                                    </ul>
	                                </div>
	                            </div>
	                         </div>
	                    </li>
	                    <li class="has-dropdown-wide">
	                        <a href="#">Elements</a>
	                         <div class="nav-main-dropdown">
	                            <div class="row">
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="typography.html">Typography</a></li>
	                                        <li><a href="tables.html">Tables</a></li>
	                                        <li><a href="forms.html">Forms</a></li>
	                                        <li><a href="buttons-labels.html">Buttons &amp; Labels</a></li>
	                                        <li><a href="alerts-wells.html">Alerts &amp; Wells</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="progress-bars.html">Progress Bars</a></li>
	                                        <li><a href="tooltips-popovers-modals.html">Tooltips, Popovers &amp; Modals</a></li>
	                                        <li><a href="tabs-accordions.html">Tabs &amp; Accordions</a></li>
	                                        <li><a href="carousels.html">Carousels</a></li>
                                            <li><a href="maps.html">Maps</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="icons.html">Icons</a></li>
	                                        <li><a href="icon-boxes.html">Icon Boxes</a></li>
	                                        <li><a href="counters.html">Counters</a></li>
	                                        <li><a href="testimonials.html">Testimonials</a></li>
	                                        <li><a href="pricing-tables.html">Pricing Tables</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="title-bars-default.html">Title Bars - Default</a></li>
	                                        <li><a href="title-bars-big.html">Title Bars - Big</a></li>
	                                        <li><a href="title-bars-breadcrumb.html">Title Bars - Breadcrumb Only</a></li>
	                                        <li><a href="sections.html">Sections</a></li>
	                                        <li><a href="images.html">Images</a></li>
	                                    </ul>
	                                </div>
	                            </div>
	                         </div>
	                    </li>
	                    <li><a href="#">Buy it!</a></li>
	                </ul>
            </div>
        </div>
    </div>
    <div class="mobile-menu text-uppercase"></div>

</header>
<!-- Header / End -->

<!-- Home -->
<section id="home" class="home-default">
    
    <div class="owl-carousel home-full-carousel dark" data-single-item="true" data-auto-play="4500">
        <!-- Slide slide -->
        <div class="photo-slide">
            <div class="bg-image bg-image-1"></div>
            <div class="overlay overlay-black editable-alpha" data-alpha="70"></div>  
            <div class="container vertical-center">
                <div class="slide-txt text-center">
                    <span class="sep sep-primary"></span>
                    <h4 class="text-uppercase"><strong>Get</strong> some great <strong>Coaching materials</strong> for <strong>free</strong>.</h4>
                    <a href="#" class="btn btn-primary">Sign in!</a><a href="#" class="btn btn-link">Read More</a>
                </div>
             </div>
        </div>
        <!-- Slide slide -->
        <div class="photo-slide">
            <div class="bg-image bg-image-2"></div>
            <div class="overlay overlay-black editable-alpha" data-alpha="70"></div>  
            <div class="container vertical-center">
                <div class="slide-txt text-center">
                    <span class="sep sep-primary"></span>
                    <h4 class="text-uppercase">Do you <strong>like</strong> this <strong>awesome</strong> theme? <strong>Buy it</strong> right now!</h4>
                    <a href="#" class="btn btn-primary">Sign in!</a><a href="#" class="btn btn-link">Read More</a>
                </div>
             </div>
        </div>
        <!-- Slide slide -->
        <div class="photo-slide">
            <div class="bg-image bg-image-3"></div>
            <div class="overlay overlay-black editable-alpha" data-alpha="70"></div>  
            <div class="container vertical-center">
                <div class="slide-txt text-center">
                    <span class="sep sep-primary"></span>
                    <h4 class="text-uppercase"><strong>More</strong> great stuff<strong> Comming Soon!</strong></h4>
                    <a href="#" class="btn btn-primary">Sign in!</a><a href="#" class="btn btn-link">Read More</a>
                </div>
             </div>
        </div>
    </div>
    
</section>
<!-- Home / End -->

<!-- Footer -->
<footer id="footer" class="bg-black dark">
  
  <div class="container padded-vertical-40">
      <div class="row">
          <div class="col-md-2 col-sm-6">
          	  <h5 class="text-light"><strong>Navigation</strong></h5>
              <!-- Navigation list -->
              <ul class="list-navigation margin-bottom-20">
              	<li><a href="#">Home Page</a></li>
                <li><a href="#">Features</a></li>
                <li><a href="#">Templates</a></li>
                <li><a href="#">Elements</a></li>
                <li><a href="#">Documentation</a></li>
                <li><a href="#">Buy it!</a></li>
              </ul>
          </div>
          <div class="col-md-4 col-sm-6">
          	  <h5 class="text-light">Say <strong>Hello</strong>!</h5>
              <!-- Contact Form -->
              <form id="contact-form" class="margin-bottom-20">
              		<div class="row">
              			<div class="col-md-6">
		                  	<div class="form-group">
		                    	<input type="text" class="form-control" id="name" name="name" placeholder="Name">
		                    </div>
	                    </div>
	                    <div class="col-md-6">
		                    <div class="form-group">
		                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
		                    </div>
	                    </div>
                    </div>
                    <div class="form-group">
                    	<textarea class="form-control" id="message" name="message" rows="3"  placeholder="Message"></textarea>
                    </div>
                    <div class="form-group clearfix">
						<button type="submit" class="btn btn-primary btn-filled btn-sm pull-right">Send it!</button>
                    </div>
              </form>
          </div>
          <div class="col-md-4 col-sm-6">
          	  <h5 class="text-light"><strong>Contact</strong> Me</h5>
              <!-- List with Icons -->
              <ul class="list-unstyled list-unstyled-icons margin-bototm-20">
                <li><i class="text-muted inline-icon fa fa-map-marker"></i><strong>Address:</strong> <span class="text-muted">1111-A Nowhere Lane, Outta Sight, State 90378, USA</span></li>
                <li><i class="text-muted inline-icon fa fa-comment"></i><strong>E-mail:</strong> <span class="text-muted"><a href="#">johnathandoe@suelo.pl</a></span></li>
                <li><i class="text-muted inline-icon fa fa-phone"></i><strong>Phone:</strong> <span class="text-muted">+0(31)6 89764536</span></li>
              </ul>
          </div>
          <div class="col-md-2 col-sm-6">
          	  <h5 class="text-light"><strong>Social</strong> Media</h5>
              <!-- Social Icons -->
              <div class="margin-bototm-20">
                  <a href="#" class="icon icon-circle icon-filled icon-xs icon-facebook"><i class="fa fa-facebook"></i></a>
                  <a href="#" class="icon icon-circle icon-filled icon-xs icon-twitter"><i class="fa fa-twitter"></i></a>
                  <a href="#" class="icon icon-circle icon-filled icon-xs icon-google-plus"><i class="fa fa-google-plus"></i></a>
              </div>
          </div>
      </div>
  </div>
  
  <!-- Copyright -->
  <div class="copyright bordered-top">
  	<div class="container padded-vertical-30">
    	<span class="text-muted">Copyright <strong>Trener - Multipurpose Coaching / Trainer Bootstrap Theme</strong> 2014&copy;</span>
        <ul class="list-inline pull-right text-muted margin-bottom-0">
        	<li><a href="#">Home page</a></li>
            <li><a href="#">Documentation</a></li>
            <li><a href="#">Buy it!</a></li>
        </ul>
    </div>
  </div>
  
</footer>
<!-- Footer / End -->

<!-- Alert Modal -->
<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title text-center" id="alertModalLabel"></h4>
      </div>
      <div class="modal-body text-center">
        <div class="modal-alert-content margin-bottom-20"></div>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="icon-before fa fa-times"></i>Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Alert Modal / End -->

<!-- Page Curtain -->
<div id="page-curtain" class="curtain-white"></div>

<!-- Scripts -->
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/page-loader.js"></script>
<script type="text/javascript" src="js/jquery.appear.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="js/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="js/waypoints.min.js"></script>
<script type="text/javascript" src="js/jquery.localscroll-1.2.7-min.js"></script>
<script type="text/javascript" src="js/jquery.scrollTo-1.4.3.1-min.js"></script>
<script type="text/javascript" src="js/jquery.tweet.js"></script>
<script type="text/javascript" src="js/jquery.parallax.min.js"></script>
<script type="text/javascript" src="js/jquery.fitvids.js"></script>

<!-- Custom Script -->
<script type="text/javascript" src="js/custom.js"></script>

<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->
<!--
<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
-->

</body>

</html>
