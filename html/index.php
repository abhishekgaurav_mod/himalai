<?php include('inc/_metaHeader.php') ?>

<?php include('inc/_header.php') ?>

<!-- Home -->
<section id="home" class="home-default">
	
    <div class="owl-slideshow bg-carousel home-full-carousel" data-auto-play="4500">
    	<div class="bg-image bg-image-1"></div>
        <div class="bg-image bg-image-2"></div>
        <div class="bg-image bg-image-3"></div>
    </div>
    <div class="overlay overlay-black editable-alpha" data-alpha="70"></div>  
    <div class="container dark vertical-center">
        <div class="row">
            <div class="col-md-6 col-lg-5 vertical-center">
            	<div class="owl-carousel margin-bottom-20 animated" data-animation="zoomIn" data-single-item="true" data-auto-play="3500" data-pagination="true"> 
                    <div>
                        <span class="sep sep-primary"></span>
                        <h4 class="text-uppercase"><strong>Get</strong> some great <strong>Coaching materials</strong> for <strong>free</strong>.</h4>
                        <a href="#" class="btn btn-primary">Sign in!</a><a href="#" class="btn btn-link">Read More</a>
                    </div>
                    <div>
                        <span class="sep sep-primary"></span>
                        <h4 class="text-uppercase">Do you <strong>like</strong> this <strong>awesome</strong> theme? <strong>Buy it</strong> right now!</h4>
                        <a href="#" class="btn btn-primary">Purchase it!</a><a href="#" class="btn btn-link">Read More</a>
                    </div>
                    <div>
                        <span class="sep sep-primary"></span>
                        <h4 class="text-uppercase"><strong>Get</strong> some great <strong>Coaching materials</strong> for <strong>free</strong></h4>
                        <a href="#" class="btn btn-primary">Purchase it!</a><a href="#" class="btn btn-link">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-5 col-lg-offset-2 vertical-center">
                <div class="sign-in-box dark animated" data-animation="flipInX">
                    <div class="sign-in-head bg-black-transparent">
                        <span class="pull-left icon icon-lg margin-bottom-5"><i class="fa fa-download text-primary"></i></span>
                        <div class="left-space-lg">
                          <h5 class="text-uppercase margin-bottom-0"><strong>Get Access</strong> to free files!</h5>
                          <p class="lead text-muted font-alt margin-bottom-0">Sign up to be the first to get all of my best business advices for free to your inbox.</p>
                        </div>
                    </div>
                    <div class="sign-in-content bg-primary">
                    	<form id="sign-in-form">
                            <div class="form-group form-group-lg margin-bottom-0">
                                <input type="text" id="emailSigned" name="emailSigned" class="form-control bg-white margin-bottom-5" placeholder="Tap here your e-mail address...">
                            </div>
                            <div class="form-bottom-bar clearfix">
                              <span class="checkbox-group pull-left"><input id="acceptRules" name="acceptRules" type="checkbox" /><label for="acceptRules" class="checkbox-label">I accept all the <a href="#" data-toggle="modal" data-target="#rulesModal">rules</a>.</label></span>
                              <button type="submit" class="btn btn-filled btn-primary-dark pull-right"><i class="icon-before fa fa-plus"></i>Sign In!</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</section>
<!-- Home / End -->

<!-- Content -->
<div id="content">

	<!-- Section -->
    <section class="section padded-vertical-70 container">
        <div class="row">
            <div class="col-lg-10">
            	<div class="section-title margin-bottom-40">
                    <h4 class="text-uppercase margin-bottom-0"><strong>What’s</strong> going on?</h4>
                    <span class="sep sep-primary sep-animated"></span>
                    <p class="lead font-alt">Cardigan Blue Bottle Shoreditch sartorial. Sriracha chia blog, church-key plaid pop-up next level post-ironic PBRB drinking vinegar Helvetica photo booth cornhole art party Etsy.</p>
                </div>
             </div>
        </div>
        <div class="row">
        	<!-- Icon Box -->
        	<div class="col-md-3 col-sm-6 icon-box icon-box-hover">
                <span class="icon icon-sm icon-circle icon-primary margin-bottom-20 animated" data-animation="bounceIn" data-animation-delay="400"><i class="fa fa-book"></i></span>
                <h5 class="margin-bottom-0">5 Premium <strong>Ebooks</strong></h5>
                <p class="text-muted">Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
                <a href="#" class="underline-link text-uppercase text-primary">Read more</a>
            </div>
            <!-- Icon Box -->
        	<div class="col-md-3 col-sm-6 icon-box icon-box-hover">
            	<span class="icon icon-sm icon-circle icon-primary margin-bottom-20 animated" data-animation="bounceIn" data-animation-delay="600"><i class="fa fa-video-camera"></i></span>
            	<h5 class="margin-bottom-0">6 <strong>Video</strong> Courses</h5>
                <p class="text-muted">Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
                <a href="#" class="underline-link text-uppercase text-primary">Read more</a>
            </div>
            <!-- Icon Box -->
        	<div class="col-md-3 col-sm-6 icon-box icon-box-hover">
            	<span class="icon icon-sm icon-circle icon-primary margin-bottom-20 animated" data-animation="bounceIn" data-animation-delay="800"><i class="fa fa-camera"></i></span>
            	<h5 class="margin-bottom-0">5 <strong>PSD Files</strong></h5>
                <p class="text-muted">Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
                <a href="#" class="underline-link text-uppercase text-primary">Read more</a>
            </div>
            <!-- Icon Box -->
        	<div class="col-md-3 col-sm-6 icon-box icon-box-hover">
            	<span class="icon icon-sm icon-circle icon-filled icon-primary margin-bottom-20 animated" data-animation="bounceIn" data-animation-delay="1000"><i class="fa fa-heart"></i></span>
            	<h5 class="margin-bottom-0">+<strong>Much more</strong> in future!</h5>
                <p class="text-muted">Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
                <a href="#" class="underline-link text-uppercase text-primary">Read more</a>
            </div>
        </div>
    </section>
  
    <!-- Section -->
    <section class="section padded-vertical-70">
    	<div class="bg-image bg-image-4"></div>
        <div class="overlay overlay-white editable-alpha" data-alpha="10"></div>
        <div class="container">
          <span class="pull-left icon icon-lg margin-bottom-0 animated" data-animation="bounceIn" data-animation-delay="400"><i class="fa fa-play-circle-o text-primary"></i></span>
          <div class="left-space-lg animated" data-animation="fadeInRight" data-animation-delay="400">
              <h4 class="text-uppercase margin-bottom-0"><strong>Get Access</strong> to free files!</h4>
              <p class="lead text-muted font-alt">Sign up to be the first to get all of my best business advices for free to your inbox.</p>
              <a href="#" class="btn btn-primary btn-filled">Purchase it!</a><a href="#" class="btn btn-link">Read More</a>
          </div>
        </div>
    </section>
    
    <!-- Section -->
    <section class="section padded-vertical-70 container">
        <div class="row">
            <div class="col-lg-10">
            	<div class="section-title margin-bottom-40">
                    <h4 class="text-uppercase margin-bottom-5"><strong>Trendnig</strong> articles</h4>
                    <span class="sep sep-primary sep-animated"></span>
                    <p class="lead font-alt">Sriracha chia blog, church-key plaid pop-up next level post-ironic PBRB drinking vinegar Helvetica photo booth cornhole art party Etsy.</p>
                </div>
             </div>
        </div>
        <div class="row">
        	<!-- Post  -->
            <div class="post post-horizontal col-md-6 clearfix">
                <div class="post-image-thumb animated" data-animation="fadeInLeft" data-animation-delay="400">
                    <img src="images/blog/post01-thumb.jpg" alt="" />
                </div>
                <div class="post-content">
                    <div class="post-labels">
                        <a href="#" class="label label-primary animated" data-animation="flipInX" data-animation-delay="400">Webdesign</a>
                        <a href="#" class="label label-primary animated" data-animation="flipInX" data-animation-delay="600">Webdevelopment</a>
                    </div>
                    <h5 class="post-title"><a href="#">Donec molestie in libero nec lobortis nec</a></h5>
                    <div class="post-meta">
                        <span><i class="fa fa-clock-o"></i><a href="#">24.05.2014</a></span>
                        <span><i class="fa fa-comment"></i><a href="#">2 Comments</a></span>
                    </div>
                    <p class="font-alt">Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. </p>
                    <a href="#" class="btn btn-primary btn-xs">Read more</a>
                </div>
            </div>
            <!-- Post  -->
            <div class="post post-horizontal col-md-6 clearfix">
                <div class="post-image-thumb animated" data-animation="fadeInLeft" data-animation-delay="600">
                    <img src="images/blog/post02-thumb.jpg" alt="" />
                </div>
                <div class="post-content">
                    <div class="post-labels">
                        <span class="label label-primary animated" data-animation="flipInX" data-animation-delay="400">E-marketing</span>
                    </div>
                    <h5 class="post-title"><a href="#">Donec molestie in libero nec lobortis nec</a></h5>
                    <div class="post-meta">
                        <span><i class="fa fa-clock-o"></i><a href="#">24.05.2014</a></span>
                        <span><i class="fa fa-comment"></i><a href="#">2 Comments</a></span>
                    </div>
                    <p class="font-alt">Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. </p>
                    <a href="#" class="btn btn-primary btn-xs">Read more</a>
                </div>
            </div>
            <!-- Post  -->
            <div class="post post-horizontal col-md-6 clearfix">
                <div class="post-image-thumb animated" data-animation="fadeInLeft" data-animation-delay="800">
                    <img src="images/blog/post03-thumb.jpg" alt="" />
                </div>
                <div class="post-content">
                    <div class="post-labels">
                        <span class="label label-primary animated" data-animation="flipInX" data-animation-delay="400">Webdesign</span>
                    </div>
                    <h5 class="post-title"><a href="#">Donec molestie in libero nec lobortis nec</a></h5>
                    <div class="post-meta">
                        <span><i class="fa fa-clock-o"></i><a href="#">24.05.2014</a></span>
                        <span><i class="fa fa-comment"></i><a href="#">2 Comments</a></span>
                    </div>
                    <p class="font-alt">Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. </p>
                    <a href="#" class="btn btn-primary btn-xs">Read more</a>
                </div>
            </div>
            <!-- Post  -->
            <div class="post post-horizontal col-md-6 clearfix">
                <div class="post-image-thumb animated" data-animation="fadeInLeft" data-animation-delay="1000">
                    <img src="images/blog/post04-thumb.jpg" alt="" />
                </div>
                <div class="post-content">
                    <div class="post-labels">
                        <span class="label label-primary animated" data-animation="flipInX" data-animation-delay="400">Social Media</span>
                    </div>
                    <h5 class="post-title"><a href="#">Donec molestie in libero nec lobortis nec</a></h5>
                    <div class="post-meta">
                        <span><i class="fa fa-clock-o"></i><a href="#">24.05.2014</a></span>
                        <span><i class="fa fa-comment"></i><a href="#">2 Comments</a></span>
                    </div>
                    <p class="font-alt">Sed volutpat purus non odio maximus, eget ultrices mauris rutrum. Phasellus non diam in neque interdum gravida. </p>
                    <a href="#" class="btn btn-primary btn-xs">Read more</a>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Section -->
    <section class="section padded-vertical-70 dark">
    	<div class="bg-image parallax bg-image-3"></div>
        <div class="overlay overlay-black editable-alpha" data-alpha="70"></div>
        <div class="container">
        	<div class="row">
            	<div class="col-md-12">
                    <!-- Twitter -->
                	<div class="tweet tweet-horizontal"></div>
                </div>
            </div>
        </div>
    </section>
    
</div>
<!-- Content / End -->

<?php include('inc/_footer.php') ?>

<?php include('inc/_footerScript.php') ?>

