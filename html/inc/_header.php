<!-- Loader -->
<div id="page-loader" class="loader-white"></div>
<!-- Header -->
<header id="header" class="header header-white wide clearfix">

    <div class="row">
        <div class="col-xs-12">
            <div class="nav-bar clearfix">
                <!-- Logo -->
                <div class="logo pull-left">
                    <a href="index.php"><img src="images/logo-black.png" alt="Trener" /></a>
                </div>
                <!-- Mobile Nav Toggle -->
                <a href="" class="mobile-menu-toggle pull-right"></a>
                <!-- Social Icons -->
                <div class="social-icons text-muted pull-right margin-left-20 hidden-sm hidden-xs">
                   <a href="#" class="icon icon-xs icon-facebook"><i class="fa fa-facebook"></i></a>
           		   <a href="#" class="icon icon-xs icon-twitter"><i class="fa fa-twitter"></i></a>
          		   <a href="#" class="icon icon-xs icon-google-plus"><i class="fa fa-google-plus"></i></a> 
                </div> 
                <!-- Main Navigation-->
                <ul class="menu nav nav-main text-uppercase pull-right margin-left-20 dropdown-dark">
	                    <li class="has-dropdown">
	                        <a href="#">Home</a>
	                        <div class="nav-main-dropdown">
	                            <ul>
	                                <li><a href="index.php">Trainer</a></li>
	                                <li><a href="index-marketer.php">Marketer</a></li>
	                                <li><a href="index-professional.php">Professional</a></li>
	                            </ul>
	                        </div>
	                    </li>
	                    <li class="has-dropdown">
	                        <a href="#">Features</a>
	                        <div class="nav-main-dropdown">
	                            <ul>
	                                <li class="has-dropdown">
	                                    <a href="#"><i class="icon-before fa fa-star text-muted"></i>Headers</a>
	                                    <ul>
	                                        <li><a href="headers-light.php">Headers Light</a></li>    
	                                        <li><a href="headers-dark.php">Headers Dark</a></li>   
	                                        <li><a href="headers-light-wide.php">Headers Light &amp; Wide</a></li>    
	                                        <li><a href="headers-dark-wide.php">Headers Dark &amp; Wide</a></li>     
	                                    </ul>
	                                </li>
	                                <li class="has-dropdown">
	                                    <a href="#"><i class="icon-before fa fa-desktop text-muted"></i>Sliders</a>
	                                    <ul>
	                                        <li><a href="slider-default.php">Default Slider</a></li>    
	                                        <li><a href="slider-text.php">Text Slider</a></li>      
	                                        <li><a href="slider-photo.php">Photo Slider</a></li>
	                                        <li class="has-dropdown">
	                                            <a href="#">Slider Sizes</a>
	                                            <ul>
	                                                <li><a href="slider-size-default.php">Deafult</a></li>
	                                                <li><a href="slider-size-small.php">Small</a></li>
	                                                <li><a href="slider-size-fullheight.php">Fullheight</a></li>
	                                            </ul>
	                                        </li>  
	                                    </ul>
	                                </li>
	                                <li class="has-dropdown">
	                                    <a href="#"><i class="icon-before fa fa-road text-muted"></i>Footers</a>
	                                    <ul>
	                                        <li><a href="footers-light.php">Footers Light</a></li>    
	                                        <li><a href="footers-dark.php">Footers Dark</a></li>      
	                                    </ul>
	                                </li>
	                                <li><a href="animations.php"><i class="icon-before fa fa-bolt text-muted"></i>Animations</a></li>
	                            </ul>
	                        </div>
	                    </li>
	                    <li class="has-dropdown-wide">
	                         <a href="#">Templates</a>
	                         <div class="nav-main-dropdown">
	                            <div class="row">
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="about-1.php">About 1</a></li>
	                                        <li><a href="about-2.php">About 2</a></li>
	                                        <li><a href="services-1.php">Services 1</a></li>
	                                        <li><a href="services-2.php">Services 2</a></li>
	                                        <li><a href="services-3.php">Services 3</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="portfolio-1.php">Portfolio 1</a></li>
	                                        <li><a href="portfolio-2.php">Portfolio 2</a></li>
	                                        <li><a href="blog-horizontal.php">Blog - Horizontal</a></li>
	                                        <li><a href="blog-vertical.php">Blog - Vertical</a></li>
	                                        <li><a href="blog-post.php">Blog Single Post</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="clients.php">Clients</a></li>
	                                        <li><a href="team.php">Team</a></li>
	                                        <li><a href="testimonial.php">Testimonial</a></li>
	                                        <li><a href="faq.php">FAQ</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="contact.php">Contact</a></li>
	                                        <li><a href="404.php">404 Error</a></li>
	                                        <li><a href="page-fullwidth.php">Fullwidth page</a></li>
	                                        <li><a href="page-with-sidebar.php">Page with sidebar</a></li>
	                                    </ul>
	                                </div>
	                            </div>
	                         </div>
	                    </li>
	                    <li class="has-dropdown-wide">
	                        <a href="#">Elements</a>
	                         <div class="nav-main-dropdown">
	                            <div class="row">
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="typography.php">Typography</a></li>
	                                        <li><a href="tables.php">Tables</a></li>
	                                        <li><a href="forms.php">Forms</a></li>
	                                        <li><a href="buttons-labels.php">Buttons &amp; Labels</a></li>
	                                        <li><a href="alerts-wells.php">Alerts &amp; Wells</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="progress-bars.php">Progress Bars</a></li>
	                                        <li><a href="tooltips-popovers-modals.php">Tooltips, Popovers &amp; Modals</a></li>
	                                        <li><a href="tabs-accordions.php">Tabs &amp; Accordions</a></li>
	                                        <li><a href="carousels.php">Carousels</a></li>
                                            <li><a href="maps.php">Maps</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="icons.php">Icons</a></li>
	                                        <li><a href="icon-boxes.php">Icon Boxes</a></li>
	                                        <li><a href="counters.php">Counters</a></li>
	                                        <li><a href="testimonials.php">Testimonials</a></li>
	                                        <li><a href="pricing-tables.php">Pricing Tables</a></li>
	                                    </ul>
	                                </div>
	                                <div class="col-md-3">
	                                    <ul>
	                                        <li><a href="title-bars-default.php">Title Bars - Default</a></li>
	                                        <li><a href="title-bars-big.php">Title Bars - Big</a></li>
	                                        <li><a href="title-bars-breadcrumb.php">Title Bars - Breadcrumb Only</a></li>
	                                        <li><a href="sections.php">Sections</a></li>
	                                        <li><a href="images.php">Images</a></li>
	                                    </ul>
	                                </div>
	                            </div>
	                         </div>
	                    </li>
	                    <li><a href="#">Buy it!</a></li>
	                </ul>
            </div>
        </div>
    </div>
    <div class="mobile-menu text-uppercase"></div>

</header>
<!-- Header / End -->