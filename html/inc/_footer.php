
<!-- Footer -->
<footer id="footer" class="bg-black dark">
  
  <div class="container padded-vertical-40">
      <div class="row">
          <div class="col-md-2 col-sm-6">
          	  <h5 class="text-light"><strong>Navigation</strong></h5>
              <!-- Navigation list -->
              <ul class="list-navigation margin-bottom-20">
              	<li><a href="#">Home Page</a></li>
                <li><a href="#">Features</a></li>
                <li><a href="#">Templates</a></li>
                <li><a href="#">Elements</a></li>
                <li><a href="#">Documentation</a></li>
                <li><a href="#">Buy it!</a></li>
              </ul>
          </div>
          <div class="col-md-4 col-sm-6">
          	  <h5 class="text-light">Say <strong>Hello</strong>!</h5>
              <!-- Contact Form -->
              <form id="contact-form" class="margin-bottom-20">
              		<div class="row">
              			<div class="col-md-6">
		                  	<div class="form-group">
		                    	<input type="text" class="form-control" id="name" name="name" placeholder="Name">
		                    </div>
	                    </div>
	                    <div class="col-md-6">
		                    <div class="form-group">
		                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
		                    </div>
	                    </div>
                    </div>
                    <div class="form-group">
                    	<textarea class="form-control" id="message" name="message" rows="3"  placeholder="Message"></textarea>
                    </div>
                    <div class="form-group clearfix">
						<button type="submit" class="btn btn-primary btn-filled btn-sm pull-right">Send it!</button>
                    </div>
              </form>
          </div>
          <div class="col-md-4 col-sm-6">
          	  <h5 class="text-light"><strong>Contact</strong> Me</h5>
              <!-- List with Icons -->
              <ul class="list-unstyled list-unstyled-icons margin-bototm-20">
                <li><i class="text-muted inline-icon fa fa-map-marker"></i><strong>Address:</strong> <span class="text-muted">1111-A Nowhere Lane, Outta Sight, State 90378, USA</span></li>
                <li><i class="text-muted inline-icon fa fa-comment"></i><strong>E-mail:</strong> <span class="text-muted"><a href="#">johnathandoe@suelo.pl</a></span></li>
                <li><i class="text-muted inline-icon fa fa-phone"></i><strong>Phone:</strong> <span class="text-muted">+0(31)6 89764536</span></li>
              </ul>
          </div>
          <div class="col-md-2 col-sm-6">
          	  <h5 class="text-light"><strong>Social</strong> Media</h5>
              <!-- Social Icons -->
              <div class="margin-bototm-20">
                  <a href="#" class="icon icon-circle icon-filled icon-xs icon-facebook"><i class="fa fa-facebook"></i></a>
                  <a href="#" class="icon icon-circle icon-filled icon-xs icon-twitter"><i class="fa fa-twitter"></i></a>
                  <a href="#" class="icon icon-circle icon-filled icon-xs icon-google-plus"><i class="fa fa-google-plus"></i></a>
              </div>
          </div>
      </div>
  </div>
  
  <!-- Copyright -->
  <div class="copyright bordered-top">
  	<div class="container padded-vertical-30">
    	<span class="text-muted">Copyright <strong>Trener - Multipurpose Coaching / Trainer Bootstrap Theme</strong> 2014&copy;</span>
        <ul class="list-inline pull-right text-muted margin-bottom-0">
        	<li><a href="#">Home page</a></li>
            <li><a href="#">Documentation</a></li>
            <li><a href="#">Buy it!</a></li>
        </ul>
    </div>
  </div>
  
</footer>
<!-- Footer / End -->

<!-- Alert Modal -->
<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title text-center" id="alertModalLabel"></h4>
      </div>
      <div class="modal-body text-center">
        <div class="modal-alert-content margin-bottom-20"></div>
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="icon-before fa fa-times"></i>Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Alert Modal / End -->

<!-- Rules Modal -->
<div class="modal fade" id="rulesModal" tabindex="-1" role="dialog" aria-labelledby="rulesModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="rulesModalLabel">Trener - Sign In Demo - Rules </h4>
      </div>
      <div class="modal-body">
        <ol>
        	<li>This form will send you a <strong>demo message</strong> on your e-mail address.</li>
        	<li>Your e-mail address <strong>won't be used for any purpose</strong> other than the one specified in the first point.</li>
        </ol>
        <p class="margin-bottom-0">If you accept those rules please fill the form and press <strong>"Sign In"</strong> button.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Rules Modal / End -->

<!-- Page Curtain -->
<div id="page-curtain" class="curtain-white"></div>